﻿
/*
 * Temp table to record deployment progress
 */
CREATE TABLE #__SSDT__Progress(
    ProgressID  INT IDENTITY(1, 1)
    ,StartDate  DATETIME2(3)
    ,EndDate    DATETIME2(3)
);

ALTER TABLE #__SSDT__Progress ADD CONSTRAINT PK_tmp__SSDT__Progress PRIMARY KEY CLUSTERED(ProgressID) WITH (FILLFACTOR = 100);
GO
DECLARE @SSDTStartDate  DATETIME2(3);

SELECT @SSDTStartDate = SYSDATETIME();
INSERT INTO #__SSDT__Progress(StartDate) VALUES(@SSDTStartDate);

PRINT '******************************************************************************************';
PRINT 'Started at: ' + CONVERT(VARCHAR(30), @SSDTStartDate, 121);
PRINT '';
GO