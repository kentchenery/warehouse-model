﻿/*
 * Calculate and output total duration
 */
UPDATE #__SSDT__Progress
SET EndDate = SYSDATETIME();

DECLARE @SSDTDeployDuration INT;
DECLARE @SSDTEndedAt        VARCHAR(30);

SELECT 
    @SSDTDeployDuration = DATEDIFF(SECOND, StartDate, EndDate)
    ,@SSDTEndedAt = CONVERT(VARCHAR(30), EndDate, 121)
FROM    #__SSDT__Progress;

PRINT '';
PRINT 'Ended at: ' + @SSDTEndedAt;
PRINT 'Total deployment duration: ' + CONVERT(VARCHAR(10), @SSDTDeployDuration) + ' seconds';
PRINT '******************************************************************************************';
GO

/* Drop the #__SSDT__Progress table */
DROP TABLE #__SSDT__Progress;
GO