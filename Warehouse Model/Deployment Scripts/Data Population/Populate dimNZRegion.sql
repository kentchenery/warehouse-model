﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimNZRegion';

/*
 * Information on the anniversary dates was taken from the Ministry of Business, Innovation and Employment: http://www.dol.govt.nz/er/holidaysandleave/publicholidays/publicholidaydates/current.asp
 * Their copyright notice is here: http://www.dol.govt.nz/common/copyright.asp 
 */

/* All the regions */
WITH cNZRegion(NZRegionID, RegionCode, RegionName, SubdivisionCategory, IslandID, IslandCode, IslandName, AnniversaryMonth, AnniversaryDay, AnniversaryDescription, AnniversaryName) AS (
	/* Unknown region */
	SELECT -1, 'UNK', 'Unknown', 'Unknown', NULL, 'UNK', 'Unknown', NULL, NULL, 'Unknown', 'Unknown' UNION ALL
	/* New Zealand regions */
	SELECT 1, 'NZ-AUK', 'Auckland', 'regional council', 1, 'NZ-N', 'North Island',1, 29, '29th January', 'Auckland Anniversary' UNION ALL
	SELECT 2, 'NZ-BOP', 'Bay of Plenty', 'regional council', 1, 'NZ-N', 'North Island', 1, 29, '29th January', 'Auckland Anniversary' UNION ALL
	SELECT 3, 'NZ-CAN', 'Canterbury', 'regional council', 2, 'NZ-S', 'South Island', 12, 16, '16th December', 'Canterbury Anniversary' UNION ALL
	SELECT 4, 'NZ-HKB', 'Hawkes Bay', 'regional council', 1, 'NZ-N', 'North Island', 11, 1, '1st November', 'Hawkes Bay Anniversary' UNION ALL
	SELECT 5, 'NZ-MWT', 'Manawatu-Wanganui', 'regional council', 1, 'NZ-N', 'North Island', 1, 22, '22nd January', 'Wellington Anniversary' UNION ALL
	SELECT 6, 'NZ-NTL', 'Northland', 'regional council', 1, 'NZ-N', 'North Island', 1, 29, '29th January', 'Auckland Anniversary' UNION ALL
	SELECT 7, 'NZ-OTA', 'Otago', 'regional council', 2, 'NZ-S', 'South Island', 3, 23, '23rd March', 'Otago Anniversary' UNION ALL
	SELECT 8, 'NZ-STL', 'Southland', 'regional council', 2, 'NZ-S', 'South Island', 1, 17, '17th January', 'Southland Anniversary' UNION ALL
	SELECT 9, 'NZ-TKI', 'Taranaki', 'regional council', 1, 'NZ-N', 'North Island', 3, 31, '31st March', 'Taranaki Anniversary' UNION ALL
	SELECT 10, 'NZ-WKO', 'Waikato', 'regional council', 1, 'NZ-N', 'North Island', 1, 29, '29th January', 'Auckland Anniversary'  UNION ALL
	SELECT 11, 'NZ-WGN', 'Wellington', 'regional council', 1, 'NZ-N', 'North Island', 1, 22, '22nd January', 'Wellington Anniversary' UNION ALL
	SELECT 12, 'NZ-WTC', 'West Coast', 'regional council', 2, 'NZ-S', 'South Island', 12, 1, '1st December', 'Westland Anniversary' UNION ALL
	SELECT 13, 'NZ-GIS', 'Gisborne District', 'unitary authority', 1, 'NZ-N', 'North Island', 11, 1, '1st November', 'Hawkes Bay Anniversary' UNION ALL
	SELECT 14, 'NZ-MBH', 'Marlborough District', 'unitary authority', 2, 'NZ-S', 'South Island', 12, 16, '16th December', 'Canterbury Anniversary' UNION ALL
	SELECT 15, 'NZ-NSN', 'Nelson City', 'unitary authority', 2, 'NZ-S', 'South Island', 12, 16, '16th December', 'Canterbury Anniversary' UNION ALL
	SELECT 16, 'NZ-TAS', 'Tasman District', 'unitary authority', 2, 'NZ-S', 'South Island', 12, 16, '16th December', 'Canterbury Anniversary' UNION ALL
	SELECT 17, 'NZ-CIT ', 'Chatham Islands Territory ', 'special island authority ', NULL, NULL, NULL, 11, 30, '30th November', 'Chatham Islands Anniversary'
)
MERGE INTO dbo.dimNZRegion AS tgt
USING cNZRegion AS src
ON tgt.NZRegionID = src.NZRegionID

WHEN MATCHED THEN UPDATE
SET
	RegionCode				= src.RegionCode
	,RegionName				= src.RegionName
	,SubdivisionCategory	= src.SubdivisionCategory
	,IslandID				= src.IslandID
	,IslandCode				= src.IslandCode
	,IslandName				= src.IslandName
	,AnniversaryMonth		= src.AnniversaryMonth
	,AnniversaryDay			= src.AnniversaryDay
	,AnniversaryDescription	= src.AnniversaryDescription
	,AnniversaryName		= src.AnniversaryName

WHEN NOT MATCHED THEN INSERT(
	NZRegionID
	,RegionCode
	,RegionName
	,SubdivisionCategory
	,IslandID
	,IslandCode
	,IslandName
	,AnniversaryMonth
	,AnniversaryDay
	,AnniversaryDescription
	,AnniversaryName
)
VALUES(
	src.NZRegionID
	,src.RegionCode
	,src.RegionName
	,src.SubdivisionCategory
	,src.IslandID
	,src.IslandCode
	,src.IslandName
	,src.AnniversaryMonth
	,src.AnniversaryDay
	,src.AnniversaryDescription
	,src.AnniversaryName
);
GO