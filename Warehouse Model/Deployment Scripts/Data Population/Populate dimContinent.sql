﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimContinents';

WITH cContinents(ContinentID, ContinentCode, ContinentName) AS (

	/* Unknown record */
	SELECT -1, '??', 'Unknown'			UNION ALL

	/* Continents */
	SELECT 1, 'AF', 'Africa'			UNION ALL
	SELECT 2, 'AN', 'Antarctica'		UNION ALL
	SELECT 3, 'AS', 'Asia'				UNION ALL
	SELECT 4, 'EU', 'Europe'			UNION ALL
	SELECT 5, 'NA', 'North America'		UNION ALL
	SELECT 6, 'OC', 'Oceania'			UNION ALL
	SELECT 7, 'SA', 'South America'

)
MERGE INTO dbo.dimContinent AS tgt
USING cContinents AS src
ON tgt.ContinentID = src.ContinentID

WHEN MATCHED THEN UPDATE
SET ContinentCode	= src.ContinentCode
	,ContinentName	= src.ContinentName

WHEN NOT MATCHED THEN
INSERT(ContinentID, ContinentCode, ContinentName)
VALUES(src.ContinentID, src.ContinentCode, src.ContinentName)
;
GO