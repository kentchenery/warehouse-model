﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimDate';

/* Unknown record */
MERGE INTO dbo.dimDate AS tgt
USING(
	SELECT
		-1				AS [DateID]
		,NULL			AS [DateKey]
		,'Unknown'		AS [DateDisplayShort]
		,'Unknown'		AS [DateDisplayLong]
		,NULL			AS [Year]
		,NULL			AS [Quarter]
		,'Unknown'		AS [QuarterName]
		,-1				AS [QuarterID]
		,NULL			AS [Month]
		,'Unknown'		AS [MonthName]
		,-1				AS [MonthID]
		,NULL			AS [Day]
		,'Unknown'		AS [WeekdayName]
		,NULL			AS [DayOfWeek]
		,NULL			AS [NthWeekDayOfMonth]
		,NULL			AS [NthWeekDayOfYear]
		,NULL			AS [ISOWeek]
		,NULL			AS [ISOYear]
		,'?'			AS [IsWeekDay]
		,'?'			AS [IsWeekend]
		,'?'			AS [IsBusinessDay]
		,'?'			AS [IsPublicHoliday]
		,NULL			AS [PublicHolidayName]
		,NULL			AS [HolidayDateID]
        ,NULL           AS [TaxYear]
        ,NULL           AS [TaxQuarter]
        ,NULL           AS [TaxQuarterName]
        ,NULL           AS [TaxQuarterID]
        ,NULL           AS [TaxMonthID]
	) AS src
ON tgt.DateID = src.DateID

WHEN MATCHED THEN UPDATE
SET
	[DateID]				= src.[DateID]
	,[DateKey]				= src.[DateKey]
	,[DateDisplayShort]		= src.[DateDisplayShort]
	,[DateDisplayLong]		= src.[DateDisplayLong]
	,[Year]					= src.[Year]
	,[Quarter]				= src.[Quarter]
	,[QuarterName]			= src.[QuarterName]
	,[QuarterID]			= src.[QuarterID]
	,[Month]				= src.[Month]
	,[MonthName]			= src.[MonthName]
	,[MonthID]				= src.[MonthID]
	,[Day]					= src.[Day]
	,[WeekdayName]			= src.[WeekdayName]
	,[DayOfWeek]			= src.[DayOfWeek]
	,[NthWeekDayOfMonth]	= src.[NthWeekDayOfMonth]
	,[NthWeekDayOfYear]		= src.[NthWeekDayOfYear]
	,[ISOWeek]				= src.[ISOWeek]
	,[ISOYear]				= src.[ISOYear]
	,[IsWeekday]			= src.[IsWeekDay]
	,[IsWeekend]			= src.[IsWeekend]
	,[IsBusinessDay]		= src.[IsBusinessDay]
	,[IsPublicHoliday]		= src.[IsPublicHoliday]
	,[PublicHolidayName]	= src.[PublicHolidayName]
	,[HolidayDateID]		= src.[HolidayDateID]
    ,[TaxYear]              = src.[TaxYear]
    ,[TaxQuarter]           = src.[TaxQuarter]
    ,[TaxQuarterName]       = src.[TaxQuarterName]
    ,[TaxQuarterID]         = src.[TaxQuarterID]
    ,[TaxMonthID]           = src.[TaxMonthID]

WHEN NOT MATCHED THEN
INSERT(
	[DateID]
	,[DateKey]
	,[DateDisplayShort]
	,[DateDisplayLong]
	,[Year]
	,[Quarter]
	,[QuarterName]
	,[QuarterID]
	,[Month]
	,[MonthName]
	,[MonthID]
	,[Day]
	,[WeekdayName]
	,[DayOfWeek]
	,[NthWeekDayOfMonth]
	,[NthWeekDayOfYear]
	,[ISOWeek]
	,[ISOYear]
	,[IsWeekday]
	,[IsWeekend]
	,[IsBusinessDay]
	,[IsPublicHoliday]
	,[PublicHolidayName]
	,[HolidayDateID]
    ,[TaxYear]             
    ,[TaxQuarter]          
    ,[TaxQuarterName]      
    ,[TaxQuarterID]        
    ,[TaxMonthID]          
	)
VALUES(
	src.[DateID]
	,src.[DateKey]
	,src.[DateDisplayShort]
	,src.[DateDisplayLong]
	,src.[Year]
	,src.[Quarter]
	,src.[QuarterName]
	,src.[QuarterID]
	,src.[Month]
	,src.[MonthName]
	,src.[MonthID]
	,src.[Day]
	,src.[WeekdayName]
	,src.[DayOfWeek]
	,src.[NthWeekDayOfMonth]
	,src.[NthWeekDayOfYear]
	,src.[ISOWeek]
	,src.[ISOYear]
	,src.[IsWeekDay]
	,src.[IsWeekend]
	,src.[IsBusinessDay]
	,src.[IsPublicHoliday]
	,src.[PublicHolidayName]
	,src.[HolidayDateID]
    ,src.[TaxYear]
    ,src.[TaxQuarter]
    ,src.[TaxQuarterName]
    ,src.[TaxQuarterID]
    ,src.[TaxMonthID]
	);
GO

/* All the dates */
DECLARE @StartDate	DATE = '1900-01-01';
DECLARE @EndDate	DATE = '2199-01-01';

/* Dates that the tax year starts.  E.g. 1st April: Day = 1, Month = 4 etc */
DECLARE @TaxStartDay    TINYINT = 1;
DECLARE @TaxStartMonth  TINYINT = 4;

EXEC dbo.dimDateAdd @StartDate = @StartDate, @EndDate = @EndDate, @TaxStartDay = @TaxStartDay, @TaxStartMonth = @TaxStartMonth;
GO