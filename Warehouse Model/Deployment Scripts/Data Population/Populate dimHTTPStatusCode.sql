﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimHTTPStatusCodes';

WITH cHTTPStatusCodes(HTTPStatusID, StatusCode, StatusClass, StatusName, StatusDescription) AS (

	/* Unknown record*/
	SELECT -1, NULL, 'Unknown', 'Unknown', 'Unknown' UNION ALL

	/* Valid codes*/
	SELECT 100, 100, 'Informational', 'Continue', 'This means that the server has received the request headers, and that the client should proceed to send the request body (in the case of a request for which a body needs to be sent; for example, a POST request). If the request body is large, sending it to a server when a request has already been rejected based upon inappropriate headers is inefficient. To have a server check if the request could be accepted based on the request''s headers alone, a client must send Expect: 100-continue as a header in its initial request and check if a 100 Continue status code is received in response before continuing (or receive 417 Expectation Failed and not continue).' UNION ALL
	SELECT 101, 101, 'Informational', 'Switching Protocols', 'This means the requester has asked the server to switch protocols and the server is acknowledging that it will do so.' UNION ALL
	SELECT 102, 102, 'Informational', 'Processing', 'As a WebDAV request may contain many sub-requests involving file operations, it may take a long time to complete the request. This code indicates that the server has received and is processing the request, but no response is available yet. This prevents the client from timing out and assuming the request was lost.' UNION ALL

	SELECT 200, 200, 'Success', 'OK', 'Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request the response will contain an entity describing or containing the result of the action.' UNION ALL
	SELECT 201, 201, 'Success', 'Created', 'The request has been fulfilled and resulted in a new resource being created.' UNION ALL
	SELECT 202, 202, 'Success', 'Accepted', 'The request has been accepted for processing, but the processing has not been completed. The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.' UNION ALL
	SELECT 203, 203, 'Success', 'Non-Authoritative Information', 'The server successfully processed the request, but is returning information that may be from another source.' UNION ALL
	SELECT 204, 204, 'Success', 'No Content', 'The server successfully processed the request, but is not returning any content.' UNION ALL
	SELECT 205, 205, 'Success', 'Reset Content', 'The server successfully processed the request, but is not returning any content. Unlike a 204 response, this response requires that the requester reset the document view.' UNION ALL
	SELECT 206, 206, 'Success', 'Partial Content', 'The server is delivering only part of the resource due to a range header sent by the client. The range header is used by tools like wget to enable resuming of interrupted downloads, or split a download into multiple simultaneous streams.' UNION ALL
	SELECT 207, 207, 'Success', 'Multi-Status', 'The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.' UNION ALL
	SELECT 208, 208, 'Success', 'Already Reported', 'The members of a DAV binding have already been enumerated in a previous reply to this request, and are not being included again.' UNION ALL
	SELECT 226, 226, 'Success', 'IM Used', 'The server has fulfilled a GET request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.' UNION ALL

	SELECT 300, 300, 'Redirection', 'Multiple Choices', 'Indicates multiple options for the resource that the client may follow. It, for instance, could be used to present different format options for video, list files with different extensions, or word sense disambiguation.' UNION ALL
	SELECT 301, 301, 'Redirection', 'Moved Permanently', 'This and all future requests should be directed to the given URI.' UNION ALL
	SELECT 302, 302, 'Redirection', 'Found', 'This is an example of industry practice contradicting the standard. The HTTP/1.0 specification (RFC 1945) required the client to perform a temporary redirect (the original describing phrase was &quot;Moved Temporarily&quot;), but popular browsers implemented 302 with the functionality of a 303 See Other. Therefore, HTTP/1.1 added status codes 303 and 307 to distinguish between the two behaviours. However, some Web applications and frameworks use the 302 status code as if it were the 303.' UNION ALL
	SELECT 303, 303, 'Redirection', 'See Other', 'The response to the request can be found under another URI using a GET method. When received in response to a POST (or PUT/DELETE), it should be assumed that the server has received the data and the redirect should be issued with a separate GET message.' UNION ALL
	SELECT 304, 304, 'Redirection', 'Not Modified', 'Indicates the resource has not been modified since last requested. Typically, the HTTP client provides a header like the If-Modified-Since header to provide a time against which to compare. Using this saves bandwidth and reprocessing on both the server and client, as only the header data must be sent and received in comparison to the entirety of the page being re-processed by the server, then sent again using more bandwidth of the server and client.' UNION ALL
	SELECT 305, 305, 'Redirection', 'Use Proxy', 'Many HTTP clients (such as Mozilla and Internet Explorer) do not correctly handle responses with this status code, primarily for security reasons.' UNION ALL
	SELECT 306, 306, 'Redirection', 'Switch Proxy', 'No longer used. Originally meant &quot;Subsequent requests should use the specified proxy.&quot;' UNION ALL
	SELECT 307, 307, 'Redirection', 'Temporary Redirect', 'In this case, the request should be repeated with another URI; however, future requests should still use the original URI. In contrast to how 302 was historically implemented, the request method is not allowed to be changed when reissuing the original request. For instance, a POST request repeated using another POST request.' UNION ALL
	SELECT 308, 308, 'Redirection', 'Permanent Redirect', 'The request, and all future requests should be repeated using another URI. 307 and 308 (as proposed) parallel the behaviours of 302 and 301, but do not allow the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly.' UNION ALL

	SELECT 400, 400, 'Client Error', 'Bad Request', 'The request cannot be fulfilled due to bad syntax.' UNION ALL
	SELECT 401, 401, 'Client Error', 'Unauthorized', 'Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authenticationand Digest access authentication.' UNION ALL
	SELECT 402, 402, 'Client Error', 'Payment Required', 'Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, but that has not happened, and this code is not usually used. As an example of its use, however, Apple''s MobileMe service generates a 402 error if the MobileMe account is delinquent.' UNION ALL
	SELECT 403, 403, 'Client Error', 'Forbidden', 'The request was a valid request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference. On servers where authentication is required, this commonly means that the provided credentials were successfully authenticated but that the credentials still do not grant the client permission to access the resource (e.g. a recognized user attempting to access restricted content).' UNION ALL
	SELECT 404, 404, 'Client Error', 'Not Found', 'The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.' UNION ALL
	SELECT 405, 405, 'Client Error', 'Method Not Allowed', 'A request was made of a resource using a request method not supported by that resource; for example, using GET on a form which requires data to be presented via POST, or using PUT on a read-only resource.' UNION ALL
	SELECT 406, 406, 'Client Error', 'Not Acceptable', 'The requested resource is only capable of generating content not acceptable according to the Accept headers sent in the request.' UNION ALL
	SELECT 407, 407, 'Client Error', 'Proxy Authentication Required', 'The client must first authenticate itself with the proxy.' UNION ALL
	SELECT 408, 408, 'Client Error', 'Request Timeout', 'The server timed out waiting for the request. According to W3 HTTP specifications: &quot;The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.&quot;' UNION ALL
	SELECT 409, 409, 'Client Error', 'Conflict', 'Indicates that the request could not be processed because of conflict in the request, such as an edit conflict.' UNION ALL
	SELECT 410, 410, 'Client Error', 'Gone', 'Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a 410 status code, the client should not request the resource again in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a &quot;404 Not Found&quot; may be used instead.' UNION ALL
	SELECT 411, 411, 'Client Error', 'Length Required', 'The request did not specify the length of its content, which is required by the requested resource.' UNION ALL
	SELECT 412, 412, 'Client Error', 'Precondition Failed', 'The server does not meet one of the preconditions that the requester put on the request.' UNION ALL
	SELECT 413, 413, 'Client Error', 'Request Entity Too Large', 'The request is larger than the server is willing or able to process.' UNION ALL
	SELECT 414, 414, 'Client Error', 'Request-URI Too Long', 'The URI provided was too long for the server to process.' UNION ALL
	SELECT 415, 415, 'Client Error', 'Unsupported Media Type', 'The request entity has a media type which the server or resource does not support. For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.' UNION ALL
	SELECT 416, 416, 'Client Error', 'Requested Range Not Satisfiable', 'The client has asked for a portion of the file, but the server cannot supply that portion. For example, if the client asked for a part of the file that lies beyond the end of the file.' UNION ALL
	SELECT 417, 417, 'Client Error', 'Expectation Failed', 'The server cannot meet the requirements of the Expect request-header field.' UNION ALL
	SELECT 418, 418, 'Client Error', 'I''m a teapot (RFC 2324)', 'This code was defined in 1998 as one of the traditional IETF April Fools'''' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers.' UNION ALL
	SELECT 420, 420, 'Client Error', 'Enhance Your Calm (Twitter)', 'Not part of the HTTP standard, but returned by the Twitter Search and Trends API when the client is being rate limited. Other services may wish to implement the 429 Too Many Requests response code instead.' UNION ALL
	SELECT 422, 422, 'Client Error', 'Unprocessable Entity (WebDAV; RFC 4918)', 'The request was well-formed but was unable to be followed due to semantic errors.' UNION ALL
	SELECT 423, 423, 'Client Error', 'Locked (WebDAV; RFC 4918)', 'The resource that is being accessed is locked.' UNION ALL
	SELECT 424, 424, 'Client Error', 'Failed Dependency (WebDAV; RFC 4918)', 'The request failed due to failure of a previous request (e.g. a PROPPATCH).' UNION ALL
	SELECT 425, 425, 'Client Error', 'Unordered Collection (Internet draft)', 'Defined in drafts of &quot;WebDAV Advanced Collections Protocol&quot;, but not present in &quot;Web Distributed Authoring and Versioning (WebDAV) Ordered Collections Protocol&quot;.' UNION ALL
	SELECT 426, 426, 'Client Error', 'Upgrade Required (RFC 2817)', 'The client should switch to a different protocol such as TLS/1.0.' UNION ALL
	SELECT 428, 428, 'Client Error', 'Precondition Required (RFC 6585)', 'The origin server requires the request to be conditional. Intended to prevent &quot;the ''''lost update'''' problem, where a client GETs a resource''''s state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.&quot;' UNION ALL
	SELECT 429, 429, 'Client Error', 'Too Many Requests (RFC 6585)', 'The user has sent too many requests in a given amount of time. Intended for use with rate limiting schemes.' UNION ALL
	SELECT 431, 431, 'Client Error', 'Request Header Fields Too Large (RFC 6585)', 'The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.' UNION ALL
	SELECT 444, 444, 'Client Error', 'No Response (Nginx)', 'Used in Nginx logs to indicate that the server has returned no information to the client and closed the connection (useful as a deterrent for malware).' UNION ALL
	SELECT 449, 449, 'Client Error', 'Retry With (Microsoft)', 'A Microsoft extension. The request should be retried after performing the appropriate action.' UNION ALL
	SELECT 450, 450, 'Client Error', 'Blocked by Windows Parental Controls (Microsoft)', 'A Microsoft extension. This error is given when Windows Parental Controls are turned on and are blocking access to the given webpage.' UNION ALL
	SELECT 451, 451, 'Client Error', 'Unavailable For Legal Reasons (Internet draft)', 'Defined in the internet draft &quot;A New HTTP Status Code for Legally-restricted Resources&quot;. Intended to be used when resource access is denied for legal reasons, e.g. censorship or government-mandated blocked access. A reference to the 1953 dystopian novel Fahrenheit 451, where books are outlawed.' UNION ALL
	SELECT 494, 494, 'Client Error', 'Request Header Too Large (Nginx)', 'Nginx internal code similar to 431 but it was introduced earlier.' UNION ALL
	SELECT 495, 495, 'Client Error', 'Cert Error (Nginx)', 'Nginx internal code used when SSL client certificate error occured to distinguish it from 4XX in a log and an error page redirection.' UNION ALL
	SELECT 496, 496, 'Client Error', 'No Cert (Nginx)', 'Nginx internal code used when client didn''t provide certificate to distinguish it from 4XX in a log and an error page redirection.' UNION ALL
	SELECT 497, 497, 'Client Error', 'HTTP to HTTPS (Nginx)', 'Nginx internal code used for the plain HTTP requests that are sent to HTTPS port to distinguish it from 4XX in a log and an error page redirection.' UNION ALL
	SELECT 499, 499, 'Client Error', 'Client Closed Request (Nginx)', 'Used in Nginx logs to indicate when the connection has been closed by client while the server is still processing its request, making server unable to send a status code back.' UNION ALL

	SELECT 500, 500, 'Server Error',  'Internal Server Error', 'A generic error message, given when no more specific message is suitable.' UNION ALL
	SELECT 501, 501, 'Server Error',  'Not Implemented', 'The server either does not recognize the request method, or it lacks the ability to fulfill the request.' UNION ALL
	SELECT 502, 502, 'Server Error',  'Bad Gateway', 'The server was acting as a gateway or proxy and received an invalid response from the upstream server.' UNION ALL
	SELECT 503, 503, 'Server Error',  'Service Unavailable', 'The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.' UNION ALL
	SELECT 504, 504, 'Server Error',  'Gateway Timeout', 'The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.' UNION ALL
	SELECT 505, 505, 'Server Error',  'HTTP Version Not Supported', 'The server does not support the HTTP protocol version used in the request.' UNION ALL
	SELECT 506, 506, 'Server Error',  'Variant Also Negotiates (RFC 2295)', 'Transparent content negotiation for the request results in a circular reference.' UNION ALL
	SELECT 507, 507, 'Server Error',  'Insufficient Storage (WebDAV; RFC 4918)', 'The server is unable to store the representation needed to complete the request.' UNION ALL
	SELECT 508, 508, 'Server Error',  'Loop Detected (WebDAV; RFC 5842)', 'The server detected an infinite loop while processing the request (sent in lieu of 208).' UNION ALL
	SELECT 509, 509, 'Server Error',  'Bandwidth Limit Exceeded (Apache bw/limited extension)', 'This status code, while used by many servers, is not specified in any RFCs.' UNION ALL
	SELECT 510, 510, 'Server Error',  'Not Extended (RFC 2774)', 'Further extensions to the request are required for the server to fulfill it.' UNION ALL
	SELECT 511, 511, 'Server Error',  'Network Authentication Required (RFC 6585)', 'The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network (e.g. &quot;captive portals&quot; used to require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot).' UNION ALL
	SELECT 598, 598, 'Server Error',  'Network read timeout error (Unknown)', 'This status code is not specified in any RFCs, but is used by Microsoft Corp. HTTP proxies to signal a network read timeout behind the proxy to a client in front of the proxy.' UNION ALL
	SELECT 599, 599, 'Server Error', 'Network connect timeout error (Unknown)', 'This status code is not specified in any RFCs, but is used by Microsoft Corp. HTTP proxies to signal a network connect timeout behind the proxy to a client in front of the proxy.'
)
MERGE INTO dbo.dimHTTPStatusCode AS tgt
USING cHTTPStatusCodes AS src
ON tgt.HTTPStatusID = src.HTTPStatusID

WHEN MATCHED THEN UPDATE
SET	StatusCode			= src.StatusCode
	,StatusClass		= src.StatusClass
	,StatusName			= src.StatusName
	,StatusDescription	= src.StatusDescription

WHEN NOT MATCHED THEN
INSERT(HTTPStatusID, StatusCode, StatusClass, StatusName, StatusDescription)
VALUES(src.HTTPStatusID, src.StatusCode, src.StatusClass, src.StatusName, src.StatusDescription)
;
GO