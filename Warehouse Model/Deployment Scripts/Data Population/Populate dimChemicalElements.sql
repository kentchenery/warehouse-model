﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimChemicalElements';

WITH cChemicalElements(ChemicalElementID, AtomicNumber, ElementName, ElementSymbol, [ElementGroup], Period, Block, STPState, Occurrence, Category, SubCategory, AtomicWeight, Density, MeltingPointK, BoilingPointK, Heat, ElectroNegativity, Abundance)
AS (
    /* Unknown record */
    SELECT -1, -1, 'Unknown', '?', 0, 0, '?', 'Unknown', 'Unknown', 'Unknown', 'Unknown', NULL, NULL, NULL, NULL, NULL, NULL, 'Unknown' UNION ALL

    /* All the chemical elements */
    SELECT 1, 1, 'Hydrogen', 'H', 1, 1, 's', 'Gas', 'Primordial', 'Non-metal', 'Other non-metal', 1.008, 0.00008988, 14.01, 20.28, 14.304, 2.2, '1400' UNION ALL
    SELECT 2, 2, 'Helium', 'He', 18, 1, 's', 'Gas', 'Primordial', 'Non-metal', 'Noble gas', 4.002602, 0.0001785, 0.956, 4.22, 5.193, NULL, '0.008' UNION ALL
    SELECT 3, 3, 'Lithium', 'Li', 1, 2, 's', 'Solid', 'Primordial', 'Metal', 'Alkali metal', 6.94, 0.534, 453.69, 1560, 3.582, 0.98, '20' UNION ALL
    SELECT 4, 4, 'Beryllium', 'Be', 2, 2, 's', 'Solid', 'Primordial', 'Metal', 'Alkaline earth metal', 9.012182, 1.85, 1560, 2742, 1.825, 1.57, '2.8' UNION ALL
    SELECT 5, 5, 'Boron', 'B', 13, 2, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 10.81, 2.34, 2349, 4200, 1.026, 2.04, '10' UNION ALL
    SELECT 6, 6, 'Carbon', 'C', 14, 2, 'p', 'Solid', 'Primordial', 'Non-metal', 'Other non-metal', 12.011, 2.267, 3800, 4300, 0.709, 2.55, '200' UNION ALL
    SELECT 7, 7, 'Nitrogen', 'N', 15, 2, 'p', 'Gas', 'Primordial', 'Non-metal', 'Other non-metal', 14.007, 0.0012506, 63.15, 77.36, 1.04, 3.04, '19' UNION ALL
    SELECT 8, 8, 'Oxygen', 'O', 16, 2, 'p', 'Gas', 'Primordial', 'Non-metal', 'Other non-metal', 15.999, 0.001429, 54.36, 90.2, 0.918, 3.44, '461000' UNION ALL
    SELECT 9, 9, 'Fluorine', 'F', 17, 2, 'p', 'Gas', 'Primordial', 'Non-metal', 'Halogen', 18.9984032, 0.001696, 53.53, 85.03, 0.824, 3.98, '585' UNION ALL
    SELECT 10, 10, 'Neon', 'Ne', 18, 2, 'p', 'Gas', 'Primordial', 'Non-metal', 'Noble gas', 20.1797, 0.0008999, 24.56, 27.07, 1.03, NULL, '0.005' UNION ALL
    SELECT 11, 11, 'Sodium', 'Na', 1, 3, 's', 'Solid', 'Primordial', 'Metal', 'Alkali metal', 22.98976928, 0.971, 370.87, 1156, 1.228, 0.93, '23600' UNION ALL
    SELECT 12, 12, 'Magnesium', 'Mg', 2, 3, 's', 'Solid', 'Primordial', 'Metal', 'Alkaline earth metal', 24.3050, 1.738, 923, 1363, 1.023, 1.31, '23300' UNION ALL
    SELECT 13, 13, 'Aluminium', 'Al', 13, 3, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 26.9815386, 2.698, 933.47, 2792, 0.897, 1.61, '82300' UNION ALL
    SELECT 14, 14, 'Silicon', 'Si', 14, 3, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 28.085, 2.3296, 1687, 3538, 0.705, 1.9, '282000' UNION ALL
    SELECT 15, 15, 'Phosphorus', 'P', 15, 3, 'p', 'Solid', 'Primordial', 'Non-metal', 'Other non-metal', 30.973762, 1.82, 317.3, 550, 0.769, 2.19, '1050' UNION ALL
    SELECT 16, 16, 'Sulfur', 'S', 16, 3, 'p', 'Solid', 'Primordial', 'Non-metal', 'Other non-metal', 32.06, 2.067, 388.36, 717.87, 0.71, 2.58, '350' UNION ALL
    SELECT 17, 17, 'Chlorine', 'Cl', 17, 3, 'p', 'Gas', 'Primordial', 'Non-metal', 'Halogen', 35.45, 0.003214, 171.6, 239.11, 0.479, 3.16, '145' UNION ALL
    SELECT 18, 18, 'Argon', 'Ar', 18, 3, 'p', 'Gas', 'Primordial', 'Non-metal', 'Noble gas', 39.948, 0.0017837, 83.8, 87.3, 0.52, NULL, '3.5' UNION ALL
    SELECT 19, 19, 'Potassium', 'K', 1, 4, 's', 'Solid', 'Primordial', 'Metal', 'Alkali metal', 39.0983, 0.862, 336.53, 1032, 0.757, 0.82, '20900' UNION ALL
    SELECT 20, 20, 'Calcium', 'Ca', 2, 4, 's', 'Solid', 'Primordial', 'Metal', 'Alkaline earth metal', 40.078, 1.54, 1115, 1757, 0.647, 1, '41500' UNION ALL
    SELECT 21, 21, 'Scandium', 'Sc', 3, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 44.955912, 2.989, 1814, 3109, 0.568, 1.36, '22' UNION ALL
    SELECT 22, 22, 'Titanium', 'Ti', 4, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 47.867, 4.54, 1941, 3560, 0.523, 1.54, '5650' UNION ALL
    SELECT 23, 23, 'Vanadium', 'V', 5, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 50.9415, 6.11, 2183, 3680, 0.489, 1.63, '120' UNION ALL
    SELECT 24, 24, 'Chromium', 'Cr', 6, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 51.9961, 7.15, 2180, 2944, 0.449, 1.66, '102' UNION ALL
    SELECT 25, 25, 'Manganese', 'Mn', 7, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 54.938045, 7.44, 1519, 2334, 0.479, 1.55, '950' UNION ALL
    SELECT 26, 26, 'Iron', 'Fe', 8, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 55.845, 7.874, 1811, 3134, 0.449, 1.83, '56300' UNION ALL
    SELECT 27, 27, 'Cobalt', 'Co', 9, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 58.933195, 8.86, 1768, 3200, 0.421, 1.88, '25' UNION ALL
    SELECT 28, 28, 'Nickel', 'Ni', 10, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 58.6934, 8.912, 1728, 3186, 0.444, 1.91, '84' UNION ALL
    SELECT 29, 29, 'Copper', 'Cu', 11, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 63.546, 8.96, 1357.77, 2835, 0.385, 1.9, '60' UNION ALL
    SELECT 30, 30, 'Zinc', 'Zn', 12, 4, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 65.38, 7.134, 692.88, 1180, 0.388, 1.65, '70' UNION ALL
    SELECT 31, 31, 'Gallium', 'Ga', 13, 4, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 69.723, 5.907, 302.9146, 2477, 0.371, 1.81, '19' UNION ALL
    SELECT 32, 32, 'Germanium', 'Ge', 14, 4, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 72.63, 5.323, 1211.4, 3106, 0.32, 2.01, '1.5' UNION ALL
    SELECT 33, 33, 'Arsenic', 'As', 15, 4, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 74.92160, 5.776, 1090, 887, 0.329, 2.18, '1.8' UNION ALL
    SELECT 34, 34, 'Selenium', 'Se', 16, 4, 'p', 'Solid', 'Primordial', 'Non-metal', 'Other non-metal', 78.96, 4.809, 453, 958, 0.321, 2.55, '0.05' UNION ALL
    SELECT 35, 35, 'Bromine', 'Br', 17, 4, 'p', 'Liquid', 'Primordial', 'Non-metal', 'Halogen', 79.904, 3.122, 265.8, 332, 0.474, 2.96, '2.4' UNION ALL
    SELECT 36, 36, 'Krypton', 'Kr', 18, 4, 'p', 'Gas', 'Primordial', 'Non-metal', 'Noble gas', 83.798, 0.003733, 115.79, 119.93, 0.248, 3, '<0.001' UNION ALL
    SELECT 37, 37, 'Rubidium', 'Rb', 1, 5, 's', 'Solid', 'Primordial', 'Metal', 'Alkali metal', 85.4678, 1.532, 312.46, 961, 0.363, 0.82, '90' UNION ALL
    SELECT 38, 38, 'Strontium', 'Sr', 2, 5, 's', 'Solid', 'Primordial', 'Metal', 'Alkaline earth metal', 87.62, 2.64, 1050, 1655, 0.301, 0.95, '370' UNION ALL
    SELECT 39, 39, 'Yttrium', 'Y', 3, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 88.90585, 4.469, 1799, 3609, 0.298, 1.22, '33' UNION ALL
    SELECT 40, 40, 'Zirconium', 'Zr', 4, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 91.224, 6.506, 2128, 4682, 0.278, 1.33, '165' UNION ALL
    SELECT 41, 41, 'Niobium', 'Nb', 5, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 92.90638, 8.57, 2750, 5017, 0.265, 1.6, '20' UNION ALL
    SELECT 42, 42, 'Molybdenum', 'Mo', 6, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 95.96, 10.22, 2896, 4912, 0.251, 2.16, '1.2' UNION ALL
    SELECT 43, 43, 'Technetium', 'Tc', 7, 5, 'd', 'Solid', 'Transient', 'Metal', 'Transition metal', 98, 11.5, 2430, 4538, NULL, 1.9, '<0.001' UNION ALL
    SELECT 44, 44, 'Ruthenium', 'Ru', 8, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 101.07, 12.37, 2607, 4423, 0.238, 2.2, '0.001' UNION ALL
    SELECT 45, 45, 'Rhodium', 'Rh', 9, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 102.90550, 12.41, 2237, 3968, 0.243, 2.28, '0.001' UNION ALL
    SELECT 46, 46, 'Palladium', 'Pd', 10, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 106.42, 12.02, 1828.05, 3236, 0.244, 2.2, '0.015' UNION ALL
    SELECT 47, 47, 'Silver', 'Ag', 11, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 107.8682, 10.501, 1234.93, 2435, 0.235, 1.93, '0.075' UNION ALL
    SELECT 48, 48, 'Cadmium', 'Cd', 12, 5, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 112.411, 8.69, 594.22, 1040, 0.232, 1.69, '0.159' UNION ALL
    SELECT 49, 49, 'Indium', 'In', 13, 5, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 114.818, 7.31, 429.75, 2345, 0.233, 1.78, '0.25' UNION ALL
    SELECT 50, 50, 'Tin', 'Sn', 14, 5, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 118.710, 7.287, 505.08, 2875, 0.228, 1.96, '2.3' UNION ALL
    SELECT 51, 51, 'Antimony', 'Sb', 15, 5, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 121.760, 6.685, 903.78, 1860, 0.207, 2.05, '0.2' UNION ALL
    SELECT 52, 52, 'Tellurium', 'Te', 16, 5, 'p', 'Solid', 'Primordial', 'Metalloid', 'Metalloid', 127.60, 6.232, 722.66, 1261, 0.202, 2.1, '0.001' UNION ALL
    SELECT 53, 53, 'Iodine', 'I', 17, 5, 'p', 'Solid', 'Primordial', 'Non-metal', 'Halogen', 126.90447, 4.93, 386.85, 457.4, 0.214, 2.66, '0.45' UNION ALL
    SELECT 54, 54, 'Xenon', 'Xe', 18, 5, 'p', 'Gas', 'Primordial', 'Non-metal', 'Noble gas', 131.293, 0.005887, 161.4, 165.03, 0.158, 2.6, '<0.001' UNION ALL
    SELECT 55, 55, 'Caesium', 'Cs', 1, 6, 's', 'Solid', 'Primordial', 'Metal', 'Alkali metal', 132.9054519, 1.873, 301.59, 944, 0.242, 0.79, '3' UNION ALL
    SELECT 56, 56, 'Barium', 'Ba', 2, 6, 's', 'Solid', 'Primordial', 'Metal', 'Alkaline earth metal', 137.327, 3.594, 1000, 2170, 0.204, 0.89, '425' UNION ALL
    SELECT 57, 57, 'Lanthanum', 'La', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 138.90547, 6.145, 1193, 3737, 0.195, 1.1, '39' UNION ALL
    SELECT 58, 58, 'Cerium', 'Ce', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 140.116, 6.77, 1068, 3716, 0.192, 1.12, '66.5' UNION ALL
    SELECT 59, 59, 'Praseodymium', 'Pr', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 140.90765, 6.773, 1208, 3793, 0.193, 1.13, '9.2' UNION ALL
    SELECT 60, 60, 'Neodymium', 'Nd', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 144.242, 7.007, 1297, 3347, 0.19, 1.14, '41.5' UNION ALL
    SELECT 61, 61, 'Promethium', 'Pm', 3, 6, 'f', 'Solid', 'Transient', 'Metal', 'Lanthanide', 145, 7.26, 1315, 3273, NULL, NULL, '<0.001' UNION ALL
    SELECT 62, 62, 'Samarium', 'Sm', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 150.36, 7.52, 1345, 2067, 0.197, 1.17, '7.05' UNION ALL
    SELECT 63, 63, 'Europium', 'Eu', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 151.964, 5.243, 1099, 1802, 0.182, 1.2, '2' UNION ALL
    SELECT 64, 64, 'Gadolinium', 'Gd', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 157.25, 7.895, 1585, 3546, 0.236, 1.2, '6.2' UNION ALL
    SELECT 65, 65, 'Terbium', 'Tb', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 158.92535, 8.229, 1629, 3503, 0.182, 1.2, '1.2' UNION ALL
    SELECT 66, 66, 'Dysprosium', 'Dy', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 162.500, 8.55, 1680, 2840, 0.17, 1.22, '5.2' UNION ALL
    SELECT 67, 67, 'Holmium', 'Ho', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 164.93032, 8.795, 1734, 2993, 0.165, 1.23, '1.3' UNION ALL
    SELECT 68, 68, 'Erbium', 'Er', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 167.259, 9.066, 1802, 3141, 0.168, 1.24, '3.5' UNION ALL
    SELECT 69, 69, 'Thulium', 'Tm', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 168.93421, 9.321, 1818, 2223, 0.16, 1.25, '0.52' UNION ALL
    SELECT 70, 70, 'Ytterbium', 'Yb', 3, 6, 'f', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 173.054, 6.965, 1097, 1469, 0.155, 1.1, '3.2' UNION ALL
    SELECT 71, 71, 'Lutetium', 'Lu', 3, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Lanthanide', 174.9668, 9.84, 1925, 3675, 0.154, 1.27, '0.8' UNION ALL
    SELECT 72, 72, 'Hafnium', 'Hf', 4, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 178.49, 13.31, 2506, 4876, 0.144, 1.3, '3' UNION ALL
    SELECT 73, 73, 'Tantalum', 'Ta', 5, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 180.94788, 16.654, 3290, 5731, 0.14, 1.5, '2' UNION ALL
    SELECT 74, 74, 'Tungsten', 'W', 6, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 183.84, 19.25, 3695, 5828, 0.132, 2.36, '1.3' UNION ALL
    SELECT 75, 75, 'Rhenium', 'Re', 7, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 186.207, 21.02, 3459, 5869, 0.137, 1.9, '<0.001' UNION ALL
    SELECT 76, 76, 'Osmium', 'Os', 8, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 190.23, 22.61, 3306, 5285, 0.13, 2.2, '0.002' UNION ALL
    SELECT 77, 77, 'Iridium', 'Ir', 9, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 192.217, 22.56, 2719, 4701, 0.131, 2.2, '0.001' UNION ALL
    SELECT 78, 78, 'Platinum', 'Pt', 10, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 195.084, 21.46, 2041.4, 4098, 0.133, 2.28, '0.005' UNION ALL
    SELECT 79, 79, 'Gold', 'Au', 11, 6, 'd', 'Solid', 'Primordial', 'Metal', 'Transition metal', 196.966569, 19.282, 1337.33, 3129, 0.129, 2.54, '0.004' UNION ALL
    SELECT 80, 80, 'Mercury', 'Hg', 12, 6, 'd', 'Liquid', 'Primordial', 'Metal', 'Transition metal', 200.59, 13.5336, 234.43, 629.88, 0.14, 2, '0.085' UNION ALL
    SELECT 81, 81, 'Thallium', 'Tl', 13, 6, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 204.38, 11.85, 577, 1746, 0.129, 1.62, '0.85' UNION ALL
    SELECT 82, 82, 'Lead', 'Pb', 14, 6, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 207.2, 11.342, 600.61, 2022, 0.129, 2.33, '14' UNION ALL
    SELECT 83, 83, 'Bismuth', 'Bi', 15, 6, 'p', 'Solid', 'Primordial', 'Metal', 'Post-transition metal', 208.98040, 9.807, 544.7, 1837, 0.122, 2.02, '0.009' UNION ALL
    SELECT 84, 84, 'Polonium', 'Po', 16, 6, 'p', 'Solid', 'Transient', 'Metal', 'Post-transition metal', 209, 9.32, 527, 1235, NULL, 2, '<0.001' UNION ALL
    SELECT 85, 85, 'Astatine', 'At', 17, 6, 'p', 'Solid', 'Transient', 'Non-metal', 'Halogen', 210, 7, 575, 610, NULL, 2.2, '<0.001' UNION ALL
    SELECT 86, 86, 'Radon', 'Rn', 18, 6, 'p', 'Gas', 'Transient', 'Non-metal', 'Noble gas', 222, 0.00973, 202, 211.3, 0.094, NULL, '<0.001' UNION ALL
    SELECT 87, 87, 'Francium', 'Fr', 1, 7, 's', 'Solid', 'Transient', 'Metal', 'Alkali metal', 223, 1.87, 300, 950, NULL, 0.7, '<0.001' UNION ALL
    SELECT 88, 88, 'Radium', 'Ra', 2, 7, 's', 'Solid', 'Transient', 'Metal', 'Alkaline earth metal', 226, 5.5, 973, 2010, NULL, 0.9, '<0.001' UNION ALL
    SELECT 89, 89, 'Actinium', 'Ac', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 227, 10.07, 1323, 3471, 0.12, 1.1, '<0.001' UNION ALL
    SELECT 90, 90, 'Thorium', 'Th', 3, 7, 'f', 'Solid', 'Primordial', 'Metal', 'Actinide', 232.03806, 11.72, 2115, 5061, 0.113, 1.3, '9.6' UNION ALL
    SELECT 91, 91, 'Protactinium', 'Pa', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 231.03588, 15.37, 1841, 4300, NULL, 1.5, '<0.001' UNION ALL
    SELECT 92, 92, 'Uranium', 'U', 3, 7, 'f', 'Solid', 'Primordial', 'Metal', 'Actinide', 238.02891, 18.95, 1405.3, 4404, 0.116, 1.38, '2.7' UNION ALL
    SELECT 93, 93, 'Neptunium', 'Np', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 237, 20.45, 917, 4273, NULL, 1.36, '<0.001' UNION ALL
    SELECT 94, 94, 'Plutonium', 'Pu', 3, 7, 'f', 'Solid', 'Primordial', 'Metal', 'Actinide', 244, 19.84, 912.5, 3501, NULL, 1.28, '<0.001' UNION ALL
    SELECT 95, 95, 'Americium', 'Am', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 243, 13.69, 1449, 2880, NULL, 1.3, '<0.001' UNION ALL
    SELECT 96, 96, 'Curium', 'Cm', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 247, 13.51, 1613, 3383, NULL, 1.3, '<0.001' UNION ALL
    SELECT 97, 97, 'Berkelium', 'Bk', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 247, 14.79, 1259, NULL, NULL, 1.3, '<0.001' UNION ALL
    SELECT 98, 98, 'Californium', 'Cf', 3, 7, 'f', 'Solid', 'Transient', 'Metal', 'Actinide', 251, 15.1, 1173, NULL, NULL, 1.3, '<0.001' UNION ALL
    SELECT 99, 99, 'Einsteinium', 'Es', 3, 7, 'f', 'Solid', 'Synthetic', 'Metal', 'Actinide', 252, 13.5, 1133, NULL, NULL, 1.3, '0' UNION ALL
    SELECT 100,100, 'Fermium', 'Fm', 3, 7, 'f', 'Solid', 'Synthetic', 'Metal', 'Actinide', 257, NULL, 1800, NULL, NULL, 1.3, '0' UNION ALL
    SELECT 101,101, 'Mendelevium', 'Md', 3, 7, 'f', 'Solid', 'Synthetic', 'Metal', 'Actinide', 258, NULL, 1100, NULL, NULL, 1.3, '0' UNION ALL
    SELECT 102,102, 'Nobelium', 'No', 3, 7, 'f', 'Solid', 'Synthetic', 'Metal', 'Actinide', 259, NULL, 1100, NULL, NULL, 1.3, '0' UNION ALL
    SELECT 103,103, 'Lawrencium', 'Lr', 3, 7, 'd', 'Solid', 'Synthetic', 'Metal', 'Actinide', 262, NULL, 1900, NULL, NULL, 1.3, '0' UNION ALL
    SELECT 104,104, 'Rutherfordium', 'Rf', 4, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 267, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 105,105, 'Dubnium', 'Db', 5, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 268, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 106,106, 'Seaborgium', 'Sg', 6, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 269, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 107,107, 'Bohrium', 'Bh', 7, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 270, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 108,108, 'Hassium', 'Hs', 8, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 269, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 109,109, 'Meitnerium', 'Mt', 9, 7, 'd', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 278, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 110,110, 'Darmstadtium', 'Ds', 10, 7, 'd', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 281, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 111,111, 'Roentgenium', 'Rg', 11, 7, 'd', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 281, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 112,112, 'Copernicium', 'Cn', 12, 7, 'd', 'Unknown', 'Synthetic', 'Metal', 'Transition metal', 285, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 113,113, 'Ununtrium', 'Uut', 13, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 286, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 114,114, 'Flerovium', 'Fl', 14, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 289, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 115,115, 'Ununpentium', 'Uup', 15, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 288, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 116,116, 'Livermorium', 'Lv', 16, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 293, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 117,117, 'Ununseptium', 'Uus', 17, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 294, NULL, NULL, NULL, NULL, NULL, '0' UNION ALL
    SELECT 118,118, 'Ununoctium', 'Uuo', 18, 7, 'p', 'Unknown', 'Synthetic', 'Unknown', 'Unknown', 294, NULL, NULL, NULL, NULL, NULL, '0' 
)
MERGE INTO dbo.dimChemicalElement AS tgt
USING cChemicalElements AS src
ON tgt.ChemicalElementID = src.ChemicalElementID

WHEN MATCHED THEN UPDATE
SET
   ChemicalElementID = src.ChemicalElementID
   ,AtomicNumber = src.AtomicNumber
   ,ElementSymbol = src.ElementSymbol
   ,ElementName = src.ElementName
   ,ElementGroup = src.ElementGroup
   ,Period = src.Period
   ,Block = src.Block
   ,STPState = src.STPState
   ,Occurrence = src.Occurrence
   ,Category = src.Category
   ,SubCategory = src.SubCategory
   ,AtomicWeight = src.AtomicWeight
   ,Density = src.Density
   ,MeltingPointK = src.MeltingPointK
   ,BoilingPointK = src.BoilingPointK
   ,Heat = src.Heat
   ,ElectroNegativity = src.ElectroNegativity
   ,Abundance = src.Abundance

WHEN NOT MATCHED THEN INSERT(ChemicalElementID, AtomicNumber, ElementName, ElementSymbol, [ElementGroup], Period, Block, STPState, Occurrence, Category, SubCategory, AtomicWeight, Density, MeltingPointK, BoilingPointK, Heat, ElectroNegativity, Abundance)
VALUES(src.ChemicalElementID, src.AtomicNumber, src.ElementName, src.ElementSymbol, src.[ElementGroup], src.Period, src.Block, src.STPState, src.Occurrence, src.Category, src.SubCategory, src.AtomicWeight, src.Density, src.MeltingPointK, src.BoilingPointK, src.Heat, src.ElectroNegativity, src.Abundance)
;
GO