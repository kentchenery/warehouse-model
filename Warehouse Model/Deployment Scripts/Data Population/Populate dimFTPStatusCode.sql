﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimFTPStatusCodes';


WITH cFTPCodeList(FTPStatusCode, StatusDescription) AS (

	SELECT 110, 'Restart marker replay . In this case, the text is exact and not left to the particular implementation; it must read: MARK yyyy = mmmm where yyyy is User-process data stream marker, and mmmm server''s equivalent marker (note the spaces between markers and "=").' UNION ALL
	SELECT 120, 'Service ready in nnn minutes.' UNION ALL
	SELECT 125, 'Data connection already open; transfer starting.' UNION ALL
	SELECT 150, 'File status okay; about to open data connection.' UNION ALL
	SELECT 202, 'Command not implemented, superfluous at this site.' UNION ALL
	SELECT 211, 'System status, or system help reply.' UNION ALL
	SELECT 212, 'Directory status.' UNION ALL
	SELECT 213, 'File status.' UNION ALL
	SELECT 214, 'Help message.On how to use the server or the meaning of a particular non-standard command. This reply is useful only to the human user.' UNION ALL
	SELECT 215, 'NAME system type. Where NAME is an official system name from the registry kept by IANA.' UNION ALL
	SELECT 220, 'Service ready for new user.' UNION ALL
	SELECT 221, 'Service closing control connection.' UNION ALL
	SELECT 225, 'Data connection open; no transfer in progress.' UNION ALL
	SELECT 226, 'Closing data connection. Requested file action successful (for example, file transfer or file abort).' UNION ALL
	SELECT 227, 'Entering Passive Mode (h1,h2,h3,h4,p1,p2).' UNION ALL
	SELECT 228, 'Entering Long Passive Mode (long address, port).' UNION ALL
	SELECT 229, 'Entering Extended Passive Mode (|||port|).' UNION ALL
	SELECT 230, 'User logged in, proceed. Logged out if appropriate.' UNION ALL
	SELECT 231, 'User logged out; service terminated.' UNION ALL
	SELECT 232, 'Logout command noted, will complete when transfer done.' UNION ALL
	SELECT 250, 'Requested file action okay, completed.' UNION ALL
	SELECT 257, 'PATHNAME created.' UNION ALL
	SELECT 331, 'User name okay, need password.' UNION ALL
	SELECT 332, 'Need account for login.' UNION ALL
	SELECT 350, 'Requested file action pending further information' UNION ALL
	SELECT 421, 'Service not available, closing control connection. This may be a reply to any command if the service knows it must shut down.' UNION ALL
	SELECT 425, 'Can''t open data connection.' UNION ALL
	SELECT 426, 'Connection closed; transfer aborted.' UNION ALL
	SELECT 430, 'Invalid username or password' UNION ALL
	SELECT 434, 'Requested host unavailable.' UNION ALL
	SELECT 450, 'Requested file action not taken.' UNION ALL
	SELECT 451, 'Requested action aborted. Local error in processing.' UNION ALL
	SELECT 452, 'Requested action not taken. Insufficient storage space in system.File unavailable (e.g., file busy).' UNION ALL
	SELECT 501, 'Syntax error in parameters or arguments.' UNION ALL
	SELECT 502, 'Command not implemented.' UNION ALL
	SELECT 503, 'Bad sequence of commands.' UNION ALL
	SELECT 504, 'Command not implemented for that parameter.' UNION ALL
	SELECT 530, 'Not logged in.' UNION ALL
	SELECT 532, 'Need account for storing files.' UNION ALL
	SELECT 550, 'Requested action not taken. File unavailable (e.g., file not found, no access).' UNION ALL
	SELECT 551, 'Requested action aborted. Page type unknown.' UNION ALL
	SELECT 552, 'Requested file action aborted. Exceeded storage allocation (for current directory or dataset).' UNION ALL
	SELECT 553, 'Requested action not taken. File name not allowed.' UNION ALL
	SELECT 631, 'Integrity protected reply.' UNION ALL
	SELECT 632, 'Confidentiality and integrity protected reply.' UNION ALL
	SELECT 633, 'Confidentiality protected reply.' UNION ALL
	SELECT 10054, 'Connection reset by peer. The connection was forcibly closed by the remote host.' UNION ALL
	SELECT 10060, 'Cannot connect to remote server.' UNION ALL
	SELECT 10061, 'Cannot connect to remote server. The connection is actively refused by the server.' UNION ALL
	SELECT 10066, 'Directory not empty.' UNION ALL
	SELECT 10068, 'Too many users, server is full.'

), cFTPCodes(FTPStatusCode, StatusDescription, FTPReplyType, FTPGroupType) AS (

	SELECT
		cfl.FTPStatusCode
		,cfl.StatusDescription
		,CASE WHEN cfl.FTPStatusCode > 999 THEN NULL ELSE cfl.FTPStatusCode / 100 END AS FTPReplyType
		,CASE WHEN cfl.FTPStatusCode > 999 THEN NULL ELSE (cfl.FTPStatusCode / 10) % 10 END AS FTPGroupType
	FROM
		cFTPCodeList cfl

), cReplyTypes(ReplyType, ReplyTypeName, ReplyTypeDescription) AS (

	SELECT 1, 'Positive Preliminary', 'The requested action is being initiated; expect another reply before proceeding with a new command. (The user-process sending another command before the completion reply would be in violation of protocol; but server-FTP processes should queue any commands that arrive while a preceding command is in progress.) This type of reply can be used to indicate that the command was accepted and the user-process may now pay attention to the data connections, for implementations where simultaneous monitoring is difficult. The server-FTP process may send at most, one 1xx reply per command.'	UNION ALL
	SELECT 2, 'Positive Completion', 'The requested action has been successfully completed. A new request may be initiated.' UNION ALL
	SELECT 3, 'Positive Intermediate', 'The command has been accepted, but the requested action is being held in abeyance, pending receipt of further information. The user should send another command specifying this information. This reply is used in command sequence groups.' UNION ALL
	SELECT 4, 'Transient Negative Completion', 'The command was not accepted and the requested action did not take place, but the error condition is temporary and the action may be requested again. The user should return to the beginning of the command sequence, if any. It is difficult to assign a meaning to "transient", particularly when two distinct sites (Server- and User-processes) have to agree on the interpretation. Each reply in the 4xx category might have a slightly different time value, but the intent is that the user-process is encouraged to try again. A rule of thumb in determining if a reply fits into the 4xx or the 5xx (Permanent Negative) category is that replies are 4xx if the commands can be repeated without any change in command form or in properties of the User or Server (e.g., the command is spelled the same with the same arguments used; the user does not change his file access or user name; the server does not put up a new implementation.)' UNION ALL
	SELECT 5, 'Permanent Negative Completion', 'The command was not accepted and the requested action did not take place. The User-process is discouraged from repeating the exact request (in the same sequence). Even some "permanent" error conditions can be corrected, so the human user may want to direct his User-process to reinitiate the command sequence by direct action at some point in the future (e.g., after the spelling has been changed, or the user has altered his directory status.)' UNION ALL
	SELECT 6, 'Protected', 'The RFC 2228 introduced the concept of protected replies to increase security over the FTP communications. The 6xx replies are Base64 encoded protected messages that serves as responses to secure commands. When properly decoded, these replies fall into the above categories.'

), cReplyGroup(ReplyGroup, ReplyGroupName, ReplyGroupDescription) AS (

	SELECT 0, 'Syntax', 'These replies refer to syntax errors, syntactically correct commands that don''t fit any functional category, unimplemented or superfluous commands.' UNION ALL
	SELECT 1, 'Information', 'These are replies to requests for information, such as status or help.' UNION ALL
	SELECT 2, 'Connections', 'Replies referring to the control and data connections.' UNION ALL
	SELECT 3, 'Authentication and accounting', 'Replies for the login process and accounting procedures.' UNION ALL
	SELECT 4, 'Unspecified as of RFC 959', 'Unspecified as of RFC 959' UNION ALL
	SELECT 5, 'File system', 'These replies indicate the status of the Server file system vis-a-vis the requested transfer or other file system action.'

), cFTP AS (

	SELECT
		f.FTPStatusCode									AS FTPStatusCodeID
		,f.FTPStatusCode								AS StatusCode
		,f.StatusDescription							AS StatusDescription
		,ISNULL(f.FTPReplyType, -1)                     AS ReplyType
		,ISNULL(crt.ReplyTypeName, 'Unknown')           AS ReplyTypeName
		,ISNULL(crt.ReplyTypeDescription, 'Unknown')    AS ReplyTypeDescription
		,ISNULL(f.FTPGroupType, -1)                     AS GroupType
		,ISNULL(crg.ReplyGroupName, 'Unknown')          AS GroupTypeName
		,ISNULL(crg.ReplyGroupDescription, 'Unknown')   AS GroupTypeDescription
	FROM
		cFTPCodes AS f
		LEFT OUTER JOIN cReplyTypes AS crt
			ON f.FTPReplyType = crt.ReplyType
		LEFT OUTER JOIN cReplyGroup AS crg
			ON f.FTPGroupType = crg.ReplyGroup

	UNION ALL

	SELECT
		-1, NULL, 'Unknown', -1, 'Unknown', 'Unknown', -1, 'Unknown', 'Unknown'

)
MERGE INTO dbo.dimFTPStatusCode AS tgt
USING cFTP AS src
ON tgt.FTPStatusCodeID	= src.FTPStatusCodeID

WHEN MATCHED THEN UPDATE
SET StatusCode				= src.StatusCode
	,StatusDescription		= src.StatusDescription
	,ReplyType				= src.ReplyType
	,ReplyTypeName			= src.ReplyTypeName
	,ReplyTypeDescription	= src.ReplyTypeDescription
	,GroupType				= src.GroupType
	,GroupTypeName			= src.GroupTypeName
	,GroupTypeDescription	= src.GroupTypeDescription

WHEN NOT MATCHED THEN
INSERT(FTPStatusCodeID, StatusCode, StatusDescription, ReplyType, ReplyTypeName, ReplyTypeDescription, GroupType, GroupTypeName, GroupTypeDescription)
VALUES(src.FTPStatusCodeID, src.StatusCode, src.StatusDescription, src.ReplyType, src.ReplyTypeName, src.ReplyTypeDescription, src.GroupType, src.GroupTypeName, src.GroupTypeDescription)
;
GO