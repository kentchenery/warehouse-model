﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimLanguages';

WITH cLanguages(LanguageID, LanguageFamily, EnglishName, NativeName, ISO639_1, ISO639_2T, ISO639_2B) AS (

	/* Unknown Languages */
	SELECT -1, N'Unknown', N'Unknown', N'Unknown','??','???','???' UNION ALL

	/* ISO Languages */
	SELECT 1, N'Northwest Caucasian', N'Abkhaz', N'аҧсуа бызшәа, аҧсшәа','ab','abk','abk' UNION ALL
	SELECT 2, N'Afro-Asiatic', N'Afar', N'Afaraf','aa','aar','aar' UNION ALL
	SELECT 3, N'Indo-European', N'Afrikaans', N'Afrikaans','af','afr','afr' UNION ALL
	SELECT 4, N'Niger–Congo', N'Akan', N'Akan','ak','aka','aka' UNION ALL
	SELECT 5, N'Indo-European', N'Albanian', N'gjuha shqipe','sq','sqi','alb' UNION ALL
	SELECT 6, N'Afro-Asiatic', N'Amharic', N'አማርኛ','am','amh','amh' UNION ALL
	SELECT 7, N'Afro-Asiatic', N'Arabic', N'العربية','ar','ara','ara' UNION ALL
	SELECT 8, N'Indo-European', N'Aragonese', N'aragonés','an','arg','arg' UNION ALL
	SELECT 9, N'Indo-European', N'Armenian', N'Հայերեն','hy','hye','arm' UNION ALL
	SELECT 10, N'Indo-European', N'Assamese', N'অসমীয়া','as','asm','asm' UNION ALL
	SELECT 11, N'Northeast Caucasian', N'Avaric', N'авар мацӀ, магӀарул мацӀ','av','ava','ava' UNION ALL
	SELECT 12, N'Indo-European', N'Avestan', N'avesta','ae','ave','ave' UNION ALL
	SELECT 13, N'Aymaran', N'Aymara', N'aymar aru','ay','aym','aym' UNION ALL
	SELECT 14, N'Turkic', N'Azerbaijani', N'azərbaycan dili','az','aze','aze' UNION ALL
	SELECT 15, N'Niger–Congo', N'Bambara', N'bamanankan','bm','bam','bam' UNION ALL
	SELECT 16, N'Turkic', N'Bashkir', N'башҡорт теле','ba','bak','bak' UNION ALL
	SELECT 17, N'Language isolate', N'Basque', N'euskara, euskera','eu','eus','baq' UNION ALL
	SELECT 18, N'Indo-European', N'Belarusian', N'беларуская мова','be','bel','bel' UNION ALL
	SELECT 19, N'Indo-European', N'Bengali', N'বাংলা','bn','ben','ben' UNION ALL
	SELECT 20, N'Indo-European', N'Bihari', N'भोजपुरी','bh','bih','bih' UNION ALL
	SELECT 21, N'Creole', N'Bislama', N'Bislama','bi','bis','bis' UNION ALL
	SELECT 22, N'Indo-European', N'Bosnian', N'bosanski jezik','bs','bos','bos' UNION ALL
	SELECT 23, N'Indo-European', N'Breton', N'brezhoneg','br','bre','bre' UNION ALL
	SELECT 24, N'Indo-European', N'Bulgarian', N'български език','bg','bul','bul' UNION ALL
	SELECT 25, N'Sino-Tibetan', N'Burmese', N'ဗမာစာ','my','mya','bur' UNION ALL
	SELECT 26, N'Indo-European', N'Catalan; Valencian', N'català, valencià','ca','cat','cat' UNION ALL
	SELECT 27, N'Austronesian', N'Chamorro', N'Chamoru','ch','cha','cha' UNION ALL
	SELECT 28, N'Northeast Caucasian', N'Chechen', N'нохчийн мотт','ce','che','che' UNION ALL
	SELECT 29, N'Niger–Congo', N'Chichewa; Chewa; Nyanja', N'chiCheŵa, chinyanja','ny','nya','nya' UNION ALL
	SELECT 30, N'Sino-Tibetan', N'Chinese', N'中文 (Zhōngwén), 汉语, 漢語','zh','zho','chi' UNION ALL
	SELECT 31, N'Turkic', N'Chuvash', N'чӑваш чӗлхи','cv','chv','chv' UNION ALL
	SELECT 32, N'Indo-European', N'Cornish', N'Kernewek','kw','cor','cor' UNION ALL
	SELECT 33, N'Indo-European', N'Corsican', N'corsu, lingua corsa','co','cos','cos' UNION ALL
	SELECT 34, N'Algonquian', N'Cree', N'ᓀᐦᐃᔭᐍᐏᐣ','cr','cre','cre' UNION ALL
	SELECT 35, N'Indo-European', N'Croatian', N'hrvatski jezik','hr','hrv','hrv' UNION ALL
	SELECT 36, N'Indo-European', N'Czech', N'čeština, český jazyk','cs','ces','cze' UNION ALL
	SELECT 37, N'Indo-European', N'Danish', N'dansk','da','dan','dan' UNION ALL
	SELECT 38, N'Indo-European', N'Divehi; Dhivehi; Maldivian;', N'ދިވެހި','dv','div','div' UNION ALL
	SELECT 39, N'Indo-European', N'Dutch', N'Nederlands, Vlaams','nl','nld','dut' UNION ALL
	SELECT 40, N'Sino-Tibetan', N'Dzongkha', N'རྫོང་ཁ','dz','dzo','dzo' UNION ALL
	SELECT 41, N'Indo-European', N'English', N'English','en','eng','eng' UNION ALL
	SELECT 42, N'Constructed', N'Esperanto', N'Esperanto','eo','epo','epo' UNION ALL
	SELECT 43, N'Uralic', N'Estonian', N'eesti, eesti keel','et','est','est' UNION ALL
	SELECT 44, N'Niger–Congo', N'Ewe', N'Eʋegbe','ee','ewe','ewe' UNION ALL
	SELECT 45, N'Indo-European', N'Faroese', N'føroyskt','fo','fao','fao' UNION ALL
	SELECT 46, N'Austronesian', N'Fijian', N'vosa Vakaviti','fj','fij','fij' UNION ALL
	SELECT 47, N'Uralic', N'Finnish', N'suomi, suomen kieli','fi','fin','fin' UNION ALL
	SELECT 48, N'Indo-European', N'French', N'français, langue française','fr','fra','fre' UNION ALL
	SELECT 49, N'Niger–Congo', N'Fula; Fulah; Pulaar; Pular', N'Fulfulde, Pulaar, Pular','ff','ful','ful' UNION ALL
	SELECT 50, N'Indo-European', N'Galician', N'galego','gl','glg','glg' UNION ALL
	SELECT 51, N'Niger–Congo', N'Ganda', N'Luganda','lg','lug','lug' UNION ALL
	SELECT 52, N'South Caucasian', N'Georgian', N'ქართული','ka','kat','geo' UNION ALL
	SELECT 53, N'Indo-European', N'German', N'Deutsch','de','deu','ger' UNION ALL
	SELECT 54, N'Indo-European', N'Greek, Modern', N'ελληνικά','el','ell','gre' UNION ALL
	SELECT 55, N'Tupian', N'Guaraní', N'Avañe''ẽ','gn','grn','grn' UNION ALL
	SELECT 56, N'Indo-European', N'Gujarati', N'ગુજરાતી','gu','guj','guj' UNION ALL
	SELECT 57, N'Creole', N'Haitian; Haitian Creole', N'Kreyòl ayisyen','ht','hat','hat' UNION ALL
	SELECT 58, N'Afro-Asiatic', N'Hausa', N'Hausa, هَوُسَ','ha','hau','hau' UNION ALL
	SELECT 59, N'Afro-Asiatic', N'Hebrew (modern)', N'עברית','he','heb','heb' UNION ALL
	SELECT 60, N'Niger–Congo', N'Herero', N'Otjiherero','hz','her','her' UNION ALL
	SELECT 61, N'Indo-European', N'Hindi', N'हिन्दी, हिंदी','hi','hin','hin' UNION ALL
	SELECT 62, N'Austronesian', N'Hiri Motu', N'Hiri Motu','ho','hmo','hmo' UNION ALL
	SELECT 63, N'Uralic', N'Hungarian', N'magyar','hu','hun','hun' UNION ALL
	SELECT 64, N'Indo-European', N'Icelandic', N'Íslenska','is','isl','ice' UNION ALL
	SELECT 65, N'Constructed', N'Ido', N'Ido','io','ido','ido' UNION ALL
	SELECT 66, N'Niger–Congo', N'Igbo', N'Asụsụ Igbo','ig','ibo','ibo' UNION ALL
	SELECT 67, N'Austronesian', N'Indonesian', N'Bahasa Indonesia','id','ind','ind' UNION ALL
	SELECT 68, N'Constructed', N'Interlingua', N'Interlingua','ia','ina','ina' UNION ALL
	SELECT 69, N'Constructed', N'Interlingue', N'Originally called Occidental; then Interlingue after WWII','ie','ile','ile' UNION ALL
	SELECT 70, N'Eskimo–Aleut', N'Inuktitut', N'ᐃᓄᒃᑎᑐᑦ','iu','iku','iku' UNION ALL
	SELECT 71, N'Eskimo–Aleut', N'Inupiaq', N'Iñupiaq, Iñupiatun','ik','ipk','ipk' UNION ALL
	SELECT 72, N'Indo-European', N'Irish', N'Gaeilge','ga','gle','gle' UNION ALL
	SELECT 73, N'Indo-European', N'Italian', N'italiano','it','ita','ita' UNION ALL
	SELECT 74, N'Japonic', N'Japanese', N'日本語 (にほんご)','ja','jpn','jpn' UNION ALL
	SELECT 75, N'Austronesian', N'Javanese', N'basa Jawa','jv','jav','jav' UNION ALL
	SELECT 76, N'Eskimo–Aleut', N'Kalaallisut, Greenlandic', N'kalaallisut, kalaallit oqaasii','kl','kal','kal' UNION ALL
	SELECT 77, N'Dravidian', N'Kannada', N'ಕನ್ನಡ','kn','kan','kan' UNION ALL
	SELECT 78, N'Nilo-Saharan', N'Kanuri', N'Kanuri','kr','kau','kau' UNION ALL
	SELECT 79, N'Indo-European', N'Kashmiri', N'कश्मीरी, كشميري‎','ks','kas','kas' UNION ALL
	SELECT 80, N'Turkic', N'Kazakh', N'қазақ тілі','kk','kaz','kaz' UNION ALL
	SELECT 81, N'Austroasiatic', N'Khmer', N'ខ្មែរ, ខេមរភាសា, ភាសាខ្មែរ','km','khm','khm' UNION ALL
	SELECT 82, N'Niger–Congo', N'Kikuyu, Gikuyu', N'Gĩkũyũ','ki','kik','kik' UNION ALL
	SELECT 83, N'Niger–Congo', N'Kinyarwanda', N'Ikinyarwanda','rw','kin','kin' UNION ALL
	SELECT 84, N'Niger–Congo', N'Kirundi', N'Ikirundi','rn','run','run' UNION ALL
	SELECT 85, N'Uralic', N'Komi', N'коми кыв','kv','kom','kom' UNION ALL
	SELECT 86, N'Niger–Congo', N'Kongo', N'KiKongo','kg','kon','kon' UNION ALL
	SELECT 87, N'Language isolate', N'Korean', N'한국어 (韓國語), 조선어 (朝鮮語)','ko','kor','kor' UNION ALL
	SELECT 88, N'Indo-European', N'Kurdish', N'Kurdî, كوردی‎','ku','kur','kur' UNION ALL
	SELECT 89, N'Niger–Congo', N'Kwanyama, Kuanyama', N'Kuanyama','kj','kua','kua' UNION ALL
	SELECT 90, N'Turkic', N'Kyrgyz', N'Кыргызча, Кыргыз тили','ky','kir','kir' UNION ALL
	SELECT 91, N'Tai–Kadai', N'Lao', N'ພາສາລາວ','lo','lao','lao' UNION ALL
	SELECT 92, N'Indo-European', N'Latin', N'latine, lingua latina','la','lat','lat' UNION ALL
	SELECT 93, N'Indo-European', N'Latvian', N'latviešu valoda','lv','lav','lav' UNION ALL
	SELECT 94, N'Indo-European', N'Limburgish, Limburgan, Limburger', N'Limburgs','li','lim','lim' UNION ALL
	SELECT 95, N'Niger–Congo', N'Lingala', N'Lingála','ln','lin','lin' UNION ALL
	SELECT 96, N'Indo-European', N'Lithuanian', N'lietuvių kalba','lt','lit','lit' UNION ALL
	SELECT 97, N'Niger–Congo', N'Luba-Katanga', N'Tshiluba','lu','lub','lub' UNION ALL
	SELECT 98, N'Indo-European', N'Luxembourgish, Letzeburgesch', N'Lëtzebuergesch','lb','ltz','ltz' UNION ALL
	SELECT 99, N'Indo-European', N'Macedonian', N'македонски јазик','mk','mkd','mac' UNION ALL
	SELECT 100, N'Austronesian', N'Malagasy', N'fiteny malagasy','mg','mlg','mlg' UNION ALL
	SELECT 101, N'Austronesian', N'Malay', N'bahasa Melayu, بهاس ملايو‎','ms','msa','may' UNION ALL
	SELECT 102, N'Dravidian', N'Malayalam', N'മലയാളം','ml','mal','mal' UNION ALL
	SELECT 103, N'Afro-Asiatic', N'Maltese', N'Malti','mt','mlt','mlt' UNION ALL
	SELECT 104, N'Indo-European', N'Manx', N'Gaelg, Gailck','gv','glv','glv' UNION ALL
	SELECT 105, N'Austronesian', N'Māori', N'te reo Māori','mi','mri','mao' UNION ALL
	SELECT 106, N'Indo-European', N'Marathi (Marāṭhī)', N'मराठी','mr','mar','mar' UNION ALL
	SELECT 107, N'Austronesian', N'Marshallese', N'Kajin M̧ajeļ','mh','mah','mah' UNION ALL
	SELECT 108, N'Mongolic', N'Mongolian', N'монгол','mn','mon','mon' UNION ALL
	SELECT 109, N'Austronesian', N'Nauru', N'Ekakairũ Naoero','na','nau','nau' UNION ALL
	SELECT 110, N'Dené–Yeniseian', N'Navajo, Navaho', N'Diné bizaad, Dinékʼehǰí','nv','nav','nav' UNION ALL
	SELECT 111, N'Niger–Congo', N'Ndonga', N'Owambo','ng','ndo','ndo' UNION ALL
	SELECT 112, N'Indo-European', N'Nepali', N'नेपाली','ne','nep','nep' UNION ALL
	SELECT 113, N'Niger–Congo', N'North Ndebele', N'isiNdebele','nd','nde','nde' UNION ALL
	SELECT 114, N'Uralic', N'Northern Sami', N'Davvisámegiella','se','sme','sme' UNION ALL
	SELECT 115, N'Indo-European', N'Norwegian', N'Norsk','no','nor','nor' UNION ALL
	SELECT 116, N'Indo-European', N'Norwegian Bokmål', N'Norsk bokmål','nb','nob','nob' UNION ALL
	SELECT 117, N'Indo-European', N'Norwegian Nynorsk', N'Norsk nynorsk','nn','nno','nno' UNION ALL
	SELECT 118, N'Sino-Tibetan', N'Nuosu', N'ꆈꌠ꒿ Nuosuhxop','ii','iii','iii' UNION ALL
	SELECT 119, N'Indo-European', N'Occitan', N'occitan, lenga d''òc','oc','oci','oci' UNION ALL
	SELECT 120, N'Algonquian', N'Ojibwe, Ojibwa', N'ᐊᓂᔑᓈᐯᒧᐎᓐ','oj','oji','oji' UNION ALL
	SELECT 121, N'Indo-European', N'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic', N'ѩзыкъ словѣньскъ','cu','chu','chu' UNION ALL
	SELECT 122, N'Inida-Asia', N'Oriya', N'ଓଡ଼ିଆ','or','ori','ori' UNION ALL
	SELECT 123, N'Afro-Asiatic', N'Oromo', N'Afaan Oromoo','om','orm','orm' UNION ALL
	SELECT 124, N'Indo-European', N'Ossetian, Ossetic', N'ирон æвзаг','os','oss','oss' UNION ALL
	SELECT 125, N'Indo-European', N'Pāli', N'पाऴि','pi','pli','pli' UNION ALL
	SELECT 126, N'Indo-European', N'Panjabi, Punjabi', N'ਪੰਜਾਬੀ, پنجابی‎','pa','pan','pan' UNION ALL
	SELECT 127, N'Indo-European', N'Pashto, Pushto', N'پښتو','ps','pus','pus' UNION ALL
	SELECT 128, N'Indo-European', N'Persian', N'فارسی','fa','fas','per' UNION ALL
	SELECT 129, N'Indo-European', N'Polish', N'język polski, polszczyzna','pl','pol','pol' UNION ALL
	SELECT 130, N'Indo-European', N'Portuguese', N'português','pt','por','por' UNION ALL
	SELECT 131, N'Quechuan', N'Quechua', N'Runa Simi, Kichwa','qu','que','que' UNION ALL
	SELECT 132, N'Indo-European', N'Romanian, Moldavian(Romanian from Republic of Moldova)', N'limba română','ro','ron','rum' UNION ALL
	SELECT 133, N'Indo-European', N'Romansh', N'rumantsch grischun','rm','roh','roh' UNION ALL
	SELECT 134, N'Indo-European', N'Russian', N'русский язык','ru','rus','rus' UNION ALL
	SELECT 135, N'Austronesian', N'Samoan', N'gagana fa''a Samoa','sm','smo','smo' UNION ALL
	SELECT 136, N'Creole', N'Sango', N'yângâ tî sängö','sg','sag','sag' UNION ALL
	SELECT 137, N'Indo-European', N'Sanskrit (Saṁskṛta)', N'संस्कृतम्','sa','san','san' UNION ALL
	SELECT 138, N'Indo-European', N'Sardinian', N'sardu','sc','srd','srd' UNION ALL
	SELECT 139, N'Indo-European', N'Scottish Gaelic; Gaelic', N'Gàidhlig','gd','gla','gla' UNION ALL
	SELECT 140, N'Indo-European', N'Serbian', N'српски језик','sr','srp','srp' UNION ALL
	SELECT 141, N'Niger–Congo', N'Shona', N'chiShona','sn','sna','sna' UNION ALL
	SELECT 142, N'Indo-European', N'Sindhi', N'सिन्धी, سنڌي، سندھی‎','sd','snd','snd' UNION ALL
	SELECT 143, N'Indo-European', N'Sinhala, Sinhalese', N'සිංහල','si','sin','sin' UNION ALL
	SELECT 144, N'Indo-European', N'Slovak', N'slovenčina, slovenský jazyk','sk','slk','slo' UNION ALL
	SELECT 145, N'Indo-European', N'Slovene', N'slovenski jezik, slovenščina','sl','slv','slv' UNION ALL
	SELECT 146, N'Afro-Asiatic', N'Somali', N'Soomaaliga, af Soomaali','so','som','som' UNION ALL
	SELECT 147, N'Niger–Congo', N'South Ndebele', N'isiNdebele','nr','nbl','nbl' UNION ALL
	SELECT 148, N'Niger–Congo', N'Southern Sotho', N'Sesotho','st','sot','sot' UNION ALL
	SELECT 149, N'Indo-European', N'Spanish; Castilian', N'español, castellano','es','spa','spa' UNION ALL
	SELECT 150, N'Austronesian', N'Sundanese', N'Basa Sunda','su','sun','sun' UNION ALL
	SELECT 151, N'Niger–Congo', N'Swahili', N'Kiswahili','sw','swa','swa' UNION ALL
	SELECT 152, N'Niger–Congo', N'Swati', N'SiSwati','ss','ssw','ssw' UNION ALL
	SELECT 153, N'Indo-European', N'Swedish', N'Svenska','sv','swe','swe' UNION ALL
	SELECT 154, N'Austronesian', N'Tagalog', N'Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔','tl','tgl','tgl' UNION ALL
	SELECT 155, N'Austronesian', N'Tahitian', N'Reo Tahiti','ty','tah','tah' UNION ALL
	SELECT 156, N'Indo-European', N'Tajik', N'тоҷикӣ, toğikī, تاجیکی‎','tg','tgk','tgk' UNION ALL
	SELECT 157, N'Indo-European', N'Tamil', N'தமிழ்','ta','tam','tam' UNION ALL
	SELECT 158, N'Turkic', N'Tatar', N'татар теле, tatar tele','tt','tat','tat' UNION ALL
	SELECT 159, N'Indo-European', N'Telugu', N'తెలుగు','te','tel','tel' UNION ALL
	SELECT 160, N'Tai–Kadai', N'Thai', N'ไทย','th','tha','tha' UNION ALL
	SELECT 161, N'Sino-Tibetan', N'Tibetan Standard, Tibetan, Central', N'བོད་ཡིག','bo','bod','tib' UNION ALL
	SELECT 162, N'Afro-Asiatic', N'Tigrinya', N'ትግርኛ','ti','tir','tir' UNION ALL
	SELECT 163, N'Austronesian', N'Tonga (Tonga Islands)', N'faka Tonga','to','ton','ton' UNION ALL
	SELECT 164, N'Niger–Congo', N'Tsonga', N'Xitsonga','ts','tso','tso' UNION ALL
	SELECT 165, N'Niger–Congo', N'Tswana', N'Setswana','tn','tsn','tsn' UNION ALL
	SELECT 166, N'Turkic', N'Turkish', N'Türkçe','tr','tur','tur' UNION ALL
	SELECT 167, N'Turkic', N'Turkmen', N'Türkmen, Түркмен','tk','tuk','tuk' UNION ALL
	SELECT 168, N'Niger–Congo', N'Twi', N'Twi','tw','twi','twi' UNION ALL
	SELECT 169, N'Turkic', N'Uighur, Uyghur', N'Uyƣurqə, ئۇيغۇرچە‎','ug','uig','uig' UNION ALL
	SELECT 170, N'Indo-European', N'Ukrainian', N'українська мова','uk','ukr','ukr' UNION ALL
	SELECT 171, N'Indo-European', N'Urdu', N'اردو','ur','urd','urd' UNION ALL
	SELECT 172, N'Turkic', N'Uzbek', N'O''zbek, Ўзбек, أۇزبېك‎','uz','uzb','uzb' UNION ALL
	SELECT 173, N'Niger–Congo', N'Venda', N'Tshivenḓa','ve','ven','ven' UNION ALL
	SELECT 174, N'Austroasiatic', N'Vietnamese', N'Tiếng Việt','vi','vie','vie' UNION ALL
	SELECT 175, N'Constructed', N'Volapük', N'Volapük','vo','vol','vol' UNION ALL
	SELECT 176, N'Indo-European', N'Walloon', N'walon','wa','wln','wln' UNION ALL
	SELECT 177, N'Indo-European', N'Welsh', N'Cymraeg','cy','cym','wel' UNION ALL
	SELECT 178, N'Indo-European', N'Western Frisian', N'Frysk','fy','fry','fry' UNION ALL
	SELECT 179, N'Niger–Congo', N'Wolof', N'Wollof','wo','wol','wol' UNION ALL
	SELECT 180, N'Niger–Congo', N'Xhosa', N'isiXhosa','xh','xho','xho' UNION ALL
	SELECT 181, N'Indo-European', N'Yiddish', N'ייִדיש','yi','yid','yid' UNION ALL
	SELECT 182, N'Niger–Congo', N'Yoruba', N'Yorùbá','yo','yor','yor' UNION ALL
	SELECT 183, N'Tai–Kadai', N'Zhuang, Chuang', N'Saɯ cueŋƅ, Saw cuengh','za','zha','zha' UNION ALL
	SELECT 184, N'Niger–Congo', N'Zulu', N'isiZulu','zu','zul','zul'

)
MERGE INTO dbo.dimLanguages AS tgt
USING cLanguages AS src
ON tgt.LanguageID = src.LanguageID

WHEN MATCHED THEN UPDATE
SET EnglishName		= src.EnglishName
	,NativeName		= src.NativeName
	,LanguageFamily = src.LanguageFamily
	,ISO639_1		= src.ISO639_1
	,ISO639_2T		= src.ISO639_2T
	,ISO639_2B		= src.ISO639_2B

WHEN NOT MATCHED THEN
INSERT(LanguageID, EnglishName, NativeName, LanguageFamily, ISO639_1, ISO639_2T, ISO639_2B)
VALUES(src.LanguageID, src.EnglishName, src.NativeName, src.LanguageFamily, src.ISO639_1, src.ISO639_2T, src.ISO639_2B)
;
GO