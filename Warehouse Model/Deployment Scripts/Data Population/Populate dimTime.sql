﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimTime';

/* Unknown record */
MERGE INTO dbo.dimTime AS tgt
USING (
		/* Unkown time */
		SELECT
			-1		AS TimeID
			,NULL	AS TimeKey
			,NULL	AS Hour24
			,NULL	AS Hour12
			,NULL	AS HalfHourID
			,'Unk'	AS HalfHour
			,-1		AS QuarterHourID
			,'Unk'	AS QuarterHour
			,-1		AS MinuteID
			,NULL	AS Minute
			,'Unk'	AS MinuteDisplay
			,NULL	AS Seconds
			,'Unknown'	AS TimeDisplayShort
			,'Unknown'	AS TimeDisplayLong
			,'?'		AS IsBusinessHours
			,'?'		AS IsMorning
			,'?'		AS IsAfternoon
			,'?'		AS AMPM
	) AS src
	ON tgt.TimeID = src.TimeID

WHEN MATCHED THEN UPDATE
SET	TimeKey				= src.TimeKey
	,Hour24				= src.Hour24
	,Hour12				= src.Hour12
	,HalfHourID			= src.HalfHourID
	,HalfHour			= src.HalfHour
	,QuarterHourID		= src.QuarterHourID
	,QuarterHour		= src.QuarterHour
	,MinuteID			= src.MinuteID
	,Minute				= src.Minute
	,MinuteDisplay		= src.MinuteDisplay
	,Seconds			= src.Seconds
	,TimeDisplayShort	= src.TimeDisplayShort
	,TimeDisplayLong	= src.TimeDisplayLong
	,IsBusinessHours	= src.IsBusinessHours
	,IsMorning			= src.IsMorning
	,IsAfternoon		= src.IsAfternoon
	,AMPM				= src.AMPM

WHEN NOT MATCHED THEN
INSERT(	TimeID
		,TimeKey				
		,Hour24				
		,Hour12				
		,HalfHourID			
		,HalfHour			
		,QuarterHourID		
		,QuarterHour		
		,MinuteID			
		,Minute				
		,MinuteDisplay		
		,Seconds			
		,TimeDisplayShort	
		,TimeDisplayLong	
		,IsBusinessHours	
		,IsMorning			
		,IsAfternoon		
		,AMPM				
)
VALUES(	src.TimeID
		,src.TimeKey
		,src.Hour24
		,src.Hour12
		,src.HalfHourID
		,src.HalfHour
		,src.QuarterHourID
		,src.QuarterHour
		,src.MinuteID
		,src.Minute
		,src.MinuteDisplay
		,src.Seconds
		,src.TimeDisplayShort
		,src.TimeDisplayLong
		,src.IsBusinessHours
		,src.IsMorning
		,src.IsAfternoon
		,src.AMPM
	);
GO

/* All the times */
DECLARE @TimeStart	TIME(0)	= '00:00:00';
DECLARE @TimeEnd	TIME(0) = '23:59:59';

WITH cTime AS (

	SELECT
		DATEADD(SECOND, n.Num, @TimeStart)	AS TimeKey
	FROM
		dbo.Numbers AS n
	WHERE
		n.Num <= DATEDIFF(SECOND, @TimeStart, @TimeEnd)

), cFormattedTime AS (

SELECT
	(DATEPART(HOUR, t.TimeKey) * 10000) + (DATEPART(MINUTE, t.TimeKey) * 100) + (DATEPART(SECOND, t.TimeKey))	AS TimeID
	,t.TimeKey
	,DATEPART(HOUR, t.TimeKey)			AS Hour24
	,DATEPART(HOUR, t.TimeKey) % 12		AS Hour12
	,(DATEPART(HOUR, t.TimeKey) * 100) + (DATEPART(MINUTE, t.TimeKey) / 30)										AS HalfHourID
	,CAST(CAST(CAST(DATEPART(HOUR, t.TimeKey) AS VARCHAR(10)) + ':' + CAST(((DATEPART(MINUTE, t.TimeKey) / 30) * 30)	AS VARCHAR(10)) + ':00' AS TIME(0))	AS VARCHAR(5)) AS HalfHour
	,(DATEPART(HOUR, t.TimeKey) * 100) + (DATEPART(MINUTE, t.TimeKey) / 15)										AS QuarterHourID
	,CAST(CAST(CAST(DATEPART(HOUR, t.TimeKey) AS VARCHAR(10)) + ':' + CAST(((DATEPART(MINUTE, t.TimeKey) / 15) * 15)	AS VARCHAR(10)) + ':00' AS TIME(0))	AS VARCHAR(5)) AS QuarterHour
	,(DATEPART(HOUR, t.TimeKey) * 100) + DATEPART(MINUTE, t.TimeKey)		AS MinuteID
	,DATEPART(MINUTE, t.TimeKey)	AS Minute
	,CAST(CAST(CAST(DATEPART(HOUR, t.TimeKey) AS VARCHAR(10)) + ':' + CAST(DATEPART(MINUTE, t.TimeKey)	AS VARCHAR(10)) + ':00' AS TIME(0)) AS NVARCHAR(5)) AS MinuteDisplay
	,DATEPART(SECOND, t.TimeKey)	AS Seconds
	,CONVERT(NVARCHAR(10), t.TimeKey, 114)	AS TimeDisplayShort
	,REPLACE(REPLACE(CONVERT(NVARCHAR(30), t.TimeKey, 109), 'AM', ' a.m.'), 'PM', ' p.m.')	AS TimeDisplayLong
FROM
	cTime AS t

)
MERGE INTO dbo.dimTime AS tgt
USING (
	SELECT
		*
		,CASE WHEN TimeKey BETWEEN '08:30:00' AND '16:59:59' THEN 'Y' ELSE 'N' END AS IsBusinessHours
		,CASE WHEN Hour24 BETWEEN 0 AND 11 THEN 'Y' ELSE 'N' END AS IsMorning
		,CASE WHEN Hour24 BETWEEN 0 AND 11 THEN 'N' ELSE 'Y' END AS IsAfternoon
		,CASE WHEN Hour24 BETWEEN 0 AND 11 THEN 'a.m.' ELSE 'p.m.' END AS AMPM
	FROM
		cFormattedTime
	) AS src
	ON tgt.TimeID = src.TimeID

WHEN MATCHED THEN UPDATE
SET	TimeKey				= src.TimeKey
	,Hour24				= src.Hour24
	,Hour12				= src.Hour12
	,HalfHourID			= src.HalfHourID
	,HalfHour			= src.HalfHour
	,QuarterHourID		= src.QuarterHourID
	,QuarterHour		= src.QuarterHour
	,MinuteID			= src.MinuteID
	,Minute				= src.Minute
	,MinuteDisplay		= src.MinuteDisplay
	,Seconds			= src.Seconds
	,TimeDisplayShort	= src.TimeDisplayShort
	,TimeDisplayLong	= src.TimeDisplayLong
	,IsBusinessHours	= src.IsBusinessHours
	,IsMorning			= src.IsMorning
	,IsAfternoon		= src.IsAfternoon
	,AMPM				= src.AMPM

WHEN NOT MATCHED THEN
INSERT(	TimeID
		,TimeKey				
		,Hour24				
		,Hour12				
		,HalfHourID			
		,HalfHour			
		,QuarterHourID		
		,QuarterHour		
		,MinuteID			
		,Minute				
		,MinuteDisplay		
		,Seconds			
		,TimeDisplayShort	
		,TimeDisplayLong	
		,IsBusinessHours	
		,IsMorning			
		,IsAfternoon		
		,AMPM				
)
VALUES(	src.TimeID
		,src.TimeKey
		,src.Hour24
		,src.Hour12
		,src.HalfHourID
		,src.HalfHour
		,src.QuarterHourID
		,src.QuarterHour
		,src.MinuteID
		,src.Minute
		,src.MinuteDisplay
		,src.Seconds
		,src.TimeDisplayShort
		,src.TimeDisplayLong
		,src.IsBusinessHours
		,src.IsMorning
		,src.IsAfternoon
		,src.AMPM
	);
GO