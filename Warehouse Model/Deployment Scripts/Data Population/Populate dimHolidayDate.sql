﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimHolidayDates';

WITH cHolidays(HolidayDateID, HolidayName, Month, Day, WeekdayName, NthOccurrence, IsMondayised) AS (

	SELECT	1, 'Christmas Day', 12, 25, NULL, NULL, 1			UNION ALL
	SELECT	2, 'Boxing Day', 12, 26, NULL, NULL, 1				UNION ALL
	SELECT	3, 'New Years Day', 1, 1, NULL, NULL, 1			    UNION ALL
	SELECT	4, 'Day after New Years Day', 1, 2, NULL, NULL, 1	UNION ALL
	SELECT	5, 'ANZAC Day', 4, 25, NULL, NULL, 1				UNION ALL
	SELECT	6, 'Waitangi Day', 2, 6, NULL, NULL, 1				UNION ALL

	SELECT	7, 'Queens Birthday', 6, NULL, 'Monday', 1, 0		UNION ALL
	SELECT	8, 'Labour Day', 10, NULL, 'Monday', 4, 0	

)
MERGE INTO dbo.[dimHolidayDate]	AS tgt
USING cHolidays	AS src
ON tgt.HolidayDateID = src.HolidayDateID

WHEN MATCHED THEN UPDATE
SET	Month			= src.Month
	,Day			= src.Day
	,WeekdayName	= src.WeekdayName
	,NthOccurrence	= src.NthOccurrence
	,IsMondayised	= src.IsMondayised

WHEN NOT MATCHED THEN 
INSERT(HolidayDateID, HolidayName, Month, Day, WeekdayName, NthOccurrence, IsMondayised)
VALUES(src.HolidayDateID, src.HolidayName, src.Month, src.Day, src.WeekdayName, src.NthOccurrence, src.IsMondayised);
GO