﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimGender';

WITH cGender(GenderID, Gender, GenderCode) AS (

    /* Genders */
    SELECT 0, 'Unknown', 'U' UNION ALL
    SELECT 1, 'Male', 'M' UNION ALL
    SELECT 2, 'Female', 'F' UNION ALL
    SELECT 9, 'Not Applicable', 'N'

)
MERGE INTO dbo.dimGender AS tgt
USING cGender AS src
ON tgt.GenderID = src.GenderID

WHEN MATCHED THEN UPDATE
SET GenderID    = src.GenderID
    ,Gender     = src.Gender
    ,GenderCode = src.GenderCode

WHEN NOT MATCHED THEN
INSERT(GenderID, Gender, GenderCode)
VALUES(src.GenderID, src.Gender, src.GenderCode)
;
GO