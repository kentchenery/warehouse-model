﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimCurrency';

WITH cCurrency(CurrencyCode, CurrencyName, CurrencyNumber, DecimalMinor, CurrencySymbol) AS (
	/* Unknown record */
	SELECT '???', 'Unknown', '-01', NULL, N'¤' UNION ALL

	/* Currencies */
	SELECT 'AED', 'UAE Dirham', '784', 2, NULL UNION ALL
	SELECT 'ANG', 'Netherlands Antillean Guilder', '532', 2, N'ƒ' UNION ALL
	SELECT 'AUD', 'Australian Dollar', '036', 2, N'$' UNION ALL
	SELECT 'AWG', 'Aruban Florin', '533', 2, N'ƒ' UNION ALL
	SELECT 'AZN', 'Azerbaijanian Manat', '944', 2, NULL UNION ALL
	SELECT 'BAM', 'Convertible Mark', '977', 2, NULL UNION ALL
	SELECT 'BBD', 'Barbados Dollar', '052', 2, N'$' UNION ALL
	SELECT 'BDT', 'Taka', '050', 2, N'৳' UNION ALL
	SELECT 'BGN', 'Bulgarian Lev', '975', 2, N'лв' UNION ALL
	SELECT 'BHD', 'Bahraini Dinar', '048', 3, NULL UNION ALL
	SELECT 'BIF', 'Burundi Franc', '108', 0, N'FBu' UNION ALL
	SELECT 'BMD', 'Bermudian Dollar', '060', 2, N'$' UNION ALL
	SELECT 'BND', 'Brunei Dollar', '096', 2, N'$' UNION ALL
	SELECT 'BOB', 'Boliviano', '068', 2, NULL UNION ALL
	SELECT 'BOV', 'Mvdol', '984', 2, NULL UNION ALL
	SELECT 'BRL', 'Brazilian Real', '986', 2, N'$' UNION ALL
	SELECT 'BSD', 'Bahamian Dollar', '044', 2, N'$' UNION ALL
	SELECT 'BTN', 'Ngultrum', '064', 2, N'Nu.' UNION ALL
	SELECT 'BWP', 'Pula', '072', 2, N'P' UNION ALL
	SELECT 'BYR', 'Belarussian Ruble', '974', 0, NULL UNION ALL
	SELECT 'BZD', 'Belize Dollar', '084', 2, N'$' UNION ALL
	SELECT 'CAD', 'Canadian Dollar', '124', 2, N'$' UNION ALL
	SELECT 'CDF', 'Congolese Franc', '976', 2, N'₣' UNION ALL
	SELECT 'CHE', 'WIR Euro', '947', 2, NULL UNION ALL
	SELECT 'CHF', 'Swiss Franc', '756', 2, N'₣' UNION ALL
	SELECT 'CHW', 'WIR Franc', '948', 2, NULL UNION ALL
	SELECT 'CLF', 'Unidades de fomento', '990', 0, NULL UNION ALL
	SELECT 'CLP', 'Chilean Peso', '152', 0, N'$' UNION ALL
	SELECT 'CNY', 'Yuan Renminbi', '156', 2, N'¥' UNION ALL
	SELECT 'COP', 'Colombian Peso', '170', 2, N'$' UNION ALL
	SELECT 'COU', 'Unidad de Valor Real', '970', 2, NULL UNION ALL
	SELECT 'CRC', 'Costa Rican Colon', '188', 2, N'₡' UNION ALL
	SELECT 'CUC', 'Peso Convertible', '931', 2, NULL UNION ALL
	SELECT 'CUP', 'Cuban Peso', '192', 2, N'$' UNION ALL
	SELECT 'CVE', 'Cape Verde Escudo', '132', 2, N'Esc' UNION ALL
	SELECT 'CZK', 'Czech Koruna', '203', 2, N'Kč' UNION ALL
	SELECT 'DJF', 'Djibouti Franc', '262', 0, N'₣' UNION ALL
	SELECT 'DKK', 'Danish Krone', '208', 2, N'kr' UNION ALL
	SELECT 'DOP', 'Dominican Peso', '214', 2, N'$' UNION ALL
	SELECT 'EGP', 'Egyptian Pound', '818', 2, NULL UNION ALL
	SELECT 'ERN', 'Nakfa', '232', 2, N'Nfk' UNION ALL
	SELECT 'ETB', 'Ethiopian Birr', '230', 2, NULL UNION ALL
	SELECT 'EUR', 'Euro', '978', 2, N'€' UNION ALL
	SELECT 'FJD', 'Fiji Dollar', '242', 2, N'$' UNION ALL
	SELECT 'FKP', 'Falkland Islands Pound', '238', 2, N'£' UNION ALL
	SELECT 'GBP', 'Pound Sterling', '826', 2, N'£' UNION ALL
	SELECT 'GEL', 'Lari', '981', 2, N'ლ' UNION ALL
	SELECT 'GHS', 'Ghana Cedi', '936', 2, NULL UNION ALL
	SELECT 'GIP', 'Gibraltar Pound', '292', 2, N'£' UNION ALL
	SELECT 'GMD', 'Dalasi', '270', 2, N'D' UNION ALL
	SELECT 'GNF', 'Guinea Franc', '324', 0, N'₣' UNION ALL
	SELECT 'GTQ', 'Quetzal', '320', 2, N'Q' UNION ALL
	SELECT 'GYD', 'Guyana Dollar', '328', 2, N'$' UNION ALL
	SELECT 'HKD', 'Hong Kong Dollar', '344', 2, N'$' UNION ALL
	SELECT 'HNL', 'Lempira', '340', 2, N'L' UNION ALL
	SELECT 'HRK', 'Croatian Kuna', '191', 2, N'kn' UNION ALL
	SELECT 'HTG', 'Gourde', '332', 2, N'G' UNION ALL
	SELECT 'HUF', 'Forint', '348', 2, N'Ft' UNION ALL
	SELECT 'IDR', 'Rupiah', '360', 2, N'Rp' UNION ALL
	SELECT 'ILS', 'New Israeli Sheqel', '376', 2, N'₪' UNION ALL
	SELECT 'INR', 'Indian Rupee', '356', 2, N'₹' UNION ALL
	SELECT 'IQD', 'Iraqi Dinar', '368', 3, NULL UNION ALL
	SELECT 'IRR', 'Iranian Rial', '364', 2, NULL UNION ALL
	SELECT 'ISK', 'Iceland Krona', '352', 0, N'kr' UNION ALL
	SELECT 'JMD', 'Jamaican Dollar', '388', 2, N'$' UNION ALL
	SELECT 'JOD', 'Jordanian Dinar', '400', 3, N'JD' UNION ALL
	SELECT 'JPY', 'Yen', '392', 0, N'¥' UNION ALL
	SELECT 'KES', 'Kenyan Shilling', '404', 2, N'Ksh' UNION ALL
	SELECT 'KGS', 'Som', '417', 2, N'сом' UNION ALL
	SELECT 'KHR', 'Riel', '116', 2, N'៛' UNION ALL
	SELECT 'KMF', 'Comoro Franc', '174', 0, N'₣' UNION ALL
	SELECT 'KPW', 'North Korean Won', '408', 2, N'₩' UNION ALL
	SELECT 'KRW', 'Won', '410', 0, N'₩' UNION ALL
	SELECT 'KWD', 'Kuwaiti Dinar', '414', 3, NULL UNION ALL
	SELECT 'KYD', 'Cayman Islands Dollar', '136', 2, N'$' UNION ALL
	SELECT 'KZT', 'Tenge', '398', 2, NULL UNION ALL
	SELECT 'LAK', 'Kip', '418', 2, N'₭' UNION ALL
	SELECT 'LBP', 'Lebanese Pound', '422', 2, N'£' UNION ALL
	SELECT 'LKR', 'Sri Lanka Rupee', '144', 2, N'₨' UNION ALL
	SELECT 'LRD', 'Liberian Dollar', '430', 2, N'$' UNION ALL
	SELECT 'LSL', 'Loti', '426', 2, N'M' UNION ALL
	SELECT 'LTL', 'Lithuanian Litas', '440', 2, N'Lt' UNION ALL
	SELECT 'LVL', 'Latvian Lats', '428', 2, N'Ls' UNION ALL
	SELECT 'LYD', 'Libyan Dinar', '434', 3, NULL UNION ALL
	SELECT 'MAD', 'Moroccan Dirham', '504', 2, NULL UNION ALL
	SELECT 'MDL', 'Moldovan Leu', '498', 2, NULL UNION ALL
	SELECT 'MGA', 'Malagasy Ariary', '969', 2, NULL UNION ALL
	SELECT 'MKD', 'Denar', '807', 2, NULL UNION ALL
	SELECT 'MMK', 'Kyat', '104', 2, N'K' UNION ALL
	SELECT 'MNT', 'Tugrik', '496', 2, NULL UNION ALL
	SELECT 'MOP', 'Pataca', '446', 2, N'MOP$' UNION ALL
	SELECT 'MRO', 'Ouguiya', '478', 2, N'UM' UNION ALL
	SELECT 'MUR', 'Mauritius Rupee', '480', 2, N'₨' UNION ALL
	SELECT 'MVR', 'Rufiyaa', '462', 2, N'Rf.' UNION ALL
	SELECT 'MWK', 'Kwacha', '454', 2, N'MK' UNION ALL
	SELECT 'MXN', 'Mexican Peso', '484', 2, N'$' UNION ALL
	SELECT 'MXV', 'Mexican Unidad de Inversion (UDI)', '979', 2, NULL UNION ALL
	SELECT 'MYR', 'Malaysian Ringgit', '458', 2, N'RM' UNION ALL
	SELECT 'MZN', 'Mozambique Metical', '943', 2, N'MT'N UNION ALL
	SELECT 'NAD', 'Namibia Dollar', '516', 2, N'$' UNION ALL
	SELECT 'NGN', 'Naira', '566', 2, N'₦' UNION ALL
	SELECT 'NIO', 'Cordoba Oro', '558', 2, NULL UNION ALL
	SELECT 'NOK', 'Norwegian Krone', '578', 2, NULL UNION ALL
	SELECT 'NPR', 'Nepalese Rupee', '524', 2, N'₨' UNION ALL
	SELECT 'NZD', 'New Zealand Dollar', '554', 2, N'$' UNION ALL
	SELECT 'OMR', 'Rial Omani', '512', 3, NULL UNION ALL
	SELECT 'PAB', 'Balboa', '590', 2, NULL UNION ALL
	SELECT 'PEN', 'Nuevo Sol', '604', 2, N'S/.' UNION ALL
	SELECT 'PGK', 'Kina', '598', 2, NULL UNION ALL
	SELECT 'PHP', 'Philippine Peso', '608', 2, N'₱' UNION ALL
	SELECT 'PKR', 'Pakistan Rupee', '586', 2, N'₨' UNION ALL
	SELECT 'PLN', 'Zloty', '985', 2, N'zł' UNION ALL
	SELECT 'PYG', 'Guarani', '600', 0, N'₲' UNION ALL
	SELECT 'QAR', 'Qatari Rial', '634', 2, NULL UNION ALL
	SELECT 'RON', 'New Romanian Leu', '946', 2, NULL UNION ALL
	SELECT 'RSD', 'Serbian Dinar', '941', 2, N'ДИН' UNION ALL
	SELECT 'RUB', 'Russian Ruble', '643', 2, NULL UNION ALL
	SELECT 'RWF', 'Rwanda Franc', '646', 0, N'FRw' UNION ALL
	SELECT 'SAR', 'Saudi Riyal', '682', 2, NULL UNION ALL
	SELECT 'SBD', 'Solomon Islands Dollar', '090', 2, N'$' UNION ALL
	SELECT 'SCR', 'Seychelles Rupee', '690', 2, N'SRe' UNION ALL
	SELECT 'SDG', 'Sudanese Pound', '938', 2, N'£' UNION ALL
	SELECT 'SEK', 'Swedish Krona', '752', 2, N'kr' UNION ALL
	SELECT 'SGD', 'Singapore Dollar', '702', 2, N'$' UNION ALL
	SELECT 'SHP', 'Saint Helena Pound', '654', 2, N'£' UNION ALL
	SELECT 'SLL', 'Leone', '694', 2, N'Le' UNION ALL
	SELECT 'SOS', 'Somali Shilling', '706', 2, N'Sh.So.' UNION ALL
	SELECT 'SRD', 'Surinam Dollar', '968', 2, N'$' UNION ALL
	SELECT 'SSP', 'South Sudanese Pound', '728', 2, N'£' UNION ALL
	SELECT 'STD', 'Dobra', '678', 2, N'Db' UNION ALL
	SELECT 'SVC', 'El Salvador Colon', '222', 2, NULL UNION ALL
	SELECT 'SYP', 'Syrian Pound', '760', 2, N'£' UNION ALL
	SELECT 'SZL', 'Lilangeni', '748', 2, N'E' UNION ALL
	SELECT 'THB', 'Baht', '764', 2, N'฿' UNION ALL
	SELECT 'TJS', 'Somoni', '972', 2, NULL UNION ALL
	SELECT 'TMT', 'Turkmenistan New Manat', '934', 2, NULL UNION ALL
	SELECT 'TND', 'Tunisian Dinar', '788', 3, NULL UNION ALL
	SELECT 'TOP', 'Pa’anga', '776', 2, NULL UNION ALL
	SELECT 'TRY', 'Turkish Lira', '949', 2, N'' UNION ALL
	SELECT 'TTD', 'Trinidad and Tobago Dollar', '780', 2, N'$' UNION ALL
	SELECT 'TWD', 'New Taiwan Dollar', '901', 2, N'$' UNION ALL
	SELECT 'TZS', 'Tanzanian Shilling', '834', 2, NULL UNION ALL
	SELECT 'UAH', 'Hryvnia', '980', 2, NULL UNION ALL
	SELECT 'UGX', 'Uganda Shilling', '800', 2, N'USh' UNION ALL
	SELECT 'USD', 'US Dollar', '840', 2, N'$' UNION ALL
	SELECT 'USN', 'US Dollar (Next day)', '997', 2, N'$' UNION ALL
	SELECT 'USS', 'US Dollar (Same day)', '998', 2, N'$' UNION ALL
	SELECT 'UYI', 'Uruguay Peso en Unidades Indexadas (URUIURUI)', '940', 0, NULL UNION ALL
	SELECT 'UYU', 'Peso Uruguayo', '858', 2, N'$' UNION ALL
	SELECT 'UZS', 'Uzbekistan Sum', '860', 2, NULL UNION ALL
	SELECT 'VEF', 'Bolivar ', '937', 2, NULL UNION ALL
	SELECT 'VND', 'Dong', '704', 0, N'₫' UNION ALL
	SELECT 'VUV', 'Vatu', '548', 0, N'VT' UNION ALL
	SELECT 'WST', 'Tala', '882', 2, N'WS$' UNION ALL
	SELECT 'XAF', 'CFA Franc BEAC', '950', 0, NULL UNION ALL
	SELECT 'XAG', 'Silver', '961', NULL, NULL UNION ALL
	SELECT 'XAU', 'Gold', '959', NULL, NULL UNION ALL
	SELECT 'XBA', 'Bond Markets Unit European Composite Unit (EURCO)', '955', NULL, NULL UNION ALL
	SELECT 'XBB', 'Bond Markets Unit European Monetary Unit (E.M.U.-6)', '956', NULL, NULL UNION ALL
	SELECT 'XBC', 'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)', '957', NULL, NULL UNION ALL
	SELECT 'XBD', 'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)', '958', NULL, NULL UNION ALL
	SELECT 'XCD', 'East Caribbean Dollar', '951', 2, N'$' UNION ALL
	SELECT 'XDR', 'SDR (Special Drawing Right)', '960', NULL, NULL UNION ALL
	SELECT 'XFU', 'UIC-Franc', 'Nil', NULL, NULL UNION ALL
	SELECT 'XOF', 'CFA Franc BCEAO', '952', 0, NULL UNION ALL
	SELECT 'XPD', 'Palladium', '964', NULL, NULL UNION ALL
	SELECT 'XPF', 'CFP Franc', '953', 0, NULL UNION ALL
	SELECT 'XPT', 'Platinum', '962', NULL, NULL UNION ALL
	SELECT 'XSU', 'Sucre', '994', NULL, NULL UNION ALL
	SELECT 'XTS', 'Codes specifically reserved for testing purposes', '963', NULL, NULL UNION ALL
	SELECT 'XUA', 'ADB Unit of Account', '965', NULL, NULL UNION ALL
	SELECT 'XXX', 'The codes assigned for transactions where no currency is involved', '999', NULL, NULL UNION ALL
	SELECT 'YER', 'Yemeni Rial', '886', 2, NULL UNION ALL
	SELECT 'ZAR', 'Rand', '710', 2, N'R' UNION ALL
	SELECT 'ZMW', 'Zambian Kwacha', '967', 2, N'ZK' UNION ALL
	SELECT 'ZWL', 'Zimbabwe Dollar', '932', 2, N'$' 

)
MERGE INTO dbo.dimCurrency AS tgt
USING (
    SELECT
        CONVERT(INT,
            CASE
                WHEN CurrencyNumber = 'Nil' THEN '000'
                ELSE CurrencyNumber
            END
            ) AS CurrencyID
        ,CurrencyCode
        ,CurrencyName
        ,CurrencyNumber
        ,DecimalMinor
        ,CurrencySymbol
    FROM
        cCurrency
    ) AS src
ON tgt.CurrencyID = src.CurrencyID

WHEN MATCHED THEN UPDATE
SET CurrencyCode    = src.CurrencyCode
    ,CurrencyName   = src.CurrencyName
    ,CurrencyNumber = src.CurrencyNumber
    ,DecimalMinor   = src.DecimalMinor
    ,CurrencySymbol = src.CurrencySymbol

WHEN NOT MATCHED THEN
INSERT(CurrencyID, CurrencyCode, CurrencyName, CurrencyNumber, DecimalMinor, CurrencySymbol)
VALUES(src.CurrencyID, src.CurrencyCode, src.CurrencyName, src.CurrencyNumber, src.DecimalMinor, src.CurrencySymbol)
;
GO