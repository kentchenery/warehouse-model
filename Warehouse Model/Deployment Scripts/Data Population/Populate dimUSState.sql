﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimUSState';

WITH cUSState(USStateID, StateCode, CommonName, OfficialName, CapitalCity, MostPopulatedCity, StatehoodDate, AreaMi, AreaKM, [Population], CensusBureauRegion, CensusBureauDivision, EconomicAnalysisRegion
) AS (
    /* Unknown record */
    SELECT -1, '??', 'Unknown', 'Unknown', 'Unknown', 'Unknown', NULL, NULL, NULL, NULL, 'Unknown', 'Unknown', 'Unknown' UNION ALL

    /* District of columbia */
    -- SELECT 1, 'DC', 'Washington DC', District of Columbia'
    /* All the states */
    SELECT 2, 'AL', 'Alabama', 'State of Alabama', 'Montgomery', 'Birmingham', CAST('December 14, 1819' AS DATE), 52419, 135765 , 4822023, 'South', 'East South Central', 'Southeast' UNION ALL
    SELECT 3, 'AK', 'Alaska', 'State of Alaska', 'Juneau', 'Anchorage', CAST('January 3, 1959' AS DATE), 663267, 1717854 , 731449, 'West', 'Pacific', 'Far West' UNION ALL
    SELECT 4, 'AZ', 'Arizona', 'State of Arizona', 'Phoenix', 'Phoenix', CAST('February 14, 1912' AS DATE), 113998, 295254 , 6553255, 'West', 'Mountain', 'Southwest' UNION ALL
    SELECT 5, 'AR', 'Arkansas', 'State of Arkansas', 'Little Rock', 'Little Rock', CAST('June 15, 1836' AS DATE), 52897, 137002 , 2949131, 'South', 'West South Central', 'Southeast' UNION ALL
    SELECT 6, 'CA', 'California', 'State of California', 'Sacramento', 'Los Angeles', CAST('September 9, 1850' AS DATE), 163700, 423970 , 38041430, 'West', 'Pacific', 'Far West' UNION ALL
    SELECT 7, 'CO', 'Colorado', 'State of Colorado', 'Denver', 'Denver', CAST('August 1, 1876' AS DATE), 104185, 269837 , 5187582, 'West', 'Mountain', 'Rocky Mountain' UNION ALL
    SELECT 8, 'CT', 'Connecticut', 'State of Connecticut', 'Hartford', 'Bridgeport', CAST('January 9, 1788' AS DATE), 5543, 14356 , 3590347, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 9, 'DE', 'Delaware', 'State of Delaware', 'Dover', 'Wilmington', CAST('December 7, 1787' AS DATE), 2491, 6452 , 917092, 'South', 'South Atlantic', 'Mideast' UNION ALL
    SELECT 10,'FL', 'Florida', 'State of Florida', 'Tallahassee', 'Jacksonville', CAST('March 3, 1845' AS DATE), 65755, 170304 , 19317568, 'South', 'South Atlantic', 'Southeast' UNION ALL
    SELECT 11, 'GA', 'Georgia', 'State of Georgia', 'Atlanta', 'Atlanta', CAST('January 2, 1788' AS DATE), 59425, 153909 , 9919945, 'South', 'South Atlantic', 'Georgia' UNION ALL
    SELECT 12, 'HI', 'Hawai''i', 'State of Hawai''i', 'Honolulu', 'Honolulu', CAST('August 21, 1959' AS DATE), 10931, 28311 , 1392313, 'West', 'Pacific', 'Far West' UNION ALL
    SELECT 13, 'ID', 'Idaho', 'State of Idaho', 'Boise', 'Boise', CAST('July 3, 1890' AS DATE), 83642, 216632 , 1595728, 'West', 'Mountain', 'Rocky Mountain' UNION ALL
    SELECT 14, 'IL', 'Illinois', 'State of Illinois', 'Springfield', 'Chicago', CAST('December 3, 1818' AS DATE), 54826, 141998 , 12875255, 'Midwest', 'East North Central', 'Great Lakes' UNION ALL
    SELECT 15, 'IN', 'Indiana', 'State of Indiana', 'Indianapolis', 'Indianapolis', CAST('December 11, 1816' AS DATE), 36418, 94321 , 6537334, 'Midwest', 'East North Central', 'Great Lakes' UNION ALL
    SELECT 16, 'IA', 'Iowa', 'State of Iowa', 'Des Moines', 'Des Moines', CAST('December 28, 1846' AS DATE), 56272, 145743 , 3074186, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 17, 'KS', 'Kansas', 'State of Kansas', 'Topeka', 'Wichita', CAST('January 29, 1861' AS DATE), 82277, 213096 , 2885905, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 18, 'KY', 'Kentucky', 'Commonwealth of Kentucky', 'Frankfort', 'Louisville', CAST('June 1, 1792' AS DATE), 40409, 104659 , 4380415, 'South', 'East South Central', 'Southeast' UNION ALL
    SELECT 19, 'LA', 'Louisiana', 'State of Louisiana', 'Baton Rouge', 'New Orleans', CAST('April 30, 1812' AS DATE), 52271, 135382 , 4601893, 'South', 'West South Central', 'Southeast' UNION ALL
    SELECT 20, 'ME', 'Maine', 'State of Maine', 'Augusta', 'Portland', CAST('March 15, 1820' AS DATE), 35385, 91646 , 1329192, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 21, 'MD', 'Maryland', 'State of Maryland', 'Annapolis', 'Baltimore', CAST('April 28, 1788' AS DATE), 12407, 32133 , 5884563, 'South', 'South Atlantic', 'Mideast' UNION ALL
    SELECT 22, 'MA', 'Massachusetts', 'Commonwealth of Massachusetts', 'Boston', 'Boston', CAST('February 6, 1788' AS DATE), 10554, 27336 , 6646144, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 23, 'MI', 'Michigan', 'State of Michigan', 'Lansing', 'Detroit', CAST('January 26, 1837' AS DATE), 97990, 253793 , 9883360, 'Midwest', 'East North Central', 'Great Lakes' UNION ALL
    SELECT 24, 'MN', 'Minnesota', 'State of Minnesota', 'Saint Paul', 'Minneapolis', CAST('May 11, 1858' AS DATE), 86943, 225181 , 5379139, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 25, 'MS', 'Mississippi', 'State of Mississippi', 'Jackson', 'Jackson', CAST('December 10, 1817' AS DATE), 48434, 125443 , 2984926, 'South', 'East South Central', 'Southeast' UNION ALL
    SELECT 26, 'MO', 'Missouri', 'State of Missouri', 'Jefferson City', 'Kansas City', CAST('August 10, 1821' AS DATE), 69704, 180533 , 6021988, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 27, 'MT', 'Montana', 'State of Montana', 'Helena', 'Billings', CAST('November 8, 1889' AS DATE), 147165, 381156 , 1005141, 'West', 'Mountain', 'Rocky Mountain' UNION ALL
    SELECT 28, 'NE', 'Nebraska', 'State of Nebraska', 'Lincoln', 'Omaha', CAST('March 1, 1867' AS DATE), 77420, 200520 , 1855525, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 29, 'NV', 'Nevada', 'State of Nevada', 'Carson City', 'Las Vegas', CAST('October 31, 1864' AS DATE), 110567, 286367 , 2758931, 'West', 'Mountain', 'Far West' UNION ALL
    SELECT 30, 'NH', 'New Hampshire', 'State of New Hampshire', 'Concord', 'Manchester', CAST('June 21, 1788' AS DATE), 9350, 24217 , 1320718, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 31, 'NJ', 'New Jersey', 'State of New Jersey', 'Trenton', 'Newark', CAST('December 18, 1787' AS DATE), 8729, 22608 , 8864590, 'Northeast', 'Mid-Atlantic', 'Mideast' UNION ALL
    SELECT 32, 'NM', 'New Mexico', 'State of New Mexico', 'Santa Fe', 'Albuquerque', CAST('January 6, 1912' AS DATE), 121697, 315194 , 2085538, 'West', 'Mountain', 'Southwest' UNION ALL
    SELECT 33, 'NY', 'New York', 'State of New York', 'Albany', 'New York City', CAST('July 26, 1788' AS DATE), 54556, 141299 , 19570261, 'Northeast', 'Mid-Atlantic', 'Mideast' UNION ALL
    SELECT 34, 'NC', 'North Carolina', 'State of North Carolina', 'Raleigh', 'Charlotte', CAST('November 21, 1789' AS DATE), 53865, 139509 , 9752073, 'South', 'South Atlantic', 'Southeast' UNION ALL
    SELECT 35, 'ND', 'North Dakota', 'State of North Dakota', 'Bismarck', 'Fargo', CAST('November 2, 1889' AS DATE), 70762, 183272 , 699628, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 36, 'OH', 'Ohio', 'State of Ohio', 'Columbus', 'Columbus', CAST('March 1, 1803' AS DATE), 44825, 116096 , 11544225, 'Midwest', 'East North Central', 'Great Lakes' UNION ALL
    SELECT 37, 'OK', 'Oklahoma', 'State of Oklahoma', 'Oklahoma City', 'Oklahoma City', CAST('November 16, 1907' AS DATE), 69960, 181195 , 3814820, 'South', 'West South Central', 'Southwest' UNION ALL
    SELECT 38, 'OR', 'Oregon', 'State of Oregon', 'Salem', 'Portland', CAST('February 14, 1859' AS DATE), 98466, 255026 , 3899353, 'West', 'Mountain', 'Far West' UNION ALL
    SELECT 39, 'PA', 'Pennsylvania', 'Commonwealth of Pennsylvania', 'Harrisburg', 'Philadelphia', CAST('December 12, 1787' AS DATE), 46055, 119283 , 12763536, 'Northeast', 'Mid-Atlantic', 'Mideast' UNION ALL
    SELECT 40, 'RI', 'Rhode Island', 'State of Rhode Island and Providence Plantations', 'Providence', 'Providence', CAST('May 29, 1790' AS DATE), 1210, 3140 , 1050292, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 41, 'SC', 'South Carolina', 'State of South Carolina', 'Columbia', 'Columbia', CAST('May 23, 1788' AS DATE), 32020, 82931 , 4723723, 'South', 'South Atlantic', 'Southeast' UNION ALL
    SELECT 42, 'SD', 'South Dakota', 'State of South Dakota', 'Pierre', 'Sioux Falls', CAST('November 2, 1889' AS DATE), 77184, 199905 , 833354, 'Midwest', 'West North Central', 'Plains' UNION ALL
    SELECT 43, 'TN', 'Tennessee', 'State of Tennessee', 'Nashville', 'Memphis', CAST('June 1, 1796' AS DATE), 42181, 109247 , 6456243, 'South', 'East South Central', 'Southeast' UNION ALL
    SELECT 44, 'TX', 'Texas', 'State of Texas', 'Austin', 'Houston', CAST('December 29, 1845' AS DATE), 268820, 696241 , 26059203, 'South', 'West South Central', 'Southwest' UNION ALL
    SELECT 45, 'UT', 'Utah', 'State of Utah', 'Salt Lake City', 'Salt Lake City', CAST('January 4, 1896' AS DATE), 84899, 219887 , 2855287, 'West', 'Mountain', 'Rocky Mountain' UNION ALL
    SELECT 46, 'VT', 'Vermont', 'State of Vermont', 'Montpelier', 'Burlington', CAST('March 4, 1791' AS DATE), 9623, 24923 , 626011, 'Northeast', 'New England', 'New England' UNION ALL
    SELECT 47, 'VA', 'Virginia', 'Commonwealth of Virginia', 'Richmond', 'Virginia Beach', CAST('June 25, 1788' AS DATE), 42774, 110785 , 8185867, 'South', 'South Atlantic', 'Southeast' UNION ALL
    SELECT 48, 'WA', 'Washington', 'State of Washington', 'Olympia', 'Seattle', CAST('November 11, 1889' AS DATE), 71362, 184827 , 6897012, 'West', 'Pacific', 'Far West' UNION ALL
    SELECT 49, 'WV', 'West Virginia', 'State of West Virginia', 'Charleston', 'Charleston', CAST('June 20, 1863' AS DATE), 24230, 62755 , 1855413, 'South', 'South Atlantic', 'Southeast' UNION ALL
    SELECT 50, 'WI', 'Wisconsin', 'State of Wisconsin', 'Madison', 'Milwaukee', CAST('May 29, 1848' AS DATE), 65498, 169639 , 5726398, 'Midwest', 'East North Central', 'Great Lakes' UNION ALL
    SELECT 51, 'WY', 'Wyoming', 'State of Wyoming', 'Cheyenne', 'Cheyenne', CAST('July 10, 1890' AS DATE), 97818, 253348 , 576412, 'West', 'Mountain', 'Rocky Mountain'
)
MERGE INTO dbo.dimUSState AS tgt
USING cUSState AS src
ON tgt.USStateID = src.USStateID

WHEN MATCHED THEN UPDATE
SET
    StateCode = src.StateCode
   ,CommonName = src.CommonName
   ,OfficialName = src.OfficialName
   ,CapitalCity = src.CapitalCity
   ,MostPopulatedCity = src.MostPopulatedCity
   ,StatehoodDate = src.StatehoodDate
   ,AreaMi = src.AreaMi
   ,AreaKM = src.AreaKM
   ,[Population] = src.[Population]
   ,CensusBureauRegion = src.CensusBureauRegion
   ,CensusBureauDivision = src.CensusBureauDivision
   ,EconomicAnalysisRegion = src.EconomicAnalysisRegion

WHEN NOT MATCHED THEN INSERT(USStateID, StateCode, CommonName, OfficialName, CapitalCity, MostPopulatedCity, StatehoodDate, AreaMi, AreaKM, [Population], CensusBureauRegion, CensusBureauDivision, EconomicAnalysisRegion)
VALUES(src.USStateID, src.StateCode, src.CommonName, src.OfficialName, src.CapitalCity, src.MostPopulatedCity, src.StatehoodDate, src.AreaMi, src.AreaKM, src.[Population], src.CensusBureauRegion, src.CensusBureauDivision, src.EconomicAnalysisRegion)
;
GO