﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimCountry';

WITH cCountry(CountryID, CountryName, Alpha2Code, Alpha3Code, NumericCode, NumericNum) AS (

	/* Unknown country */
	SELECT -1, 'Unknown', '??', '???', '-01', -1 UNION ALL

	/* All the countries */
	SELECT 4, 'Afghanistan ', 'AF ', 'AFG ', '004', 4 UNION ALL 
	SELECT 248, 'Åland Islands ', 'AX ', 'ALA ', '248', 248 UNION ALL 
	SELECT 8, 'Albania ', 'AL ', 'ALB ', '008', 8 UNION ALL 
	SELECT 12, 'Algeria ', 'DZ ', 'DZA ', '012', 12 UNION ALL 
	SELECT 16, 'American Samoa ', 'AS ', 'ASM ', '016', 16 UNION ALL 
	SELECT 20, 'Andorra ', 'AD ', 'AND ', '020', 20 UNION ALL 
	SELECT 24, 'Angola ', 'AO ', 'AGO ', '024', 24 UNION ALL 
	SELECT 660, 'Anguilla ', 'AI ', 'AIA ', '660', 660 UNION ALL 
	SELECT 10, 'Antarctica ', 'AQ ', 'ATA ', '010', 10 UNION ALL 
	SELECT 28, 'Antigua and Barbuda ', 'AG ', 'ATG ', '028', 28 UNION ALL 
	SELECT 32, 'Argentina ', 'AR ', 'ARG ', '032', 32 UNION ALL 
	SELECT 51, 'Armenia ', 'AM ', 'ARM ', '051', 51 UNION ALL 
	SELECT 533, 'Aruba ', 'AW ', 'ABW ', '533', 533 UNION ALL 
	SELECT 36, 'Australia ', 'AU ', 'AUS ', '036', 36 UNION ALL 
	SELECT 40, 'Austria ', 'AT ', 'AUT ', '040', 40 UNION ALL 
	SELECT 31, 'Azerbaijan ', 'AZ ', 'AZE ', '031', 31 UNION ALL 
	SELECT 44, 'Bahamas ', 'BS ', 'BHS ', '044', 44 UNION ALL 
	SELECT 48, 'Bahrain ', 'BH ', 'BHR ', '048', 48 UNION ALL 
	SELECT 50, 'Bangladesh ', 'BD ', 'BGD ', '050', 50 UNION ALL 
	SELECT 52, 'Barbados ', 'BB ', 'BRB ', '052', 52 UNION ALL 
	SELECT 112, 'Belarus ', 'BY ', 'BLR ', '112', 112 UNION ALL 
	SELECT 56, 'Belgium ', 'BE ', 'BEL ', '056', 56 UNION ALL 
	SELECT 84, 'Belize ', 'BZ ', 'BLZ ', '084', 84 UNION ALL 
	SELECT 204, 'Benin ', 'BJ ', 'BEN ', '204', 204 UNION ALL 
	SELECT 60, 'Bermuda ', 'BM ', 'BMU ', '060', 60 UNION ALL 
	SELECT 64, 'Bhutan ', 'BT ', 'BTN ', '064', 64 UNION ALL 
	SELECT 68, 'Bolivia, Plurinational State of ', 'BO ', 'BOL ', '068', 68 UNION ALL 
	SELECT 535, 'Bonaire, Sint Eustatius and Saba ', 'BQ ', 'BES ', '535', 535 UNION ALL 
	SELECT 70, 'Bosnia and Herzegovina ', 'BA ', 'BIH ', '070', 70 UNION ALL 
	SELECT 72, 'Botswana ', 'BW ', 'BWA ', '072', 72 UNION ALL 
	SELECT 74, 'Bouvet Island ', 'BV ', 'BVT ', '074', 74 UNION ALL 
	SELECT 76, 'Brazil ', 'BR ', 'BRA ', '076', 76 UNION ALL 
	SELECT 86, 'British Indian Ocean Territory ', 'IO ', 'IOT ', '086', 86 UNION ALL 
	SELECT 96, 'Brunei Darussalam ', 'BN ', 'BRN ', '096', 96 UNION ALL 
	SELECT 100, 'Bulgaria ', 'BG ', 'BGR ', '100', 100 UNION ALL 
	SELECT 854, 'Burkina Faso ', 'BF ', 'BFA ', '854', 854 UNION ALL 
	SELECT 108, 'Burundi ', 'BI ', 'BDI ', '108', 108 UNION ALL 
	SELECT 116, 'Cambodia ', 'KH ', 'KHM ', '116', 116 UNION ALL 
	SELECT 120, 'Cameroon ', 'CM ', 'CMR ', '120', 120 UNION ALL 
	SELECT 124, 'Canada ', 'CA ', 'CAN ', '124', 124 UNION ALL 
	SELECT 132, 'Cape Verde ', 'CV ', 'CPV ', '132', 132 UNION ALL 
	SELECT 136, 'Cayman Islands ', 'KY ', 'CYM ', '136', 136 UNION ALL 
	SELECT 140, 'Central African Republic ', 'CF ', 'CAF ', '140', 140 UNION ALL 
	SELECT 148, 'Chad ', 'TD ', 'TCD ', '148', 148 UNION ALL 
	SELECT 152, 'Chile ', 'CL ', 'CHL ', '152', 152 UNION ALL 
	SELECT 156, 'China ', 'CN ', 'CHN ', '156', 156 UNION ALL 
	SELECT 162, 'Christmas Island ', 'CX ', 'CXR ', '162', 162 UNION ALL 
	SELECT 166, 'Cocos (Keeling) Islands ', 'CC ', 'CCK ', '166', 166 UNION ALL 
	SELECT 170, 'Colombia ', 'CO ', 'COL ', '170', 170 UNION ALL 
	SELECT 174, 'Comoros ', 'KM ', 'COM ', '174', 174 UNION ALL 
	SELECT 178, 'Congo ', 'CG ', 'COG ', '178', 178 UNION ALL 
	SELECT 180, 'Congo, the Democratic Republic of the ', 'CD ', 'COD ', '180', 180 UNION ALL 
	SELECT 184, 'Cook Islands ', 'CK ', 'COK ', '184', 184 UNION ALL 
	SELECT 188, 'Costa Rica ', 'CR ', 'CRI ', '188', 188 UNION ALL 
	SELECT 384, 'Côte d''Ivoire ', 'CI ', 'CIV ', '384', 384 UNION ALL 
	SELECT 191, 'Croatia ', 'HR ', 'HRV ', '191', 191 UNION ALL 
	SELECT 192, 'Cuba ', 'CU ', 'CUB ', '192', 192 UNION ALL 
	SELECT 531, 'Curaçao ', 'CW ', 'CUW ', '531', 531 UNION ALL 
	SELECT 196, 'Cyprus ', 'CY ', 'CYP ', '196', 196 UNION ALL 
	SELECT 203, 'Czech Republic ', 'CZ ', 'CZE ', '203', 203 UNION ALL 
	SELECT 208, 'Denmark ', 'DK ', 'DNK ', '208', 208 UNION ALL 
	SELECT 262, 'Djibouti ', 'DJ ', 'DJI ', '262', 262 UNION ALL 
	SELECT 212, 'Dominica ', 'DM ', 'DMA ', '212', 212 UNION ALL 
	SELECT 214, 'Dominican Republic ', 'DO ', 'DOM ', '214', 214 UNION ALL 
	SELECT 218, 'Ecuador ', 'EC ', 'ECU ', '218', 218 UNION ALL 
	SELECT 818, 'Egypt ', 'EG ', 'EGY ', '818', 818 UNION ALL 
	SELECT 222, 'El Salvador ', 'SV ', 'SLV ', '222', 222 UNION ALL 
	SELECT 226, 'Equatorial Guinea ', 'GQ ', 'GNQ ', '226', 226 UNION ALL 
	SELECT 232, 'Eritrea ', 'ER ', 'ERI ', '232', 232 UNION ALL 
	SELECT 233, 'Estonia ', 'EE ', 'EST ', '233', 233 UNION ALL 
	SELECT 231, 'Ethiopia ', 'ET ', 'ETH ', '231', 231 UNION ALL 
	SELECT 238, 'Falkland Islands (Malvinas) ', 'FK ', 'FLK ', '238', 238 UNION ALL 
	SELECT 234, 'Faroe Islands ', 'FO ', 'FRO ', '234', 234 UNION ALL 
	SELECT 242, 'Fiji ', 'FJ ', 'FJI ', '242', 242 UNION ALL 
	SELECT 246, 'Finland ', 'FI ', 'FIN ', '246', 246 UNION ALL 
	SELECT 250, 'France ', 'FR ', 'FRA ', '250', 250 UNION ALL 
	SELECT 254, 'French Guiana ', 'GF ', 'GUF ', '254', 254 UNION ALL 
	SELECT 258, 'French Polynesia ', 'PF ', 'PYF ', '258', 258 UNION ALL 
	SELECT 260, 'French Southern Territories ', 'TF ', 'ATF ', '260', 260 UNION ALL 
	SELECT 266, 'Gabon ', 'GA ', 'GAB ', '266', 266 UNION ALL 
	SELECT 270, 'Gambia ', 'GM ', 'GMB ', '270', 270 UNION ALL 
	SELECT 268, 'Georgia ', 'GE ', 'GEO ', '268', 268 UNION ALL 
	SELECT 276, 'Germany ', 'DE ', 'DEU ', '276', 276 UNION ALL 
	SELECT 288, 'Ghana ', 'GH ', 'GHA ', '288', 288 UNION ALL 
	SELECT 292, 'Gibraltar ', 'GI ', 'GIB ', '292', 292 UNION ALL 
	SELECT 300, 'Greece ', 'GR ', 'GRC ', '300', 300 UNION ALL 
	SELECT 304, 'Greenland ', 'GL ', 'GRL ', '304', 304 UNION ALL 
	SELECT 308, 'Grenada ', 'GD ', 'GRD ', '308', 308 UNION ALL 
	SELECT 312, 'Guadeloupe ', 'GP ', 'GLP ', '312', 312 UNION ALL 
	SELECT 316, 'Guam ', 'GU ', 'GUM ', '316', 316 UNION ALL 
	SELECT 320, 'Guatemala ', 'GT ', 'GTM ', '320', 320 UNION ALL 
	SELECT 831, 'Guernsey ', 'GG ', 'GGY ', '831', 831 UNION ALL 
	SELECT 324, 'Guinea ', 'GN ', 'GIN ', '324', 324 UNION ALL 
	SELECT 624, 'Guinea-Bissau ', 'GW ', 'GNB ', '624', 624 UNION ALL 
	SELECT 328, 'Guyana ', 'GY ', 'GUY ', '328', 328 UNION ALL 
	SELECT 332, 'Haiti ', 'HT ', 'HTI ', '332', 332 UNION ALL 
	SELECT 334, 'Heard Island and McDonald Islands ', 'HM ', 'HMD ', '334', 334 UNION ALL 
	SELECT 336, 'Holy See (Vatican City State) ', 'VA ', 'VAT ', '336', 336 UNION ALL 
	SELECT 340, 'Honduras ', 'HN ', 'HND ', '340', 340 UNION ALL 
	SELECT 344, 'Hong Kong ', 'HK ', 'HKG ', '344', 344 UNION ALL 
	SELECT 348, 'Hungary ', 'HU ', 'HUN ', '348', 348 UNION ALL 
	SELECT 352, 'Iceland ', 'IS ', 'ISL ', '352', 352 UNION ALL 
	SELECT 356, 'India ', 'IN ', 'IND ', '356', 356 UNION ALL 
	SELECT 360, 'Indonesia ', 'ID ', 'IDN ', '360', 360 UNION ALL 
	SELECT 364, 'Iran, Islamic Republic of ', 'IR ', 'IRN ', '364', 364 UNION ALL 
	SELECT 368, 'Iraq ', 'IQ ', 'IRQ ', '368', 368 UNION ALL 
	SELECT 372, 'Ireland ', 'IE ', 'IRL ', '372', 372 UNION ALL 
	SELECT 833, 'Isle of Man ', 'IM ', 'IMN ', '833', 833 UNION ALL 
	SELECT 376, 'Israel ', 'IL ', 'ISR ', '376', 376 UNION ALL 
	SELECT 380, 'Italy ', 'IT ', 'ITA ', '380', 380 UNION ALL 
	SELECT 388, 'Jamaica ', 'JM ', 'JAM ', '388', 388 UNION ALL 
	SELECT 392, 'Japan ', 'JP ', 'JPN ', '392', 392 UNION ALL 
	SELECT 832, 'Jersey ', 'JE ', 'JEY ', '832', 832 UNION ALL 
	SELECT 400, 'Jordan ', 'JO ', 'JOR ', '400', 400 UNION ALL 
	SELECT 398, 'Kazakhstan ', 'KZ ', 'KAZ ', '398', 398 UNION ALL 
	SELECT 404, 'Kenya ', 'KE ', 'KEN ', '404', 404 UNION ALL 
	SELECT 296, 'Kiribati ', 'KI ', 'KIR ', '296', 296 UNION ALL 
	SELECT 408, 'Korea, Democratic People''s Republic of ', 'KP ', 'PRK ', '408', 408 UNION ALL 
	SELECT 410, 'Korea, Republic of ', 'KR ', 'KOR ', '410', 410 UNION ALL 
	SELECT 414, 'Kuwait ', 'KW ', 'KWT ', '414', 414 UNION ALL 
	SELECT 417, 'Kyrgyzstan ', 'KG ', 'KGZ ', '417', 417 UNION ALL 
	SELECT 418, 'Lao People''s Democratic Republic ', 'LA ', 'LAO ', '418', 418 UNION ALL 
	SELECT 428, 'Latvia ', 'LV ', 'LVA ', '428', 428 UNION ALL 
	SELECT 422, 'Lebanon ', 'LB ', 'LBN ', '422', 422 UNION ALL 
	SELECT 426, 'Lesotho ', 'LS ', 'LSO ', '426', 426 UNION ALL 
	SELECT 430, 'Liberia ', 'LR ', 'LBR ', '430', 430 UNION ALL 
	SELECT 434, 'Libya ', 'LY ', 'LBY ', '434', 434 UNION ALL 
	SELECT 438, 'Liechtenstein ', 'LI ', 'LIE ', '438', 438 UNION ALL 
	SELECT 440, 'Lithuania ', 'LT ', 'LTU ', '440', 440 UNION ALL 
	SELECT 442, 'Luxembourg ', 'LU ', 'LUX ', '442', 442 UNION ALL 
	SELECT 446, 'Macao ', 'MO ', 'MAC ', '446', 446 UNION ALL 
	SELECT 807, 'Macedonia, The Former Yugoslav Republic of ', 'MK ', 'MKD ', '807', 807 UNION ALL 
	SELECT 450, 'Madagascar ', 'MG ', 'MDG ', '450', 450 UNION ALL 
	SELECT 454, 'Malawi ', 'MW ', 'MWI ', '454', 454 UNION ALL 
	SELECT 458, 'Malaysia ', 'MY ', 'MYS ', '458', 458 UNION ALL 
	SELECT 462, 'Maldives ', 'MV ', 'MDV ', '462', 462 UNION ALL 
	SELECT 466, 'Mali ', 'ML ', 'MLI ', '466', 466 UNION ALL 
	SELECT 470, 'Malta ', 'MT ', 'MLT ', '470', 470 UNION ALL 
	SELECT 584, 'Marshall Islands ', 'MH ', 'MHL ', '584', 584 UNION ALL 
	SELECT 474, 'Martinique ', 'MQ ', 'MTQ ', '474', 474 UNION ALL 
	SELECT 478, 'Mauritania ', 'MR ', 'MRT ', '478', 478 UNION ALL 
	SELECT 480, 'Mauritius ', 'MU ', 'MUS ', '480', 480 UNION ALL 
	SELECT 175, 'Mayotte ', 'YT ', 'MYT ', '175', 175 UNION ALL 
	SELECT 484, 'Mexico ', 'MX ', 'MEX ', '484', 484 UNION ALL 
	SELECT 583, 'Micronesia, Federated States of ', 'FM ', 'FSM ', '583', 583 UNION ALL 
	SELECT 498, 'Moldova, Republic of ', 'MD ', 'MDA ', '498', 498 UNION ALL 
	SELECT 492, 'Monaco ', 'MC ', 'MCO ', '492', 492 UNION ALL 
	SELECT 496, 'Mongolia ', 'MN ', 'MNG ', '496', 496 UNION ALL 
	SELECT 499, 'Montenegro ', 'ME ', 'MNE ', '499', 499 UNION ALL 
	SELECT 500, 'Montserrat ', 'MS ', 'MSR ', '500', 500 UNION ALL 
	SELECT 504, 'Morocco ', 'MA ', 'MAR ', '504', 504 UNION ALL 
	SELECT 508, 'Mozambique ', 'MZ ', 'MOZ ', '508', 508 UNION ALL 
	SELECT 104, 'Myanmar ', 'MM ', 'MMR ', '104', 104 UNION ALL 
	SELECT 516, 'Namibia ', 'NA ', 'NAM ', '516', 516 UNION ALL 
	SELECT 520, 'Nauru ', 'NR ', 'NRU ', '520', 520 UNION ALL 
	SELECT 524, 'Nepal ', 'NP ', 'NPL ', '524', 524 UNION ALL 
	SELECT 528, 'Netherlands ', 'NL ', 'NLD ', '528', 528 UNION ALL 
	SELECT 540, 'New Caledonia ', 'NC ', 'NCL ', '540', 540 UNION ALL 
	SELECT 554, 'New Zealand ', 'NZ ', 'NZL ', '554', 554 UNION ALL 
	SELECT 558, 'Nicaragua ', 'NI ', 'NIC ', '558', 558 UNION ALL 
	SELECT 562, 'Niger ', 'NE ', 'NER ', '562', 562 UNION ALL 
	SELECT 566, 'Nigeria ', 'NG ', 'NGA ', '566', 566 UNION ALL 
	SELECT 570, 'Niue ', 'NU ', 'NIU ', '570', 570 UNION ALL 
	SELECT 574, 'Norfolk Island ', 'NF ', 'NFK ', '574', 574 UNION ALL 
	SELECT 580, 'Northern Mariana Islands ', 'MP ', 'MNP ', '580', 580 UNION ALL 
	SELECT 578, 'Norway ', 'NO ', 'NOR ', '578', 578 UNION ALL 
	SELECT 512, 'Oman ', 'OM ', 'OMN ', '512', 512 UNION ALL 
	SELECT 586, 'Pakistan ', 'PK ', 'PAK ', '586', 586 UNION ALL 
	SELECT 585, 'Palau ', 'PW ', 'PLW ', '585', 585 UNION ALL 
	SELECT 275, 'Palestinian Territory, Occupied ', 'PS ', 'PSE ', '275', 275 UNION ALL 
	SELECT 591, 'Panama ', 'PA ', 'PAN ', '591', 591 UNION ALL 
	SELECT 598, 'Papua New Guinea ', 'PG ', 'PNG ', '598', 598 UNION ALL 
	SELECT 600, 'Paraguay ', 'PY ', 'PRY ', '600', 600 UNION ALL 
	SELECT 604, 'Peru ', 'PE ', 'PER ', '604', 604 UNION ALL 
	SELECT 608, 'Philippines ', 'PH ', 'PHL ', '608', 608 UNION ALL 
	SELECT 612, 'Pitcairn ', 'PN ', 'PCN ', '612', 612 UNION ALL 
	SELECT 616, 'Poland ', 'PL ', 'POL ', '616', 616 UNION ALL 
	SELECT 620, 'Portugal ', 'PT ', 'PRT ', '620', 620 UNION ALL 
	SELECT 630, 'Puerto Rico ', 'PR ', 'PRI ', '630', 630 UNION ALL 
	SELECT 634, 'Qatar ', 'QA ', 'QAT ', '634', 634 UNION ALL 
	SELECT 638, 'Réunion ', 'RE ', 'REU ', '638', 638 UNION ALL 
	SELECT 642, 'Romania ', 'RO ', 'ROU ', '642', 642 UNION ALL 
	SELECT 643, 'Russian Federation ', 'RU ', 'RUS ', '643', 643 UNION ALL 
	SELECT 646, 'Rwanda ', 'RW ', 'RWA ', '646', 646 UNION ALL 
	SELECT 652, 'Saint Barthélemy ', 'BL ', 'BLM ', '652', 652 UNION ALL 
	SELECT 654, 'Saint Helena, Ascension and Tristan da Cunha ', 'SH ', 'SHN ', '654', 654 UNION ALL 
	SELECT 659, 'Saint Kitts and Nevis ', 'KN ', 'KNA ', '659', 659 UNION ALL 
	SELECT 662, 'Saint Lucia ', 'LC ', 'LCA ', '662', 662 UNION ALL 
	SELECT 663, 'Saint Martin (French part) ', 'MF ', 'MAF ', '663', 663 UNION ALL 
	SELECT 666, 'Saint Pierre and Miquelon ', 'PM ', 'SPM ', '666', 666 UNION ALL 
	SELECT 670, 'Saint Vincent and the Grenadines ', 'VC ', 'VCT ', '670', 670 UNION ALL 
	SELECT 882, 'Samoa ', 'WS ', 'WSM ', '882', 882 UNION ALL 
	SELECT 674, 'San Marino ', 'SM ', 'SMR ', '674', 674 UNION ALL 
	SELECT 678, 'Sao Tome and Principe ', 'ST ', 'STP ', '678', 678 UNION ALL 
	SELECT 682, 'Saudi Arabia ', 'SA ', 'SAU ', '682', 682 UNION ALL 
	SELECT 686, 'Senegal ', 'SN ', 'SEN ', '686', 686 UNION ALL 
	SELECT 688, 'Serbia ', 'RS ', 'SRB ', '688', 688 UNION ALL 
	SELECT 690, 'Seychelles ', 'SC ', 'SYC ', '690', 690 UNION ALL 
	SELECT 694, 'Sierra Leone ', 'SL ', 'SLE ', '694', 694 UNION ALL 
	SELECT 702, 'Singapore ', 'SG ', 'SGP ', '702', 702 UNION ALL 
	SELECT 534, 'Sint Maarten (Dutch part) ', 'SX ', 'SXM ', '534', 534 UNION ALL 
	SELECT 703, 'Slovakia ', 'SK ', 'SVK ', '703', 703 UNION ALL 
	SELECT 705, 'Slovenia ', 'SI ', 'SVN ', '705', 705 UNION ALL 
	SELECT 90, 'Solomon Islands ', 'SB ', 'SLB ', '090', 90 UNION ALL 
	SELECT 706, 'Somalia ', 'SO ', 'SOM ', '706', 706 UNION ALL 
	SELECT 710, 'South Africa ', 'ZA ', 'ZAF ', '710', 710 UNION ALL 
	SELECT 239, 'South Georgia and the South Sandwich Islands ', 'GS ', 'SGS ', '239', 239 UNION ALL 
	SELECT 728, 'South Sudan ', 'SS ', 'SSD ', '728', 728 UNION ALL 
	SELECT 724, 'Spain ', 'ES ', 'ESP ', '724', 724 UNION ALL 
	SELECT 144, 'Sri Lanka ', 'LK ', 'LKA ', '144', 144 UNION ALL 
	SELECT 729, 'Sudan ', 'SD ', 'SDN ', '729', 729 UNION ALL 
	SELECT 740, 'Suriname ', 'SR ', 'SUR ', '740', 740 UNION ALL 
	SELECT 744, 'Svalbard and Jan Mayen ', 'SJ ', 'SJM ', '744', 744 UNION ALL 
	SELECT 748, 'Swaziland ', 'SZ ', 'SWZ ', '748', 748 UNION ALL 
	SELECT 752, 'Sweden ', 'SE ', 'SWE ', '752', 752 UNION ALL 
	SELECT 756, 'Switzerland ', 'CH ', 'CHE ', '756', 756 UNION ALL 
	SELECT 760, 'Syrian Arab Republic ', 'SY ', 'SYR ', '760', 760 UNION ALL 
	SELECT 158, 'Taiwan, Province of China ', 'TW ', 'TWN ', '158', 158 UNION ALL 
	SELECT 762, 'Tajikistan ', 'TJ ', 'TJK ', '762', 762 UNION ALL 
	SELECT 834, 'Tanzania, United Republic of ', 'TZ ', 'TZA ', '834', 834 UNION ALL 
	SELECT 764, 'Thailand ', 'TH ', 'THA ', '764', 764 UNION ALL 
	SELECT 626, 'Timor-Leste ', 'TL ', 'TLS ', '626', 626 UNION ALL 
	SELECT 768, 'Togo ', 'TG ', 'TGO ', '768', 768 UNION ALL 
	SELECT 772, 'Tokelau ', 'TK ', 'TKL ', '772', 772 UNION ALL 
	SELECT 776, 'Tonga ', 'TO ', 'TON ', '776', 776 UNION ALL 
	SELECT 780, 'Trinidad and Tobago ', 'TT ', 'TTO ', '780', 780 UNION ALL 
	SELECT 788, 'Tunisia ', 'TN ', 'TUN ', '788', 788 UNION ALL 
	SELECT 792, 'Turkey ', 'TR ', 'TUR ', '792', 792 UNION ALL 
	SELECT 795, 'Turkmenistan ', 'TM ', 'TKM ', '795', 795 UNION ALL 
	SELECT 796, 'Turks and Caicos Islands ', 'TC ', 'TCA ', '796', 796 UNION ALL 
	SELECT 798, 'Tuvalu ', 'TV ', 'TUV ', '798', 798 UNION ALL 
	SELECT 800, 'Uganda ', 'UG ', 'UGA ', '800', 800 UNION ALL 
	SELECT 804, 'Ukraine ', 'UA ', 'UKR ', '804', 804 UNION ALL 
	SELECT 784, 'United Arab Emirates ', 'AE ', 'ARE ', '784', 784 UNION ALL 
	SELECT 826, 'United Kingdom ', 'GB ', 'GBR ', '826', 826 UNION ALL 
	SELECT 840, 'United States ', 'US ', 'USA ', '840', 840 UNION ALL 
	SELECT 581, 'United States Minor Outlying Islands ', 'UM ', 'UMI ', '581', 581 UNION ALL 
	SELECT 858, 'Uruguay ', 'UY ', 'URY ', '858', 858 UNION ALL 
	SELECT 860, 'Uzbekistan ', 'UZ ', 'UZB ', '860', 860 UNION ALL 
	SELECT 548, 'Vanuatu ', 'VU ', 'VUT ', '548', 548 UNION ALL 
	SELECT 862, 'Venezuela, Bolivarian Republic of ', 'VE ', 'VEN ', '862', 862 UNION ALL 
	SELECT 704, 'Viet Nam ', 'VN ', 'VNM ', '704', 704 UNION ALL 
	SELECT 92, 'Virgin Islands, British ', 'VG ', 'VGB ', '092', 92 UNION ALL 
	SELECT 850, 'Virgin Islands, U.S. ', 'VI ', 'VIR ', '850', 850 UNION ALL 
	SELECT 876, 'Wallis and Futuna ', 'WF ', 'WLF ', '876', 876 UNION ALL 
	SELECT 732, 'Western Sahara ', 'EH ', 'ESH ', '732', 732 UNION ALL 
	SELECT 887, 'Yemen ', 'YE ', 'YEM ', '887', 887 UNION ALL 
	SELECT 894, 'Zambia ', 'ZM ', 'ZMB ', '894', 894 UNION ALL 
	SELECT 716, 'Zimbabwe ', 'ZW ', 'ZWE ', '716', 716 

)
MERGE INTO dbo.[dimCountry] AS tgt
USING cCountry AS src
ON tgt.CountryID = src.CountryID

WHEN MATCHED THEN UPDATE
SET	CountryName		= src.CountryNAme
	,Alpha2Code		= src.Alpha2Code
	,Alpha3Code		= src.Alpha3Code
	,NumericCode	= src.NumericCode
	,NumericNum		= src.NumericNum

WHEN NOT MATCHED THEN
INSERT(CountryID, CountryName, Alpha2Code, Alpha3Code, NumericCode, NumericNum)
VALUES(src.CountryID, src.CountryName, src.Alpha2Code, src.Alpha3Code, src.NumericCode, src.NumericNum)
;
GO