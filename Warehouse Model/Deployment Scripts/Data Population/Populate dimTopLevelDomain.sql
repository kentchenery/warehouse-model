﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimTopLevelDomain';

/* Unknown record */
SET IDENTITY_INSERT dbo.dimTopLevelDomain ON;

WITH cUnknownTLD(TopLevelDomainID, TopLevelDomainCode, DomainType, SponsoringOrganisation) AS (

	SELECT -1, 'Unknown', 'Unknown', 'Unknown'

)
MERGE INTO dbo.dimTopLevelDomain AS tgt
USING cUnknownTLD AS src
ON tgt.TopLevelDomainID = src.TopLevelDomainID

WHEN MATCHED THEN UPDATE
SET	TopLevelDomainCode		= src.TopLevelDomainCode
	,DomainType				= src.DomainType
	,SponsoringOrganisation	= src.SponsoringOrganisation

WHEN NOT MATCHED THEN
INSERT(TopLevelDomainID, TopLevelDomainCode, DomainType, SponsoringOrganisation)
VALUES(src.TopLevelDomainID, src.TopLevelDomainCode, src.DomainType, src.SponsoringOrganisation)
;

SET IDENTITY_INSERT dbo.dimTopLevelDomain OFF;

/* All the domains */
WITH cTLDs(TopLevelDomainCode, DomainType, SponsoringOrganisation) AS (

	SELECT N'.ac', N'Country code', N'Network Information Center (AC Domain Registry) c/o Cable and Wireless (Ascension Island)' UNION ALL
	SELECT N'.ad', N'Country code', N'Andorra Telecom' UNION ALL
	SELECT N'.ae', N'Country code', N'Telecommunication Regulatory Authority (TRA)' UNION ALL
	SELECT N'.aero', N'Sponsored', N'Societe Internationale de Telecommunications Aeronautique (SITA INC USA)' UNION ALL
	SELECT N'.af', N'Country code', N'Ministry of Communications and IT' UNION ALL
	SELECT N'.ag', N'Country code', N'UHSA School of Medicine' UNION ALL
	SELECT N'.ai', N'Country code', N'Government of Anguilla' UNION ALL
	SELECT N'.al', N'Country code', N'Electronic and Postal Communications Authority - AKEP' UNION ALL
	SELECT N'.am', N'Country code', N'Internet Society' UNION ALL
	SELECT N'.an', N'Country code', N'University of The Netherlands Antilles' UNION ALL
	SELECT N'.ao', N'Country code', N'Faculdade de Engenharia da Universidade Agostinho Neto' UNION ALL
	SELECT N'.aq', N'Country code', N'Mott and Associates' UNION ALL
	SELECT N'.ar', N'Country code', N'Presidencia de la Nación – Secretaría Legal y Técnica' UNION ALL
	SELECT N'.arpa', N'Infrastructure', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.as', N'Country code', N'AS Domain Registry' UNION ALL
	SELECT N'.asia', N'Sponsored', N'DotAsia Organisation Ltd.' UNION ALL
	SELECT N'.at', N'Country code', N'nic.at GmbH' UNION ALL
	SELECT N'.au', N'Country code', N'.au Domain Administration (auDA)' UNION ALL
	SELECT N'.aw', N'Country code', N'SETAR' UNION ALL
	SELECT N'.ax', N'Country code', N'Ålands landskapsregering' UNION ALL
	SELECT N'.az', N'Country code', N'IntraNS' UNION ALL
	SELECT N'.ba', N'Country code', N'Universtiy Telinformatic Centre (UTIC)' UNION ALL
	SELECT N'.bb', N'Country code', N'Government of Barbados Ministry of Economic Affairs and Development Telecommunications Unit' UNION ALL
	SELECT N'.bd', N'Country code', N'Ministry of Post & Telecommunications Bangladesh Secretariat' UNION ALL
	SELECT N'.be', N'Country code', N'DNS BE vzw/asbl' UNION ALL
	SELECT N'.bf', N'Country code', N'ARCE-AutoritÈ de RÈgulation des Communications Electroniques' UNION ALL
	SELECT N'.bg', N'Country code', N'Register.BG' UNION ALL
	SELECT N'.bh', N'Country code', N'Telecommunications Regulatory Authority (TRA)' UNION ALL
	SELECT N'.bi', N'Country code', N'Centre National de l''Informatique' UNION ALL
	SELECT N'.biz', N'Generic (restricted)', N'NeuStar, Inc.' UNION ALL
	SELECT N'.bj', N'Country code', N'Benin Telecoms S.A.' UNION ALL
	SELECT N'.bl', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.bm', N'Country code', N'Registry General Ministry of Labour and Immigration' UNION ALL
	SELECT N'.bn', N'Country code', N'Telekom Brunei Berhad' UNION ALL
	SELECT N'.bo', N'Country code', N'Agencia para el Desarrollo de la Información de la Sociedad en Bolivia' UNION ALL
	SELECT N'.bq', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.br', N'Country code', N'Comite Gestor da Internet no Brasil' UNION ALL
	SELECT N'.bs', N'Country code', N'The College of the Bahamas' UNION ALL
	SELECT N'.bt', N'Country code', N'Ministry of Information and Communications' UNION ALL
	SELECT N'.bv', N'Country code', N'UNINETT Norid A/S' UNION ALL
	SELECT N'.bw', N'Country code', N'University of Botswana' UNION ALL
	SELECT N'.by', N'Country code', N'Reliable Software Inc.' UNION ALL
	SELECT N'.bz', N'Country code', N'University of Belize' UNION ALL
	SELECT N'.ca', N'Country code', N'Canadian Internet Registration Authority (CIRA) Autorite Canadienne pour les Enregistrements Internet (ACEI)' UNION ALL
	SELECT N'.cat', N'Sponsored', N'Fundacio puntCAT' UNION ALL
	SELECT N'.cc', N'Country code', N'eNIC Cocos (Keeling) Islands Pty. Ltd. d/b/a Island Internet Services' UNION ALL
	SELECT N'.cd', N'Country code', N'Office Congolais des Postes et Télécommunications - OCPT' UNION ALL
	SELECT N'.cf', N'Country code', N'Societe Centrafricaine de Telecommunications (SOCATEL)' UNION ALL
	SELECT N'.cg', N'Country code', N'ONPT Congo and Interpoint Switzerland' UNION ALL
	SELECT N'.ch', N'Country code', N'SWITCH The Swiss Education & Research Network' UNION ALL
	SELECT N'.ci', N'Country code', N'INP-HB Institut National Polytechnique Felix Houphouet Boigny' UNION ALL
	SELECT N'.ck', N'Country code', N'Telecom Cook Islands Ltd.' UNION ALL
	SELECT N'.cl', N'Country code', N'NIC Chile (University of Chile)' UNION ALL
	SELECT N'.cm', N'Country code', N'Cameroon Telecommunications (CAMTEL)' UNION ALL
	SELECT N'.cn', N'Country code', N'Computer Network Information Center, Chinese Academy of Sciences' UNION ALL
	SELECT N'.co', N'Country code', N'.CO Internet S.A.S.' UNION ALL
	SELECT N'.com', N'Generic', N'VeriSign Global Registry Services' UNION ALL
	SELECT N'.coop', N'Sponsored', N'DotCooperation LLC' UNION ALL
	SELECT N'.cr', N'Country code', N'National Academy of Sciences Academia Nacional de Ciencias' UNION ALL
	SELECT N'.cu', N'Country code', N'CENIAInternet Industria y San Jose Capitolio Nacional' UNION ALL
	SELECT N'.cv', N'Country code', N'Agência Nacional das Comunicações (ANAC)' UNION ALL
	SELECT N'.cw', N'Country code', N'University of the Netherlands Antilles' UNION ALL
	SELECT N'.cx', N'Country code', N'Christmas Island Internet Administration Limited' UNION ALL
	SELECT N'.cy', N'Country code', N'University of Cyprus' UNION ALL
	SELECT N'.cz', N'Country code', N'CZ.NIC, z.s.p.o' UNION ALL
	SELECT N'.de', N'Country code', N'DENIC eG' UNION ALL
	SELECT N'.dj', N'Country code', N'Djibouti Telecom S.A' UNION ALL
	SELECT N'.dk', N'Country code', N'Dansk Internet Forum' UNION ALL
	SELECT N'.dm', N'Country code', N'DotDM Corporation' UNION ALL
	SELECT N'.do', N'Country code', N'Pontificia Universidad Catolica Madre y Maestra Recinto Santo Tomas de Aquino' UNION ALL
	SELECT N'.dz', N'Country code', N'CERIST' UNION ALL
	SELECT N'.ec', N'Country code', N'NIC.EC (NICEC) S.A.' UNION ALL
	SELECT N'.edu', N'Sponsored', N'EDUCAUSE' UNION ALL
	SELECT N'.ee', N'Country code', N'National Institute of Chemical Physics and Biophysics' UNION ALL
	SELECT N'.eg', N'Country code', N'Egyptian Universities Network (EUN) Supreme Council of Universities' UNION ALL
	SELECT N'.eh', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.er', N'Country code', N'Eritrea Telecommunication Services Corporation (EriTel)' UNION ALL
	SELECT N'.es', N'Country code', N'Red.es' UNION ALL
	SELECT N'.et', N'Country code', N'Ethio telecom' UNION ALL
	SELECT N'.eu', N'Country code', N'EURid vzw/asbl' UNION ALL
	SELECT N'.fi', N'Country code', N'Finnish Communications Regulatory Authority' UNION ALL
	SELECT N'.fj', N'Country code', N'The University of the South Pacific IT Services' UNION ALL
	SELECT N'.fk', N'Country code', N'Falkland Islands Government' UNION ALL
	SELECT N'.fm', N'Country code', N'FSM Telecommunications Corporation' UNION ALL
	SELECT N'.fo', N'Country code', N'FO Council' UNION ALL
	SELECT N'.fr', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.ga', N'Country code', N'Gabon Telecom' UNION ALL
	SELECT N'.gb', N'Country code', N'Reserved Domain - IANA' UNION ALL
	SELECT N'.gd', N'Country code', N'The National Telecommunications Regulatory Commission (NTRC)' UNION ALL
	SELECT N'.ge', N'Country code', N'Caucasus Online' UNION ALL
	SELECT N'.gf', N'Country code', N'Net Plus' UNION ALL
	SELECT N'.gg', N'Country code', N'Island Networks Ltd.' UNION ALL
	SELECT N'.gh', N'Country code', N'Network Computer Systems Limited' UNION ALL
	SELECT N'.gi', N'Country code', N'Sapphire Networks' UNION ALL
	SELECT N'.gl', N'Country code', N'TELE Greenland A/S' UNION ALL
	SELECT N'.gm', N'Country code', N'GM-NIC' UNION ALL
	SELECT N'.gn', N'Country code', N'Centre National des Sciences Halieutiques de Boussoura' UNION ALL
	SELECT N'.gov', N'Sponsored', N'General Services Administration Attn: QTDC, 2E08 (.gov Domain Registration)' UNION ALL
	SELECT N'.gp', N'Country code', N'Networking Technologies Group' UNION ALL
	SELECT N'.gq', N'Country code', N'GETESA' UNION ALL
	SELECT N'.gr', N'Country code', N'ICS-FORTH GR' UNION ALL
	SELECT N'.gs', N'Country code', N'Government of South Georgia and South Sandwich Islands (GSGSSI)' UNION ALL
	SELECT N'.gt', N'Country code', N'Universidad del Valle de Guatemala' UNION ALL
	SELECT N'.gu', N'Country code', N'University of Guam Computer Center' UNION ALL
	SELECT N'.gw', N'Country code', N'Fundação IT & MEDIA Universidade de Bissao' UNION ALL
	SELECT N'.gy', N'Country code', N'University of Guyana' UNION ALL
	SELECT N'.hk', N'Country code', N'Hong Kong Internet Registration Corporation Ltd.' UNION ALL
	SELECT N'.hm', N'Country code', N'HM Domain Registry' UNION ALL
	SELECT N'.hn', N'Country code', N'Red de Desarrollo Sostenible Honduras' UNION ALL
	SELECT N'.hr', N'Country code', N'CARNet - Croatian Academic and Research Network' UNION ALL
	SELECT N'.ht', N'Country code', N'Consortium FDS/RDDH' UNION ALL
	SELECT N'.hu', N'Country code', N'Council of Hungarian Internet Providers (CHIP)' UNION ALL
	SELECT N'.id', N'Country code', N'IDNIC-PPAU Mikroelektronika' UNION ALL
	SELECT N'.ie', N'Country code', N'University College Dublin Computing Services Computer Centre' UNION ALL
	SELECT N'.il', N'Country code', N'Internet Society of Israel' UNION ALL
	SELECT N'.im', N'Country code', N'Isle of Man Government' UNION ALL
	SELECT N'.in', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.info', N'Generic', N'Afilias Limited' UNION ALL
	SELECT N'.int', N'Sponsored', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.io', N'Country code', N'IO Top Level Domain Registry Cable and Wireless' UNION ALL
	SELECT N'.iq', N'Country code', N'Communications and Media Commission (CMC)' UNION ALL
	SELECT N'.ir', N'Country code', N'Institute for Research in Fundamental Sciences' UNION ALL
	SELECT N'.is', N'Country code', N'ISNIC - Internet Iceland ltd.' UNION ALL
	SELECT N'.it', N'Country code', N'IIT - CNR' UNION ALL
	SELECT N'.je', N'Country code', N'Island Networks (Jersey) Ltd.' UNION ALL
	SELECT N'.jm', N'Country code', N'University of West Indies' UNION ALL
	SELECT N'.jo', N'Country code', N'National Information Technology Center (NITC)' UNION ALL
	SELECT N'.jobs', N'Sponsored', N'Employ Media LLC' UNION ALL
	SELECT N'.jp', N'Country code', N'Japan Registry Services Co., Ltd.' UNION ALL
	SELECT N'.ke', N'Country code', N'Kenya Network Information Center (KeNIC)' UNION ALL
	SELECT N'.kg', N'Country code', N'AsiaInfo Telecommunication Enterprise' UNION ALL
	SELECT N'.kh', N'Country code', N'Ministry of Post and Telecommunications' UNION ALL
	SELECT N'.ki', N'Country code', N'Ministry of Communications, Transport, and Tourism Development' UNION ALL
	SELECT N'.km', N'Country code', N'Comores Telecom' UNION ALL
	SELECT N'.kn', N'Country code', N'Ministry of Finance, Sustainable Development Information & Technology' UNION ALL
	SELECT N'.kp', N'Country code', N'Star Joint Venture Company' UNION ALL
	SELECT N'.kr', N'Country code', N'Korea Internet & Security Agency (KISA)' UNION ALL
	SELECT N'.kw', N'Country code', N'Ministry of Communications' UNION ALL
	SELECT N'.ky', N'Country code', N'The Information and Communications Technology Authority' UNION ALL
	SELECT N'.kz', N'Country code', N'Association of IT Companies of Kazakhstan' UNION ALL
	SELECT N'.la', N'Country code', N'Lao National Internet Committee (LANIC), Ministry of Posts and Telecommunications' UNION ALL
	SELECT N'.lb', N'Country code', N'American University of Beirut Computing and Networking Services' UNION ALL
	SELECT N'.lc', N'Country code', N'University of Puerto Rico' UNION ALL
	SELECT N'.li', N'Country code', N'Universitaet Liechtenstein' UNION ALL
	SELECT N'.lk', N'Country code', N'Council for Information Technology LK Domain Registrar' UNION ALL
	SELECT N'.lr', N'Country code', N'Data Technology Solutions, Inc.' UNION ALL
	SELECT N'.ls', N'Country code', N'National University of Lesotho' UNION ALL
	SELECT N'.lt', N'Country code', N'Kaunas University of Technology Information Technology Development Institute' UNION ALL
	SELECT N'.lu', N'Country code', N'RESTENA' UNION ALL
	SELECT N'.lv', N'Country code', N'University of Latvia Institute of Mathematics and Computer Science Department of Network Solutions (DNS)' UNION ALL
	SELECT N'.ly', N'Country code', N'General Post and Telecommunication Company' UNION ALL
	SELECT N'.ma', N'Country code', N'Agence Nationale de Réglementation des Télécommunications (ANRT)' UNION ALL
	SELECT N'.mc', N'Country code', N'Gouvernement de Monaco Direction des Communications Electroniques' UNION ALL
	SELECT N'.md', N'Country code', N'MoldData S.E.' UNION ALL
	SELECT N'.me', N'Country code', N'Government of Montenegro' UNION ALL
	SELECT N'.mf', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.mg', N'Country code', N'NIC-MG (Network Information Center Madagascar)' UNION ALL
	SELECT N'.mh', N'Country code', N'Cabinet Office' UNION ALL
	SELECT N'.mil', N'Sponsored', N'DoD Network Information Center' UNION ALL
	SELECT N'.mk', N'Country code', N'Ministry of Foreign Relations' UNION ALL
	SELECT N'.ml', N'Country code', N'SOTELMA' UNION ALL
	SELECT N'.mm', N'Country code', N'Ministry of Communications, Posts & Telegraphs' UNION ALL
	SELECT N'.mn', N'Country code', N'Datacom Co., Ltd.' UNION ALL
	SELECT N'.mo', N'Country code', N'Bureau of Telecommunications Regulation (DSRT)' UNION ALL
	SELECT N'.mobi', N'Sponsored', N'Afilias Technologies Limited dba dotMobi' UNION ALL
	SELECT N'.mp', N'Country code', N'Saipan Datacom, Inc.' UNION ALL
	SELECT N'.mq', N'Country code', N'MEDIASERV' UNION ALL
	SELECT N'.mr', N'Country code', N'University of Nouakchott' UNION ALL
	SELECT N'.ms', N'Country code', N'MNI Networks Ltd.' UNION ALL
	SELECT N'.mt', N'Country code', N'NIC (Malta)' UNION ALL
	SELECT N'.mu', N'Country code', N'Internet Direct Ltd' UNION ALL
	SELECT N'.museum', N'Sponsored', N'Museum Domain Management Association' UNION ALL
	SELECT N'.mv', N'Country code', N'Dhiraagu Pvt. Ltd. (DHIVEHINET)' UNION ALL
	SELECT N'.mw', N'Country code', N'Malawi Sustainable Development Network Programme (Malawi SDNP)' UNION ALL
	SELECT N'.mx', N'Country code', N'NIC-Mexico ITESM - Campus Monterrey' UNION ALL
	SELECT N'.my', N'Country code', N'MYNIC Berhad' UNION ALL
	SELECT N'.mz', N'Country code', N'Centro de Informatica de Universidade Eduardo Mondlane' UNION ALL
	SELECT N'.na', N'Country code', N'Namibian Network Information Center' UNION ALL
	SELECT N'.name', N'Generic (restricted)', N'VeriSign Information Services, Inc.' UNION ALL
	SELECT N'.nc', N'Country code', N'Office des Postes et Telecommunications' UNION ALL
	SELECT N'.ne', N'Country code', N'SONITEL' UNION ALL
	SELECT N'.net', N'Generic', N'VeriSign Global Registry Services' UNION ALL
	SELECT N'.nf', N'Country code', N'Norfolk Island Data Services' UNION ALL
	SELECT N'.ng', N'Country code', N'Nigeria Internet Registration Association' UNION ALL
	SELECT N'.ni', N'Country code', N'Universidad Nacional del Ingernieria Centro de Computo' UNION ALL
	SELECT N'.nl', N'Country code', N'SIDN (Stichting Internet Domeinregistratie Nederland)' UNION ALL
	SELECT N'.no', N'Country code', N'UNINETT Norid A/S' UNION ALL
	SELECT N'.np', N'Country code', N'Mercantile Communications Pvt. Ltd.' UNION ALL
	SELECT N'.nr', N'Country code', N'CENPAC NET' UNION ALL
	SELECT N'.nu', N'Country code', N'The IUSN Foundation' UNION ALL
	SELECT N'.nz', N'Country code', N'InternetNZ' UNION ALL
	SELECT N'.om', N'Country code', N'Telecommunications Regulatory Authority (TRA)' UNION ALL
	SELECT N'.org', N'Generic', N'Public Interest Registry (PIR)' UNION ALL
	SELECT N'.pa', N'Country code', N'Universidad Tecnologica de Panama' UNION ALL
	SELECT N'.pe', N'Country code', N'Red Cientifica Peruana' UNION ALL
	SELECT N'.pf', N'Country code', N'Gouvernement de la Polynésie française' UNION ALL
	SELECT N'.pg', N'Country code', N'PNG DNS Administration Vice Chancellors Office The Papua New Guinea University of Technology' UNION ALL
	SELECT N'.ph', N'Country code', N'PH Domain Foundation' UNION ALL
	SELECT N'.pk', N'Country code', N'PKNIC' UNION ALL
	SELECT N'.pl', N'Country code', N'Research and Academic Computer Network' UNION ALL
	SELECT N'.pm', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.pn', N'Country code', N'Pitcairn Island Administration' UNION ALL
	SELECT N'.post', N'Sponsored', N'Universal Postal Union' UNION ALL
	SELECT N'.pr', N'Country code', N'Gauss Research Laboratory Inc.' UNION ALL
	SELECT N'.pro', N'Generic (restricted)', N'Registry Services Corporation dba RegistryPro' UNION ALL
	SELECT N'.ps', N'Country code', N'Ministry Of Telecommunications & Information Technology, Government Computer Center.' UNION ALL
	SELECT N'.pt', N'Country code', N'Fundação para a Computação Científica Nacional' UNION ALL
	SELECT N'.pw', N'Country code', N'Micronesia Investment and Development Corporation' UNION ALL
	SELECT N'.py', N'Country code', N'NIC-PY' UNION ALL
	SELECT N'.qa', N'Country code', N'The Supreme Council of Information and Communication Technology (ictQATAR)' UNION ALL
	SELECT N'.re', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.ro', N'Country code', N'National Institute for R&D in Informatics' UNION ALL
	SELECT N'.rs', N'Country code', N'Serbian National Register of Internet Domain Names (RNIDS)' UNION ALL
	SELECT N'.ru', N'Country code', N'Coordination Center for TLD RU' UNION ALL
	SELECT N'.rw', N'Country code', N'Rwanda Information Communication and Technology Association (RICTA)' UNION ALL
	SELECT N'.sa', N'Country code', N'Communications and Information Technology Commission' UNION ALL
	SELECT N'.sb', N'Country code', N'Solomon Telekom Company Limited' UNION ALL
	SELECT N'.sc', N'Country code', N'VCS Pty Ltd' UNION ALL
	SELECT N'.sd', N'Country code', N'Sudan Internet Society' UNION ALL
	SELECT N'.se', N'Country code', N'The Internet Infrastructure Foundation' UNION ALL
	SELECT N'.sg', N'Country code', N'Singapore Network Information Centre (SGNIC) Pte Ltd' UNION ALL
	SELECT N'.sh', N'Country code', N'Government of St. Helena' UNION ALL
	SELECT N'.si', N'Country code', N'Academic and Research Network of Slovenia (ARNES)' UNION ALL
	SELECT N'.sj', N'Country code', N'UNINETT Norid A/S' UNION ALL
	SELECT N'.sk', N'Country code', N'SK-NIC, a.s.' UNION ALL
	SELECT N'.sl', N'Country code', N'Sierratel' UNION ALL
	SELECT N'.sm', N'Country code', N'Telecom Italia San Marino S.p.A.' UNION ALL
	SELECT N'.sn', N'Country code', N'Universite Cheikh Anta Diop NIC Senegal' UNION ALL
	SELECT N'.so', N'Country code', N'Ministry of Post and Telecommunications' UNION ALL
	SELECT N'.sr', N'Country code', N'Telesur' UNION ALL
	SELECT N'.ss', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.st', N'Country code', N'Tecnisys' UNION ALL
	SELECT N'.su', N'Country code', N'Russian Institute for Development of Public Networks (ROSNIIROS)' UNION ALL
	SELECT N'.sv', N'Country code', N'SVNet' UNION ALL
	SELECT N'.sx', N'Country code', N'SX Registry SA B.V.' UNION ALL
	SELECT N'.sy', N'Country code', N'National Agency for Network Services (NANS)' UNION ALL
	SELECT N'.sz', N'Country code', N'University of Swaziland Department of Computer Science' UNION ALL
	SELECT N'.tc', N'Country code', N'Melrex TC' UNION ALL
	SELECT N'.td', N'Country code', N'Société des télécommunications du Tchad (SOTEL TCHAD)' UNION ALL
	SELECT N'.tel', N'Sponsored', N'Telnic Ltd.' UNION ALL
	SELECT N'.tf', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.tg', N'Country code', N'Cafe Informatique et Telecommunications' UNION ALL
	SELECT N'.th', N'Country code', N'Thai Network Information Center Foundation' UNION ALL
	SELECT N'.tj', N'Country code', N'Information Technology Center' UNION ALL
	SELECT N'.tk', N'Country code', N'Telecommunication Tokelau Corporation (Teletok)' UNION ALL
	SELECT N'.tl', N'Country code', N'Ministry of Infrastructure Information and Technology Division' UNION ALL
	SELECT N'.tm', N'Country code', N'TM Domain Registry Ltd' UNION ALL
	SELECT N'.tn', N'Country code', N'Agence Tunisienne d''Internet' UNION ALL
	SELECT N'.to', N'Country code', N'Government of the Kingdom of Tonga H.R.H. Crown Prince Tupouto''a c/o Consulate of Tonga' UNION ALL
	SELECT N'.tp', N'Country code', N'-' UNION ALL
	SELECT N'.tr', N'Country code', N'Middle East Technical University Department of Computer Engineering' UNION ALL
	SELECT N'.travel', N'Sponsored', N'Tralliance Registry Management Company, LLC.' UNION ALL
	SELECT N'.tt', N'Country code', N'University of the West Indies Faculty of Engineering' UNION ALL
	SELECT N'.tv', N'Country code', N'Ministry of Finance and Tourism' UNION ALL
	SELECT N'.tw', N'Country code', N'Taiwan Network Information Center (TWNIC)' UNION ALL
	SELECT N'.tz', N'Country code', N'Tanzania Network Information Centre (tzNIC)' UNION ALL
	SELECT N'.ua', N'Country code', N'Communication Systems Ltd' UNION ALL
	SELECT N'.ug', N'Country code', N'Uganda Online Ltd.' UNION ALL
	SELECT N'.uk', N'Country code', N'Nominet UK' UNION ALL
	SELECT N'.um', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.us', N'Country code', N'NeuStar, Inc.' UNION ALL
	SELECT N'.uy', N'Country code', N'SeCIU - Universidad de la Republica' UNION ALL
	SELECT N'.uz', N'Country code', N'Computerization and Information Technologies Developing Center UZINFOCOM' UNION ALL
	SELECT N'.va', N'Country code', N'Holy See Secretariat of State Department of Telecommunications' UNION ALL
	SELECT N'.vc', N'Country code', N'Ministry of Telecommunications, Science, Technology and Industry' UNION ALL
	SELECT N'.ve', N'Country code', N'Comisión Nacional de Telecomunicaciones (CONATEL)' UNION ALL
	SELECT N'.vg', N'Country code', N'Pinebrook Developments Ltd' UNION ALL
	SELECT N'.vi', N'Country code', N'Virgin Islands Public Telcommunications System c/o COBEX Internet Services' UNION ALL
	SELECT N'.vn', N'Country code', N'Ministry of Information and Communications of Socialist Republic of Viet Nam' UNION ALL
	SELECT N'.vu', N'Country code', N'Telecom Vanuatu Limited' UNION ALL
	SELECT N'.wf', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.ws', N'Country code', N'Government of Samoa Ministry of Foreign Affairs & Trade' UNION ALL
	SELECT N'.测试', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.परीक्षा', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.한국', N'Country code', N'KISA (Korea Internet & Security Agency)' UNION ALL
	SELECT N'.ভারত', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.বাংলা', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.испытание', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.қаз', N'Country code', N'Association of IT Companies of Kazakhstan' UNION ALL
	SELECT N'.срб', N'Country code', N'Serbian National Register of Internet Domain Names (RNIDS)' UNION ALL
	SELECT N'.테스트', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.சிங்கப்பூர்', N'Country code', N'Singapore Network Information Centre (SGNIC) Pte Ltd' UNION ALL
	SELECT N'.טעסט', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.中国', N'Country code', N'China Internet Network Information Center' UNION ALL
	SELECT N'.中國', N'Country code', N'China Internet Network Information Center' UNION ALL
	SELECT N'.భారత్', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.ලංකා', N'Country code', N'LK Domain Registry' UNION ALL
	SELECT N'.測試', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.ભારત', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.भारत', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.آزمایشی', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.பரிட்சை', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.укр', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.香港', N'Country code', N'Hong Kong Internet Registration Corporation Ltd.' UNION ALL
	SELECT N'.δοκιμή', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.إختبار', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.台湾', N'Country code', N'Taiwan Network Information Center (TWNIC)' UNION ALL
	SELECT N'.台灣', N'Country code', N'Taiwan Network Information Center (TWNIC)' UNION ALL
	SELECT N'.мон', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.الجزائر', N'Country code', N'CERIST' UNION ALL
	SELECT N'.عمان', N'Country code', N'Telecommunications Regulatory Authority (TRA)' UNION ALL
	SELECT N'.ایران', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.امارات', N'Country code', N'Telecommunications Regulatory Authority (TRA)' UNION ALL
	SELECT N'.پاکستان', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.الاردن', N'Country code', N'National Information Technology Center (NITC)' UNION ALL
	SELECT N'.بھارت', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.المغرب', N'Country code', N'Agence Nationale de Réglementation des Télécommunications (ANRT)' UNION ALL
	SELECT N'.السعودية', N'Country code', N'Communications and Information Technology Commission' UNION ALL
	SELECT N'.سودان', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.مليسيا', N'Country code', N'MYNIC Berhad' UNION ALL
	SELECT N'.გე', N'Country code', N'Not assigned' UNION ALL
	SELECT N'.ไทย', N'Country code', N'Thai Network Information Center Foundation' UNION ALL
	SELECT N'.سورية', N'Country code', N'National Agency for Network Services (NANS)' UNION ALL
	SELECT N'.рф', N'Country code', N'Coordination Center for TLD RU' UNION ALL
	SELECT N'.تونس', N'Country code', N'Agence Tunisienne d''Internet' UNION ALL
	SELECT N'.ਭਾਰਤ', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.مصر', N'Country code', N'National Telecommunication Regulatory Authority - NTRA' UNION ALL
	SELECT N'.قطر', N'Country code', N'Supreme Council for Communications and Information Technology (ictQATAR)' UNION ALL
	SELECT N'.இலங்கை', N'Country code', N'LK Domain Registry' UNION ALL
	SELECT N'.இந்தியா', N'Country code', N'National Internet Exchange of India' UNION ALL
	SELECT N'.新加坡', N'Country code', N'Singapore Network Information Centre (SGNIC) Pte Ltd' UNION ALL
	SELECT N'.فلسطين', N'Country code', N'Ministry of Telecom & Information Technology (MTIT)' UNION ALL
	SELECT N'.テスト', N'Test', N'Internet Assigned Numbers Authority' UNION ALL
	SELECT N'.xxx', N'Sponsored', N'ICM Registry LLC' UNION ALL
	SELECT N'.ye', N'Country code', N'TeleYemen' UNION ALL
	SELECT N'.yt', N'Country code', N'AFNIC (NIC France) - Immeuble International' UNION ALL
	SELECT N'.za', N'Country code', N'ZA Domain Name Authority' UNION ALL
	SELECT N'.zm', N'Country code', N'ZAMNET Communication Systems Ltd.' UNION ALL
	SELECT N'.zw', N'Country code', N'Postal and Telecommunications Regulatory Authority of Zimbabwe (POTRAZ)' 
)
MERGE INTO dbo.dimTopLevelDomain AS tgt
USING (
	SELECT
		*
		,'http://www.iana.org/domains/root/db/' + RIGHT(TopLevelDomainCode, LEN(TopLevelDomainCode) - 1) + '.html'	AS IANAUrl
	FROM
		cTLDs
)AS src
ON tgt.TopLevelDomainCode = src.TopLevelDomainCode

WHEN MATCHED THEN UPDATE
SET DomainType				= src.DomainType
	,SponsoringOrganisation = src.SponsoringOrganisation
	,IANAUrl				= src.IANAUrl

WHEN NOT MATCHED THEN 
INSERT(TopLevelDomainCode, DomainType, SponsoringOrganisation, IANAUrl)
VALUES(src.TopLevelDomainCode, src.DomainType, src.SponsoringOrganisation, src.IANAUrl);
;
GO