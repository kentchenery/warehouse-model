﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimNZGSTRate';

WITH cNZGSTRate(NZGSTRateID, GSTRatePct, GSTRateFactor, GSTRateDisplay, ValidFrom, ValidTo) AS (

    /* Unknown and Not Applicable records */
    SELECT -1, 0.0, 1.0, 'Unknown', '0001-01-01', '9999-12-31' UNION ALL
    SELECT -2, 0.0, 1.0, 'Not Applicable', '0001-01-01', '9999-12-31' UNION ALL

    /* GST Rates */
    SELECT 1, 0.0, 1.0, 'No GST', '0001-01-01', '1986-09-30' UNION ALL
    SELECT 2, 10.0, 1.1, '10%', '1986-10-01', '1989-06-30' UNION ALL
    SELECT 3, 12.5, 1.125, '12.5%', '1989-07-01', '2010-09-30' UNION ALL
    SELECT 4, 15.0, 1.15, '15%', '2010-10-01', '9999-12-31'

)
MERGE INTO dbo.dimNZGSTRate AS tgt
USING cNZGSTRate AS src
ON tgt.NZGSTRateID = src.NZGSTRateID

WHEN MATCHED THEN UPDATE
SET GSTRatePct      = src.GSTRatePct
    ,GSTRateFactor  = src.GSTRateFactor
    ,GSTRateDisplay = src.GSTRateDisplay
    ,ValidFrom      = src.ValidFrom
    ,ValidTo        = src.ValidTo

WHEN NOT MATCHED THEN 
INSERT(NZGSTRateID, GSTRatePct, GSTRateFactor, GSTRateDisplay, ValidFrom, ValidTo)
VALUES(src.NZGSTRateID, src.GSTRatePct, src.GSTRateFactor, src.GSTRateDisplay, src.ValidFrom, src.ValidTo)
;
GO