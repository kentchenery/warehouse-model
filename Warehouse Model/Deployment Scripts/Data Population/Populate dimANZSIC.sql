﻿PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Populating dimension: dimANZSIC';

/* The ANZSIC codes were taken from the Statistics New Zealand website under their createive commons license
 * Source:  http://www.stats.govt.nz/surveys_and_methods/methods/classifications-and-standards/classification-related-stats-standards/industrial-classification.aspx
 * License: http://creativecommons.org/licenses/by/3.0/nz/deed.en
 */
/* Unknown ANZSIC code */
SET IDENTITY_INSERT dbo.dimANZSIC ON;

WITH cUnknown AS (

	SELECT
		-1				AS ANZSICID
		,'?'			AS DivisionCode
		,'Unknown'		AS DivisionDescription
		,'??'			AS SubdivisionCode
		,'Unknown'		AS SubdivisionDescription
		,'???'			AS GroupCode
		,'Unknown'		AS GroupDescription
		,'????'			AS ClassCode
		,'Unknown'		AS ClassDescription
		,'?????'		AS ANZSICCode
)
MERGE INTO dbo.dimANZSIC AS tgt
USING cUnknown AS src
ON tgt.ANZSICID = src.ANZSICID

WHEN MATCHED THEN UPDATE
SET
	DivisionCode			= src.DivisionCode
	,DivisionDescription	= src.DivisionDescription
	,SubdivisionCode		= src.SubdivisionCode
	,SubdivisionDescription	= src.SubdivisionDescription
	,GroupCode				= src.GroupCode
	,GroupDescription		= src.GroupDescription
	,ClassCode				= src.ClassCode
	,ClassDescription		= src.ClassDescription
	,ANZSICCode				= src.ANZSICCode

WHEN NOT MATCHED THEN 
INSERT(ANZSICID, DivisionCode, DivisionDescription, SubdivisionCode, SubdivisionDescription, GroupCode, GroupDescription, ClassCode, ClassDescription, ANZSICCode)
VALUES(src.ANZSICID, src.DivisionCode, src.DivisionDescription, src.SubdivisionCode, src.SubdivisionDescription, src.GroupCode, src.GroupDescription, src.ClassCode, src.ClassDescription, src.ANZSICCode);

SET IDENTITY_INSERT dbo.dimANZSIC OFF;
GO

/* All the ANZSIC codes */
WITH cDivision(DivisionCode, DivisionDescription) AS (

	SELECT 'A', 'Agriculture, Forestry and Fishing' UNION ALL
	SELECT 'B', 'Mining' UNION ALL
	SELECT 'C', 'Manufacturing' UNION ALL
	SELECT 'D', 'Electricity, Gas, Water and Waste Services' UNION ALL
	SELECT 'E', 'Construction' UNION ALL
	SELECT 'F', 'Wholesale Trade' UNION ALL
	SELECT 'G', 'Retail Trade' UNION ALL
	SELECT 'H', 'Accommodation and Food Services' UNION ALL
	SELECT 'I', 'Transport, Postal and Warehousing' UNION ALL
	SELECT 'J', 'Information Media and Telecommunications' UNION ALL
	SELECT 'K', 'Financial and Insurance Services' UNION ALL
	SELECT 'L', 'Rental, Hiring and Real Estate Services' UNION ALL
	SELECT 'M', 'Professional, Scientific and Technical Services' UNION ALL
	SELECT 'N', 'Administrative and Support Services' UNION ALL
	SELECT 'O', 'Public Administration and Safety' UNION ALL
	SELECT 'P', 'Education and Training' UNION ALL
	SELECT 'Q', 'Health Care and Social Assistance' UNION ALL
	SELECT 'R', 'Arts and Recreation Services' UNION ALL
	SELECT 'S', 'Other Services'

), cSubdivision(DivisionCode, SubdivisionCode, SubdivisionDescription) AS (

	SELECT 'A', '01', 'Agriculture' UNION ALL
	SELECT 'A', '02', 'Aquaculture' UNION ALL
	SELECT 'A', '03', 'Forestry and Logging' UNION ALL
	SELECT 'A', '04', 'Fishing, Hunting and Trapping' UNION ALL
	SELECT 'A', '05', 'Agriculture, Forestry and Fishing Support Services' UNION ALL
	SELECT 'B', '06', 'Coal Mining' UNION ALL
	SELECT 'B', '07', 'Oil and Gas Extraction' UNION ALL
	SELECT 'B', '08', 'Metal Ore Mining' UNION ALL
	SELECT 'B', '09', 'Non-Metallic Mineral Mining and Quarrying' UNION ALL
	SELECT 'B', '10', 'Exploration and Other Mining Support Services' UNION ALL
	SELECT 'C', '11', 'Food Product Manufacturing' UNION ALL
	SELECT 'C', '12', 'Beverage and Tobacco Product Manufacturing' UNION ALL
	SELECT 'C', '13', 'Textile, Leather, Clothing and Footwear Manufacturing' UNION ALL
	SELECT 'C', '14', 'Wood Product Manufacturing' UNION ALL
	SELECT 'C', '15', 'Pulp, Paper and Converted Paper Product Manufacturing' UNION ALL
	SELECT 'C', '16', 'Printing' UNION ALL
	SELECT 'C', '17', 'Petroleum and Coal Product Manufacturing' UNION ALL
	SELECT 'C', '18', 'Basic Chemical and Chemical Product Manufacturing' UNION ALL
	SELECT 'C', '19', 'Polymer Product and Rubber Product Manufacturing' UNION ALL
	SELECT 'C', '20', 'Non-Metallic Mineral Product Manufacturing' UNION ALL
	SELECT 'C', '21', 'Primary Metal and Metal Product Manufacturing' UNION ALL
	SELECT 'C', '22', 'Fabricated Metal Product Manufacturing' UNION ALL
	SELECT 'C', '23', 'Transport Equipment Manufacturing' UNION ALL
	SELECT 'C', '24', 'Machinery and Equipment Manufacturing' UNION ALL
	SELECT 'C', '25', 'Furniture and Other Manufacturing' UNION ALL
	SELECT 'D', '26', 'Electricity Supply' UNION ALL
	SELECT 'D', '27', 'Gas Supply' UNION ALL
	SELECT 'D', '28', 'Water Supply, Sewerage and Drainage Services' UNION ALL
	SELECT 'D', '29', 'Waste Collection, Treatment and Disposal Services' UNION ALL
	SELECT 'E', '30', 'Building Construction' UNION ALL
	SELECT 'E', '31', 'Heavy and Civil Engineering Construction' UNION ALL
	SELECT 'E', '32', 'Construction Services' UNION ALL
	SELECT 'F', '33', 'Basic Material Wholesaling' UNION ALL
	SELECT 'F', '34', 'Machinery and Equipment Wholesaling' UNION ALL
	SELECT 'F', '35', 'Motor Vehicle and Motor Vehicle Parts Wholesaling' UNION ALL
	SELECT 'F', '36', 'Grocery, Liquor and Tobacco Product Wholesaling' UNION ALL
	SELECT 'F', '37', 'Other Goods Wholesaling' UNION ALL
	SELECT 'F', '38', 'Commission Based Wholesaling' UNION ALL
	SELECT 'G', '39', 'Motor Vehicle and Motor Vehicle Parts Retailing' UNION ALL
	SELECT 'G', '40', 'Fuel Retailing' UNION ALL
	SELECT 'G', '41', 'Food Retailing' UNION ALL
	SELECT 'G', '42', 'Other Store-Based Retailing' UNION ALL
	SELECT 'G', '43', 'Non Store Retailing and Retail Commission Based Buying and/or Selling' UNION ALL
	SELECT 'H', '44', 'Accommodation' UNION ALL
	SELECT 'H', '45', 'Food and Beverage Services' UNION ALL
	SELECT 'I', '46', 'Road Transport' UNION ALL
	SELECT 'I', '47', 'Rail Transport' UNION ALL
	SELECT 'I', '48', 'Water Transport' UNION ALL
	SELECT 'I', '49', 'Air and Space Transport' UNION ALL
	SELECT 'I', '50', 'Other Transport' UNION ALL
	SELECT 'I', '51', 'Postal and Courier Pick-up and Delivery Services' UNION ALL
	SELECT 'I', '52', 'Transport Support Services' UNION ALL
	SELECT 'I', '53', 'Warehousing and Storage Services' UNION ALL
	SELECT 'J', '54', 'Publishing (except Internet and Music Publishing)' UNION ALL
	SELECT 'J', '55', 'Motion Picture and Sound Recording Activities' UNION ALL
	SELECT 'J', '56', 'Broadcasting (except Internet)' UNION ALL
	SELECT 'J', '57', 'Internet Publishing and Broadcasting' UNION ALL
	SELECT 'J', '58', 'Telecommunications Services' UNION ALL
	SELECT 'J', '59', 'Internet Service Providers, Web Search Portals and Data Processing Services' UNION ALL
	SELECT 'J', '60', 'Library and Other Information Services' UNION ALL
	SELECT 'K', '62', 'Finance' UNION ALL
	SELECT 'K', '63', 'Insurance and Superannuation Funds' UNION ALL
	SELECT 'K', '64', 'Auxiliary Finance and Insurance Services' UNION ALL
	SELECT 'L', '66', 'Rental and Hiring Services (except Real Estate)' UNION ALL
	SELECT 'L', '67', 'Property Operators and Real Estate Services' UNION ALL
	SELECT 'M', '69', 'Professional, Scientific and Technical Services (except Computer Systems Design and Related Services)' UNION ALL
	SELECT 'M', '70', 'Computer Systems Design and Related Services' UNION ALL
	SELECT 'N', '72', 'Administrative Services' UNION ALL
	SELECT 'N', '73', 'Building Cleaning, Pest Control and Other Support Services' UNION ALL
	SELECT 'O', '75', 'Public Administration' UNION ALL
	SELECT 'O', '76', 'Defence' UNION ALL
	SELECT 'O', '77', 'Public Order, Safety and Regulatory Services' UNION ALL
	SELECT 'P', '80', 'Preschool and School Education' UNION ALL
	SELECT 'P', '81', 'Tertiary Education' UNION ALL
	SELECT 'P', '82', 'Adult, Community and Other Education' UNION ALL
	SELECT 'Q', '84', 'Hospitals' UNION ALL
	SELECT 'Q', '85', 'Medical and Other Health Care Services' UNION ALL
	SELECT 'Q', '86', 'Residential Care Services' UNION ALL
	SELECT 'Q', '87', 'Social Assistance Services' UNION ALL
	SELECT 'R', '89', 'Heritage Activities' UNION ALL
	SELECT 'R', '90', 'Artistic Activities' UNION ALL
	SELECT 'R', '91', 'Sport and Recreation Activities' UNION ALL
	SELECT 'R', '92', 'Gambling Activities' UNION ALL
	SELECT 'S', '94', 'Repair and Maintenance' UNION ALL
	SELECT 'S', '95', 'Personal and Other Services' UNION ALL
	SELECT 'S', '96', 'Private Households Employing Staff'

), cGroup(GroupCode, GroupDescription) AS (

	SELECT '011', 'Nursery and Floriculture Production' UNION ALL
	SELECT '012', 'Mushroom and Vegetable Growing' UNION ALL
	SELECT '013', 'Fruit and Tree Nut Growing' UNION ALL
	SELECT '014', 'Grain, Sheep and Beef Cattle Farming' UNION ALL
	SELECT '015', 'Other Crop Growing' UNION ALL
	SELECT '016', 'Dairy Cattle Farming' UNION ALL
	SELECT '017', 'Poultry Farming' UNION ALL
	SELECT '018', 'Deer Farming' UNION ALL
	SELECT '019', 'Other Livestock Farming' UNION ALL
	SELECT '020', 'Aquaculture' UNION ALL
	SELECT '030', 'Forestry and Logging' UNION ALL
	SELECT '041', 'Fishing' UNION ALL
	SELECT '042', 'Hunting and Trapping' UNION ALL
	SELECT '051', 'Forestry Support Services' UNION ALL
	SELECT '052', 'Agriculture and Fishing Support Services' UNION ALL
	SELECT '060', 'Coal Mining' UNION ALL
	SELECT '070', 'Oil and Gas Extraction' UNION ALL
	SELECT '080', 'Metal Ore Mining' UNION ALL
	SELECT '091', 'Construction Material Mining' UNION ALL
	SELECT '099', 'Other Non-Metallic Mineral Mining and Quarrying' UNION ALL
	SELECT '101', 'Exploration' UNION ALL
	SELECT '109', 'Other Mining Support Services' UNION ALL
	SELECT '111', 'Meat and Meat Product Manufacturing' UNION ALL
	SELECT '112', 'Seafood Processing' UNION ALL
	SELECT '113', 'Dairy Product Manufacturing' UNION ALL
	SELECT '114', 'Fruit and Vegetable Processing' UNION ALL
	SELECT '115', 'Oil and Fat Manufacturing' UNION ALL
	SELECT '116', 'Grain Mill and Cereal Product Manufacturing' UNION ALL
	SELECT '117', 'Bakery Product Manufacturing' UNION ALL
	SELECT '118', 'Sugar and Confectionery Manufacturing' UNION ALL
	SELECT '119', 'Other Food Product Manufacturing' UNION ALL
	SELECT '121', 'Beverage Manufacturing' UNION ALL
	SELECT '122', 'Cigarette and Tobacco Product Manufacturing' UNION ALL
	SELECT '131', 'Textile Fibre, Yarn and Woven Fabric Manufacturing' UNION ALL
	SELECT '132', 'Leather Tanning and Fur Dressing' UNION ALL
	SELECT '133', 'Textile Product Manufacturing' UNION ALL
	SELECT '134', 'Knitted Product Manufacturing' UNION ALL
	SELECT '135', 'Clothing and Footwear Manufacturing' UNION ALL
	SELECT '141', 'Log Sawmilling and Timber Dressing' UNION ALL
	SELECT '149', 'Other Wood Product Manufacturing' UNION ALL
	SELECT '151', 'Pulp, Paper and Paperboard Manufacturing' UNION ALL
	SELECT '152', 'Converted Paper Product Manufacturing' UNION ALL
	SELECT '161', 'Printing' UNION ALL
	SELECT '162', 'Reproduction of Recorded Media' UNION ALL
	SELECT '170', 'Petroleum Refining and Petroleum and Coal Product Manufacturing' UNION ALL
	SELECT '181', 'Chemical Manufacturing' UNION ALL
	SELECT '182', 'Basic Polymer Manufacturing' UNION ALL
	SELECT '183', 'Fertiliser and Pesticide Manufacturing' UNION ALL
	SELECT '184', 'Pharmaceutical and Medicinal Product Manufacturing' UNION ALL
	SELECT '185', 'Cleaning Compound and Toiletry Preparation Manufacturing' UNION ALL
	SELECT '189', 'Other Basic Chemical Product Manufacturing' UNION ALL
	SELECT '191', 'Polymer Product Manufacturing' UNION ALL
	SELECT '192', 'Natural Rubber Product Manufacturing' UNION ALL
	SELECT '201', 'Glass and Glass Product Manufacturing' UNION ALL
	SELECT '202', 'Ceramic Product Manufacturing' UNION ALL
	SELECT '203', 'Cement, Lime, Plaster and Concrete Product Manufacturing' UNION ALL
	SELECT '209', 'Other Non-Metallic Mineral Product Manufacturing' UNION ALL
	SELECT '211', 'Basic Ferrous Metal Manufacturing' UNION ALL
	SELECT '212', 'Basic Ferrous Metal Product Manufacturing' UNION ALL
	SELECT '213', 'Basic Non-Ferrous Metal Manufacturing' UNION ALL
	SELECT '214', 'Basic Non-Ferrous Metal Product Manufacturing' UNION ALL
	SELECT '221', 'Iron and Steel Forging' UNION ALL
	SELECT '222', 'Structural Metal Product Manufacturing' UNION ALL
	SELECT '223', 'Metal Container Manufacturing' UNION ALL
	SELECT '224', 'Other Sheet Metal Product Manufacturing' UNION ALL
	SELECT '229', 'Other Fabricated Metal Product Manufacturing' UNION ALL
	SELECT '231', 'Motor Vehicle and Motor Vehicle Part Manufacturing' UNION ALL
	SELECT '239', 'Other Transport Equipment Manufacturing' UNION ALL
	SELECT '241', 'Professional and Scientific Equipment Manufacturing' UNION ALL
	SELECT '242', 'Computer and Electronic Equipment Manufacturing' UNION ALL
	SELECT '243', 'Electrical Equipment Manufacturing' UNION ALL
	SELECT '244', 'Domestic Appliance Manufacturing' UNION ALL
	SELECT '245', 'Pump, Compressor, Heating and Ventilation Equipment Manufacturing' UNION ALL
	SELECT '246', 'Specialised Machinery and Equipment Manufacturing' UNION ALL
	SELECT '249', 'Other Machinery and Equipment Manufacturing' UNION ALL
	SELECT '251', 'Furniture Manufacturing' UNION ALL
	SELECT '259', 'Other Manufacturing' UNION ALL
	SELECT '261', 'Electricity Generation' UNION ALL
	SELECT '262', 'Electricity Transmission' UNION ALL
	SELECT '263', 'Electricity Distribution' UNION ALL
	SELECT '264', 'On Selling Electricity and Electricity Market Operation' UNION ALL
	SELECT '270', 'Gas Supply' UNION ALL
	SELECT '281', 'Water Supply, Sewerage and Drainage Services' UNION ALL
	SELECT '291', 'Waste Collection Services' UNION ALL
	SELECT '292', 'Waste Treatment, Disposal and Remediation Services' UNION ALL
	SELECT '301', 'Residential Building Construction' UNION ALL
	SELECT '302', 'Non-Residential Building Construction' UNION ALL
	SELECT '310', 'Heavy and Civil Engineering Construction' UNION ALL
	SELECT '321', 'Land Development and Site Preparation Services' UNION ALL
	SELECT '322', 'Building Structure Services' UNION ALL
	SELECT '323', 'Building Installation Services' UNION ALL
	SELECT '324', 'Building Completion Services' UNION ALL
	SELECT '329', 'Other Construction Services' UNION ALL
	SELECT '331', 'Agricultural Product Wholesaling' UNION ALL
	SELECT '332', 'Mineral, Metal and Chemical Wholesaling' UNION ALL
	SELECT '333', 'Timber and Hardware Goods Wholesaling' UNION ALL
	SELECT '341', 'Specialised Industrial Machinery and Equipment Wholesaling' UNION ALL
	SELECT '349', 'Other Machinery and Equipment Wholesaling' UNION ALL
	SELECT '350', 'Motor Vehicle and Motor Vehicle Parts Wholesaling' UNION ALL
	SELECT '360', 'Grocery, Liquor and Tobacco Product Wholesaling' UNION ALL
	SELECT '371', 'Textile, Clothing and Footwear Wholesaling' UNION ALL
	SELECT '372', 'Pharmaceutical and Toiletry Goods Wholesaling' UNION ALL
	SELECT '373', 'Furniture, Floor Coverings and Other Goods Wholesaling' UNION ALL
	SELECT '380', 'Commission Based Wholesaling' UNION ALL
	SELECT '391', 'Motor Vehicle Retailing' UNION ALL
	SELECT '392', 'Motor Vehicle Parts Retailing' UNION ALL
	SELECT '400', 'Fuel Retailing' UNION ALL
	SELECT '411', 'Supermarket and Grocery Stores' UNION ALL
	SELECT '412', 'Specialised Food Retailing' UNION ALL
	SELECT '421', 'Furniture, Floor Coverings, Houseware and Textile Goods Retailing' UNION ALL
	SELECT '422', 'Electrical and Electronic Goods Retailing' UNION ALL
	SELECT '423', 'Hardware, Building and Garden Supplies Retailing' UNION ALL
	SELECT '424', 'Recreational Goods Retailing' UNION ALL
	SELECT '425', 'Clothing, Footwear and Personal Accessories Retailing' UNION ALL
	SELECT '426', 'Department Stores' UNION ALL
	SELECT '427', 'Pharmaceutical and Other Store-Based Retailing' UNION ALL
	SELECT '431', 'Non Store Retailing' UNION ALL
	SELECT '432', 'Retail Commission Based Buying and/or Selling' UNION ALL
	SELECT '440', 'Accommodation' UNION ALL
	SELECT '451', 'Cafes, Restaurants and Takeaway Food Services' UNION ALL
	SELECT '452', 'Pubs, Taverns and Bars' UNION ALL
	SELECT '453', 'Clubs (Hospitality)' UNION ALL
	SELECT '461', 'Road Freight Transport' UNION ALL
	SELECT '462', 'Road Passenger Transport' UNION ALL
	SELECT '471', 'Rail Freight Transport' UNION ALL
	SELECT '472', 'Rail Passenger Transport' UNION ALL
	SELECT '481', 'Water Freight Transport' UNION ALL
	SELECT '482', 'Water Passenger Transport' UNION ALL
	SELECT '490', 'Air and Space Transport' UNION ALL
	SELECT '501', 'Scenic and Sightseeing Transport' UNION ALL
	SELECT '502', 'Pipeline and Other Transport' UNION ALL
	SELECT '510', 'Postal and Courier Pick-up and Delivery Services' UNION ALL
	SELECT '521', 'Water Transport Support Services' UNION ALL
	SELECT '522', 'Air Transport Support Services' UNION ALL
	SELECT '529', 'Other Transport Support Services' UNION ALL
	SELECT '530', 'Warehousing and Storage Services' UNION ALL
	SELECT '541', 'Newspaper, Periodical, Book and Directory Publishing' UNION ALL
	SELECT '542', 'Software Publishing' UNION ALL
	SELECT '551', 'Motion Picture and Video Activities' UNION ALL
	SELECT '552', 'Sound Recording and Music Publishing' UNION ALL
	SELECT '561', 'Radio Broadcasting' UNION ALL
	SELECT '562', 'Television Broadcasting' UNION ALL
	SELECT '570', 'Internet Publishing and Broadcasting' UNION ALL
	SELECT '580', 'Telecommunications Services' UNION ALL
	SELECT '591', 'Internet Service Providers and Web Search Portals' UNION ALL
	SELECT '592', 'Data Processing, Web Hosting and Electronic Information Storage Services' UNION ALL
	SELECT '601', 'Libraries and Archives' UNION ALL
	SELECT '602', 'Other Information Services' UNION ALL
	SELECT '621', 'Central Banking' UNION ALL
	SELECT '622', 'Depository Financial Intermediation' UNION ALL
	SELECT '623', 'Non-depository Financing' UNION ALL
	SELECT '624', 'Financial Asset Investing' UNION ALL
	SELECT '631', 'Life Insurance' UNION ALL
	SELECT '632', 'Health and General Insurance' UNION ALL
	SELECT '633', 'Superannuation Funds' UNION ALL
	SELECT '641', 'Auxiliary Finance and Investment Services' UNION ALL
	SELECT '642', 'Auxiliary Insurance Services' UNION ALL
	SELECT '661', 'Motor Vehicle and Transport Equipment Rental and Hiring' UNION ALL
	SELECT '662', 'Farm Animals and Bloodstock Leasing' UNION ALL
	SELECT '663', 'Other Goods and Equipment Rental and Hiring' UNION ALL
	SELECT '664', 'Non-Financial Intangible Assets (except Copyrights) Leasing' UNION ALL
	SELECT '671', 'Property Operators' UNION ALL
	SELECT '672', 'Real Estate Services' UNION ALL
	SELECT '691', 'Scientific Research Services' UNION ALL
	SELECT '692', 'Architectural, Engineering and Technical Services' UNION ALL
	SELECT '693', 'Legal and Accounting Services' UNION ALL
	SELECT '694', 'Advertising Services' UNION ALL
	SELECT '695', 'Market Research and Statistical Services' UNION ALL
	SELECT '696', 'Management and Other Consulting Services' UNION ALL
	SELECT '697', 'Veterinary Services' UNION ALL
	SELECT '699', 'Other Professional, Scientific and Technical Services' UNION ALL
	SELECT '700', 'Computer Systems Design and Related Services' UNION ALL
	SELECT '721', 'Employment Services' UNION ALL
	SELECT '722', 'Travel Agency Services' UNION ALL
	SELECT '729', 'Other Administrative Services' UNION ALL
	SELECT '731', 'Building Cleaning, Pest Control and Gardening Services' UNION ALL
	SELECT '732', 'Packaging and Labelling Services' UNION ALL
	SELECT '751', 'Central Government Administration' UNION ALL
	SELECT '752', 'State Government Administration' UNION ALL
	SELECT '753', 'Local Government Administration' UNION ALL
	SELECT '754', 'Justice' UNION ALL
	SELECT '755', 'Government Representation' UNION ALL
	SELECT '760', 'Defence' UNION ALL
	SELECT '771', 'Public Order and Safety Services' UNION ALL
	SELECT '772', 'Regulatory Services' UNION ALL
	SELECT '801', 'Preschool Education' UNION ALL
	SELECT '802', 'School Education' UNION ALL
	SELECT '810', 'Tertiary Education' UNION ALL
	SELECT '821', 'Adult, Community and Other Education' UNION ALL
	SELECT '822', 'Educational Support Services' UNION ALL
	SELECT '840', 'Hospitals' UNION ALL
	SELECT '851', 'Medical Services' UNION ALL
	SELECT '852', 'Pathology and Diagnostic Imaging Services' UNION ALL
	SELECT '853', 'Allied Health Services' UNION ALL
	SELECT '859', 'Other Health Care Services' UNION ALL
	SELECT '860', 'Residential Care Services' UNION ALL
	SELECT '871', 'Child Care Services' UNION ALL
	SELECT '879', 'Other Social Assistance Services' UNION ALL
	SELECT '891', 'Museum Operation' UNION ALL
	SELECT '892', 'Parks and Gardens Operations' UNION ALL
	SELECT '900', 'Creative and Performing Arts Activities' UNION ALL
	SELECT '911', 'Sport and Physical Recreation Activities' UNION ALL
	SELECT '912', 'Horse and Dog Racing Activities' UNION ALL
	SELECT '913', 'Amusement and Other Recreation Activities' UNION ALL
	SELECT '920', 'Gambling Activities' UNION ALL
	SELECT '941', 'Automotive Repair and Maintenance' UNION ALL
	SELECT '942', 'Machinery and Equipment Repair and Maintenance' UNION ALL
	SELECT '949', 'Other Repair and Maintenance' UNION ALL
	SELECT '951', 'Personal Care Services' UNION ALL
	SELECT '952', 'Funeral, Crematorium and Cemetery Services' UNION ALL
	SELECT '953', 'Other Personal Services' UNION ALL
	SELECT '954', 'Religious Services' UNION ALL
	SELECT '955', 'Civic, Professional and Other Interest Group Services' UNION ALL
	SELECT '960', 'Private Households Employing Staff'

), cClass(ClassCode, ClassDescription) AS (

	SELECT '0111', 'Nursery Production (Under Cover)' UNION ALL
	SELECT '0112', 'Nursery Production (Outdoors)' UNION ALL
	SELECT '0113', 'Turf Growing' UNION ALL
	SELECT '0114', 'Floriculture Production (Under Cover)' UNION ALL
	SELECT '0115', 'Floriculture Production (Outdoors)' UNION ALL
	SELECT '0121', 'Mushroom Growing' UNION ALL
	SELECT '0122', 'Vegetable Growing (Under Cover)' UNION ALL
	SELECT '0123', 'Vegetable Growing (Outdoors)' UNION ALL
	SELECT '0131', 'Grape Growing' UNION ALL
	SELECT '0132', 'Kiwifruit Growing' UNION ALL
	SELECT '0133', 'Berry Fruit Growing' UNION ALL
	SELECT '0134', 'Apple and Pear Growing' UNION ALL
	SELECT '0135', 'Stonefruit Growing' UNION ALL
	SELECT '0136', 'Citrus Fruit Growing' UNION ALL
	SELECT '0137', 'Olive Growing' UNION ALL
	SELECT '0139', 'Other Fruit and Tree Nut Growing' UNION ALL
	SELECT '0141', 'Sheep Farming (Specialised)' UNION ALL
	SELECT '0142', 'Beef Cattle Farming (Specialised)' UNION ALL
	SELECT '0143', 'Beef Cattle Feedlots (Specialised)' UNION ALL
	SELECT '0144', 'Sheep-Beef Cattle Farming' UNION ALL
	SELECT '0145', 'Grain-Sheep and Grain-Beef Cattle Farming' UNION ALL
	SELECT '0146', 'Rice Growing' UNION ALL
	SELECT '0149', 'Other Grain Growing' UNION ALL
	SELECT '0151', 'Sugar Cane Growing' UNION ALL
	SELECT '0152', 'Cotton Growing' UNION ALL
	SELECT '0159', 'Other Crop Growing n.e.c.' UNION ALL
	SELECT '0160', 'Dairy Cattle Farming' UNION ALL
	SELECT '0171', 'Poultry Farming (Meat)' UNION ALL
	SELECT '0172', 'Poultry Farming (Eggs)' UNION ALL
	SELECT '0180', 'Deer Farming' UNION ALL
	SELECT '0191', 'Horse Farming' UNION ALL
	SELECT '0192', 'Pig Farming' UNION ALL
	SELECT '0193', 'Beekeeping' UNION ALL
	SELECT '0199', 'Other Livestock Farming n.e.c.' UNION ALL
	SELECT '0201', 'Longline and Rack (Offshore) Aquaculture' UNION ALL
	SELECT '0202', 'Caged (Offshore) Aquaculture' UNION ALL
	SELECT '0203', 'Onshore Aquaculture' UNION ALL
	SELECT '0301', 'Forestry' UNION ALL
	SELECT '0302', 'Logging' UNION ALL
	SELECT '0411', 'Rock Lobster and Crab Potting' UNION ALL
	SELECT '0412', 'Prawn Fishing' UNION ALL
	SELECT '0413', 'Line Fishing' UNION ALL
	SELECT '0414', 'Fish Trawling, Seining and Netting' UNION ALL
	SELECT '0419', 'Other Fishing' UNION ALL
	SELECT '0420', 'Hunting and Trapping' UNION ALL
	SELECT '0510', 'Forestry Support Services' UNION ALL
	SELECT '0521', 'Cotton Ginning' UNION ALL
	SELECT '0522', 'Shearing Services' UNION ALL
	SELECT '0529', 'Other Agriculture and Fishing Support Services' UNION ALL
	SELECT '0600', 'Coal Mining' UNION ALL
	SELECT '0700', 'Oil and Gas Extraction' UNION ALL
	SELECT '0801', 'Iron Ore Mining' UNION ALL
	SELECT '0802', 'Bauxite Mining' UNION ALL
	SELECT '0803', 'Copper Ore Mining' UNION ALL
	SELECT '0804', 'Gold Ore Mining' UNION ALL
	SELECT '0805', 'Mineral Sand Mining' UNION ALL
	SELECT '0806', 'Nickel Ore Mining' UNION ALL
	SELECT '0807', 'Silver-Lead-Zinc Ore Mining' UNION ALL
	SELECT '0809', 'Other Metal Ore Mining' UNION ALL
	SELECT '0911', 'Gravel and Sand Quarrying' UNION ALL
	SELECT '0919', 'Other Construction Material Mining' UNION ALL
	SELECT '0990', 'Other Non-Metallic Mineral Mining and Quarrying' UNION ALL
	SELECT '1011', 'Petroleum Exploration' UNION ALL
	SELECT '1012', 'Mineral Exploration' UNION ALL
	SELECT '1090', 'Other Mining Support Services' UNION ALL
	SELECT '1111', 'Meat Processing' UNION ALL
	SELECT '1112', 'Poultry Processing' UNION ALL
	SELECT '1113', 'Cured Meat and Smallgoods Manufacturing' UNION ALL
	SELECT '1120', 'Seafood Processing' UNION ALL
	SELECT '1131', 'Milk and Cream Processing' UNION ALL
	SELECT '1132', 'Ice Cream Manufacturing' UNION ALL
	SELECT '1133', 'Cheese and Other Dairy Product Manufacturing' UNION ALL
	SELECT '1140', 'Fruit and Vegetable Processing' UNION ALL
	SELECT '1150', 'Oil and Fat Manufacturing' UNION ALL
	SELECT '1161', 'Grain Mill Product Manufacturing' UNION ALL
	SELECT '1162', 'Cereal, Pasta and Baking Mix Manufacturing' UNION ALL
	SELECT '1171', 'Bread Manufacturing (Factory-based)' UNION ALL
	SELECT '1172', 'Cake and Pastry Manufacturing (Factory-based)' UNION ALL
	SELECT '1173', 'Biscuit Manufacturing (Factory-based)' UNION ALL
	SELECT '1174', 'Bakery Product Manufacturing (Non-factory-based)' UNION ALL
	SELECT '1181', 'Sugar Manufacturing' UNION ALL
	SELECT '1182', 'Confectionery Manufacturing' UNION ALL
	SELECT '1191', 'Potato Crisps and Corn Chips Manufacturing' UNION ALL
	SELECT '1192', 'Prepared Animal and Bird Feed Manufacturing' UNION ALL
	SELECT '1199', 'Other Food Products Manufacturing n.e.c.' UNION ALL
	SELECT '1211', 'Soft Drink, Cordial and Syrup Manufacturing' UNION ALL
	SELECT '1212', 'Beer Manufacturing' UNION ALL
	SELECT '1213', 'Spirit Manufacturing' UNION ALL
	SELECT '1214', 'Wine and Other Alcoholic Beverage Manufacturing' UNION ALL
	SELECT '1220', 'Cigarette and Tobacco Product Manufacturing' UNION ALL
	SELECT '1311', 'Wool Scouring' UNION ALL
	SELECT '1312', 'Natural Fibre Textile Manufacturing' UNION ALL
	SELECT '1313', 'Synthetic Fibre Textile Manufacturing' UNION ALL
	SELECT '1320', 'Leather Tanning and Fur Dressing' UNION ALL
	SELECT '1331', 'Textile Floor Covering Manufacturing' UNION ALL
	SELECT '1332', 'Rope, Cordage and Twine Manufacturing' UNION ALL
	SELECT '1333', 'Cut and Sewn Textile Product Manufacturing' UNION ALL
	SELECT '1334', 'Textile Finishing and Other Textile Product Manufacturing' UNION ALL
	SELECT '1340', 'Knitted Product Manufacturing' UNION ALL
	SELECT '1351', 'Clothing Manufacturing' UNION ALL
	SELECT '1352', 'Footwear Manufacturing' UNION ALL
	SELECT '1411', 'Log Sawmilling' UNION ALL
	SELECT '1412', 'Wood Chipping' UNION ALL
	SELECT '1413', 'Timber Resawing and Dressing' UNION ALL
	SELECT '1491', 'Prefabricated Wooden Building Manufacturing' UNION ALL
	SELECT '1492', 'Wooden Structural Fittings and Components Manufacturing' UNION ALL
	SELECT '1493', 'Veneer and Plywood Manufacturing' UNION ALL
	SELECT '1494', 'Reconstituted Wood Product Manufacturing' UNION ALL
	SELECT '1499', 'Other Wood Product Manufacturing n.e.c.' UNION ALL
	SELECT '1510', 'Pulp, Paper and Paperboard Manufacturing' UNION ALL
	SELECT '1521', 'Corrugated Paperboard and Paperboard Container Manufacturing' UNION ALL
	SELECT '1522', 'Paper Bag and Sack Manufacturing' UNION ALL
	SELECT '1523', 'Paper Stationery Manufacturing' UNION ALL
	SELECT '1524', 'Sanitary Paper Product Manufacturing' UNION ALL
	SELECT '1529', 'Other Converted Paper Product Manufacturing' UNION ALL
	SELECT '1611', 'Printing' UNION ALL
	SELECT '1612', 'Printing Support Services' UNION ALL
	SELECT '1620', 'Reproduction of Recorded Media' UNION ALL
	SELECT '1701', 'Petroleum Refining and Petroleum Fuels Manufacturing' UNION ALL
	SELECT '1709', 'Other Petroleum and Coal Product Manufacturing' UNION ALL
	SELECT '1811', 'Industrial Gases Manufacturing' UNION ALL
	SELECT '1812', 'Basic Organic Chemical Manufacturing' UNION ALL
	SELECT '1813', 'Basic Inorganic Chemical Manufacturing' UNION ALL
	SELECT '1821', 'Synthetic Resin and Synthetic Rubber Manufacturing' UNION ALL
	SELECT '1829', 'Other Basic Polymer Manufacturing' UNION ALL
	SELECT '1831', 'Fertiliser Manufacturing' UNION ALL
	SELECT '1832', 'Pesticide Manufacturing' UNION ALL
	SELECT '1841', 'Human Pharmaceutical and Medicinal Product Manufacturing' UNION ALL
	SELECT '1842', 'Veterinary Pharmaceutical and Medicinal Product Manufacturing' UNION ALL
	SELECT '1851', 'Cleaning Compound Manufacturing' UNION ALL
	SELECT '1852', 'Cosmetic and Toiletry Preparation Manufacturing' UNION ALL
	SELECT '1891', 'Photographic Chemical Manufacturing' UNION ALL
	SELECT '1892', 'Explosives Manufacturing' UNION ALL
	SELECT '1899', 'Other Basic Chemical Product Manufacturing n.e.c.' UNION ALL
	SELECT '1911', 'Polymer Film and Sheet Packaging Material Manufacturing' UNION ALL
	SELECT '1912', 'Rigid and Semi Rigid Polymer Product Manufacturing' UNION ALL
	SELECT '1913', 'Polymer Foam Product Manufacturing' UNION ALL
	SELECT '1914', 'Tyre Manufacturing' UNION ALL
	SELECT '1915', 'Adhesive Manufacturing' UNION ALL
	SELECT '1916', 'Paint and Coatings Manufacturing' UNION ALL
	SELECT '1919', 'Other Polymer Product Manufacturing' UNION ALL
	SELECT '1920', 'Natural Rubber Product Manufacturing' UNION ALL
	SELECT '2010', 'Glass and Glass Product Manufacturing' UNION ALL
	SELECT '2021', 'Clay Brick Manufacturing' UNION ALL
	SELECT '2029', 'Other Ceramic Product Manufacturing' UNION ALL
	SELECT '2031', 'Cement and Lime Manufacturing' UNION ALL
	SELECT '2032', 'Plaster and Gypsum Product Manufacturing' UNION ALL
	SELECT '2033', 'Ready-Mixed Concrete Manufacturing' UNION ALL
	SELECT '2034', 'Concrete Product Manufacturing' UNION ALL
	SELECT '2090', 'Other Non-Metallic Mineral Product Manufacturing' UNION ALL
	SELECT '2110', 'Iron Smelting and Steel Manufacturing' UNION ALL
	SELECT '2121', 'Iron and Steel Casting' UNION ALL
	SELECT '2122', 'Steel Pipe and Tube Manufacturing' UNION ALL
	SELECT '2131', 'Alumina Production' UNION ALL
	SELECT '2132', 'Aluminium Smelting' UNION ALL
	SELECT '2133', 'Copper, Silver, Lead, and Zinc Smelting and Refining' UNION ALL
	SELECT '2139', 'Other Basic Non-Ferrous Metal Manufacturing' UNION ALL
	SELECT '2141', 'Non-Ferrous Metal Casting' UNION ALL
	SELECT '2142', 'Aluminium Rolling, Drawing, Extruding' UNION ALL
	SELECT '2149', 'Other Basic Non-Ferrous Metal Product Manufacturing' UNION ALL
	SELECT '2210', 'Iron and Steel Forging' UNION ALL
	SELECT '2221', 'Structural Steel Fabricating' UNION ALL
	SELECT '2222', 'Prefabricated Metal Building Manufacturing' UNION ALL
	SELECT '2223', 'Architectural Aluminium Product Manufacturing' UNION ALL
	SELECT '2224', 'Metal Roof and Guttering Manufacturing (except Aluminium)' UNION ALL
	SELECT '2229', 'Other Structural Metal Product Manufacturing' UNION ALL
	SELECT '2231', 'Boiler, Tank and Other Heavy Gauge Metal Container Manufacturing' UNION ALL
	SELECT '2239', 'Other Metal Container Manufacturing' UNION ALL
	SELECT '2240', 'Other Sheet Metal Product Manufacturing' UNION ALL
	SELECT '2291', 'Spring and Wire Product Manufacturing' UNION ALL
	SELECT '2292', 'Nut, Bolt, Screw and Rivet Manufacturing' UNION ALL
	SELECT '2293', 'Metal Coating and Finishing' UNION ALL
	SELECT '2299', 'Other Fabricated Metal Product Manufacturing n.e.c.' UNION ALL
	SELECT '2311', 'Motor Vehicle Manufacturing' UNION ALL
	SELECT '2312', 'Motor Vehicle Body and Trailer Manufacturing' UNION ALL
	SELECT '2313', 'Automotive Electrical Components Manufacturing' UNION ALL
	SELECT '2319', 'Other Motor Vehicle Parts Manufacturing' UNION ALL
	SELECT '2391', 'Shipbuilding and Repair Services' UNION ALL
	SELECT '2392', 'Boatbuilding and Repair Services' UNION ALL
	SELECT '2393', 'Railway Rolling Stock Manufacturing and Repair Services' UNION ALL
	SELECT '2394', 'Aircraft Manufacturing and Repair Services' UNION ALL
	SELECT '2399', 'Other Transport Equipment Manufacturing n.e.c.' UNION ALL
	SELECT '2411', 'Photographic, Optical and Ophthalmic Equipment Manufacturing' UNION ALL
	SELECT '2412', 'Medical and Surgical Equipment Manufacturing' UNION ALL
	SELECT '2419', 'Other Professional and Scientific Equipment Manufacturing' UNION ALL
	SELECT '2421', 'Computer and Electronic Office Equipment Manufacturing' UNION ALL
	SELECT '2422', 'Communications Equipment Manufacturing' UNION ALL
	SELECT '2429', 'Other Electronic Equipment Manufacturing' UNION ALL
	SELECT '2431', 'Electric Cable and Wire Manufacturing' UNION ALL
	SELECT '2432', 'Electric Lighting Equipment Manufacturing' UNION ALL
	SELECT '2439', 'Other Electrical Equipment Manufacturing' UNION ALL
	SELECT '2441', 'Whiteware Appliance Manufacturing' UNION ALL
	SELECT '2449', 'Other Domestic Appliance Manufacturing' UNION ALL
	SELECT '2451', 'Pumps and Compressors Manufacturing' UNION ALL
	SELECT '2452', 'Fixed Space Heating, Cooling and Ventilation Equipment Manufacturing' UNION ALL
	SELECT '2461', 'Agricultural Machinery and Equipment Manufacturing' UNION ALL
	SELECT '2462', 'Mining and Construction Machinery Manufacturing' UNION ALL
	SELECT '2463', 'Machine Tool and Parts Manufacturing' UNION ALL
	SELECT '2469', 'Other Specialised Machinery and Equipment Manufacturing' UNION ALL
	SELECT '2491', 'Lifting and Material Handling Equipment Manufacturing' UNION ALL
	SELECT '2499', 'Other Machinery and Equipment Manufacturing n.e.c.' UNION ALL
	SELECT '2511', 'Wooden Furniture and Upholstered Seat Manufacturing' UNION ALL
	SELECT '2512', 'Metal Furniture Manufacturing' UNION ALL
	SELECT '2513', 'Mattress Manufacturing' UNION ALL
	SELECT '2519', 'Other Furniture Manufacturing' UNION ALL
	SELECT '2591', 'Jewellery and Silverware Manufacturing' UNION ALL
	SELECT '2592', 'Toy, Sporting and Recreational Product Manufacturing' UNION ALL
	SELECT '2599', 'Other Manufacturing n.e.c.' UNION ALL
	SELECT '2611', 'Fossil Fuel Electricity Generation' UNION ALL
	SELECT '2612', 'Hydro-electricity Generation' UNION ALL
	SELECT '2619', 'Other Electricity Generation' UNION ALL
	SELECT '2620', 'Electricity Transmission' UNION ALL
	SELECT '2630', 'Electricity Distribution' UNION ALL
	SELECT '2640', 'On Selling Electricity and Electricity Market Operation' UNION ALL
	SELECT '2700', 'Gas Supply' UNION ALL
	SELECT '2811', 'Water Supply' UNION ALL
	SELECT '2812', 'Sewerage and Drainage Services' UNION ALL
	SELECT '2911', 'Solid Waste Collection Services' UNION ALL
	SELECT '2919', 'Other Waste Collection Services' UNION ALL
	SELECT '2921', 'Waste Treatment and Disposal Services' UNION ALL
	SELECT '2922', 'Waste Remediation and Materials Recovery Services' UNION ALL
	SELECT '3011', 'House Construction' UNION ALL
	SELECT '3019', 'Other Residential Building Construction' UNION ALL
	SELECT '3020', 'Non-Residential Building Construction' UNION ALL
	SELECT '3101', 'Road and Bridge Construction' UNION ALL
	SELECT '3109', 'Other Heavy and Civil Engineering Construction' UNION ALL
	SELECT '3211', 'Land Development and Subdivision' UNION ALL
	SELECT '3212', 'Site Preparation Services' UNION ALL
	SELECT '3221', 'Concreting Services' UNION ALL
	SELECT '3222', 'Bricklaying Services' UNION ALL
	SELECT '3223', 'Roofing Services' UNION ALL
	SELECT '3224', 'Structural Steel Erection Services' UNION ALL
	SELECT '3231', 'Plumbing Services' UNION ALL
	SELECT '3232', 'Electrical Services' UNION ALL
	SELECT '3233', 'Air Conditioning and Heating Services' UNION ALL
	SELECT '3234', 'Fire and Security Alarm Installation Services' UNION ALL
	SELECT '3239', 'Other Building Installation Services' UNION ALL
	SELECT '3241', 'Plastering and Ceiling Services' UNION ALL
	SELECT '3242', 'Carpentry Services' UNION ALL
	SELECT '3243', 'Tiling and Carpeting Services' UNION ALL
	SELECT '3244', 'Painting and Decorating Services' UNION ALL
	SELECT '3245', 'Glazing Services' UNION ALL
	SELECT '3291', 'Landscape Construction Services' UNION ALL
	SELECT '3292', 'Hire of Construction Machinery with Operator' UNION ALL
	SELECT '3299', 'Other Construction Services n.e.c.' UNION ALL
	SELECT '3311', 'Wool Wholesaling' UNION ALL
	SELECT '3312', 'Cereal Grain Wholesaling' UNION ALL
	SELECT '3319', 'Other Agricultural Product Wholesaling' UNION ALL
	SELECT '3321', 'Petroleum Product Wholesaling' UNION ALL
	SELECT '3322', 'Metal and Mineral Wholesaling' UNION ALL
	SELECT '3323', 'Industrial and Agricultural Chemical Product Wholesaling' UNION ALL
	SELECT '3331', 'Timber Wholesaling' UNION ALL
	SELECT '3332', 'Plumbing Goods Wholesaling' UNION ALL
	SELECT '3339', 'Other Hardware Goods Wholesaling' UNION ALL
	SELECT '3411', 'Agricultural and Construction Machinery Wholesaling' UNION ALL
	SELECT '3419', 'Other Specialised Industrial Machinery and Equipment Wholesaling' UNION ALL
	SELECT '3491', 'Professional and Scientific Goods Wholesaling' UNION ALL
	SELECT '3492', 'Computer and Computer Peripherals Wholesaling' UNION ALL
	SELECT '3493', 'Telecommunication Goods Wholesaling' UNION ALL
	SELECT '3494', 'Other Electrical and Electronic Goods Wholesaling' UNION ALL
	SELECT '3499', 'Other Machinery and Equipment Wholesaling n' UNION ALL
	SELECT '3501', 'Car Wholesaling' UNION ALL
	SELECT '3502', 'Commercial Vehicle Wholesaling' UNION ALL
	SELECT '3503', 'Trailer and Other Motor Vehicle Wholesaling' UNION ALL
	SELECT '3504', 'Motor Vehicle New Part Wholesaling' UNION ALL
	SELECT '3505', 'Motor Vehicle Dismantling and Used Part Wholesaling' UNION ALL
	SELECT '3601', 'General Line Groceries Wholesaling' UNION ALL
	SELECT '3602', 'Meat, Poultry and Smallgoods Wholesaling' UNION ALL
	SELECT '3603', 'Dairy Produce Wholesaling' UNION ALL
	SELECT '3604', 'Fish and Seafood Wholesaling' UNION ALL
	SELECT '3605', 'Fruit and Vegetable Wholesaling' UNION ALL
	SELECT '3606', 'Liquor and Tobacco Product Wholesaling' UNION ALL
	SELECT '3609', 'Other Grocery Wholesaling' UNION ALL
	SELECT '3711', 'Textile Product Wholesaling' UNION ALL
	SELECT '3712', 'Clothing and Footwear Wholesaling' UNION ALL
	SELECT '3720', 'Pharmaceutical and Toiletry Goods Wholesaling' UNION ALL
	SELECT '3731', 'Furniture and Floor Coverings Wholesaling' UNION ALL
	SELECT '3732', 'Jewellery and Watch Wholesaling' UNION ALL
	SELECT '3733', 'Kitchen and Dining Ware Wholesaling' UNION ALL
	SELECT '3734', 'Toy and Sporting Goods Wholesaling' UNION ALL
	SELECT '3735', 'Book and Magazine Wholesaling' UNION ALL
	SELECT '3736', 'Paper Product Wholesaling' UNION ALL
	SELECT '3739', 'Other Goods Wholesaling n.e.c.' UNION ALL
	SELECT '3800', 'Commission Based Wholesaling' UNION ALL
	SELECT '3911', 'Car Retailing' UNION ALL
	SELECT '3912', 'Motor Cycle Retailing' UNION ALL
	SELECT '3913', 'Trailer and Other Motor Vehicle Retailing' UNION ALL
	SELECT '3921', 'Motor Vehicle Parts Retailing' UNION ALL
	SELECT '3922', 'Tyre Retailing' UNION ALL
	SELECT '4000', 'Fuel Retailing' UNION ALL
	SELECT '4110', 'Supermarket and Grocery Stores' UNION ALL
	SELECT '4121', 'Fresh Meat, Fish and Poultry Retailing' UNION ALL
	SELECT '4122', 'Fruit and Vegetable Retailing' UNION ALL
	SELECT '4123', 'Liquor Retailing' UNION ALL
	SELECT '4129', 'Other Specialised Food Retailing' UNION ALL
	SELECT '4211', 'Furniture Retailing' UNION ALL
	SELECT '4212', 'Floor Coverings Retailing' UNION ALL
	SELECT '4213', 'Houseware Retailing' UNION ALL
	SELECT '4214', 'Manchester and Other Textile Goods Retailing' UNION ALL
	SELECT '4221', 'Electrical, Electronic and Gas Appliance Retailing' UNION ALL
	SELECT '4222', 'Computer and Computer Peripherals Retailing' UNION ALL
	SELECT '4229', 'Other Electrical and Electronic Goods Retailing' UNION ALL
	SELECT '4231', 'Hardware and Building Supplies Retailing' UNION ALL
	SELECT '4232', 'Garden Supplies Retailing' UNION ALL
	SELECT '4241', 'Sport and Camping Equipment Retailing' UNION ALL
	SELECT '4242', 'Entertainment Media Retailing' UNION ALL
	SELECT '4243', 'Toy and Game Retailing' UNION ALL
	SELECT '4244', 'Newspaper and Book Retailing' UNION ALL
	SELECT '4245', 'Marine Equipment Retailing' UNION ALL
	SELECT '4251', 'Clothing Retailing' UNION ALL
	SELECT '4252', 'Footwear Retailing' UNION ALL
	SELECT '4253', 'Watch and Jewellery Retailing' UNION ALL
	SELECT '4259', 'Other Personal Accessories Retailing' UNION ALL
	SELECT '4260', 'Department Stores' UNION ALL
	SELECT '4271', 'Pharmaceutical, Cosmetic and Toiletry Goods Retailing' UNION ALL
	SELECT '4272', 'Stationery Goods Retailing' UNION ALL
	SELECT '4273', 'Antique and Used Goods Retailing' UNION ALL
	SELECT '4274', 'Flower Retailing' UNION ALL
	SELECT '4279', 'Other Store-Based Retailing n.e.c.' UNION ALL
	SELECT '4310', 'Non Store Retailing' UNION ALL
	SELECT '4320', 'Retail Commission Based Buying and/or Selling' UNION ALL
	SELECT '4400', 'Accommodation' UNION ALL
	SELECT '4511', 'Cafes and Restaurants' UNION ALL
	SELECT '4512', 'Takeaway Food Services' UNION ALL
	SELECT '4513', 'Catering Services' UNION ALL
	SELECT '4520', 'Pubs, Taverns and Bars' UNION ALL
	SELECT '4530', 'Clubs (Hospitality)' UNION ALL
	SELECT '4610', 'Road Freight Transport' UNION ALL
	SELECT '4621', 'Interurban and Rural Bus Transport' UNION ALL
	SELECT '4622', 'Urban Bus Transport (Including Tramway)' UNION ALL
	SELECT '4623', 'Taxi and Other Road Transport' UNION ALL
	SELECT '4710', 'Rail Freight Transport' UNION ALL
	SELECT '4720', 'Rail Passenger Transport' UNION ALL
	SELECT '4810', 'Water Freight Transport' UNION ALL
	SELECT '4820', 'Water Passenger Transport' UNION ALL
	SELECT '4900', 'Air and Space Transport' UNION ALL
	SELECT '5010', 'Scenic and Sightseeing Transport' UNION ALL
	SELECT '5021', 'Pipeline Transport' UNION ALL
	SELECT '5029', 'Other Transport n.e.c.' UNION ALL
	SELECT '5101', 'Postal Services' UNION ALL
	SELECT '5102', 'Courier Pick-up and Delivery Services' UNION ALL
	SELECT '5211', 'Stevedoring Services' UNION ALL
	SELECT '5212', 'Port and Water Transport Terminal Operations' UNION ALL
	SELECT '5219', 'Other Water Transport Support Services' UNION ALL
	SELECT '5220', 'Airport Operations and Other Air Transport Support Services' UNION ALL
	SELECT '5291', 'Customs Agency Services' UNION ALL
	SELECT '5292', 'Freight Forwarding Services' UNION ALL
	SELECT '5299', 'Other Transport Support Services n.e.c' UNION ALL
	SELECT '5301', 'Grain Storage Services' UNION ALL
	SELECT '5309', 'Other Warehousing and Storage Services' UNION ALL
	SELECT '5411', 'Newspaper Publishing' UNION ALL
	SELECT '5412', 'Magazine and Other Periodical Publishing' UNION ALL
	SELECT '5413', 'Book Publishing' UNION ALL
	SELECT '5414', 'Directory and Mailing List Publishing' UNION ALL
	SELECT '5419', 'Other Publishing (except Software, Music and Internet)' UNION ALL
	SELECT '5420', 'Software Publishing' UNION ALL
	SELECT '5511', 'Motion Picture and Video Production' UNION ALL
	SELECT '5512', 'Motion Picture and Video Distribution' UNION ALL
	SELECT '5513', 'Motion Picture Exhibition' UNION ALL
	SELECT '5514', 'Postproduction Services and Other Motion Picture and Video Activities' UNION ALL
	SELECT '5521', 'Music Publishing' UNION ALL
	SELECT '5522', 'Music and Other Sound Recording Activities' UNION ALL
	SELECT '5610', 'Radio Broadcasting' UNION ALL
	SELECT '5621', 'Free-to-Air Television Broadcasting' UNION ALL
	SELECT '5622', 'Cable and Other Subscription Programming' UNION ALL
	SELECT '5700', 'Internet Publishing and Broadcasting' UNION ALL
	SELECT '5801', 'Wired Telecommunications Network Operation' UNION ALL
	SELECT '5802', 'Other Telecommunications Network Operation' UNION ALL
	SELECT '5809', 'Other Telecommunications Services' UNION ALL
	SELECT '5910', 'Internet Access Services' UNION ALL
	SELECT '5921', 'Data Processing and Web Hosting Services' UNION ALL
	SELECT '5922', 'Electronic Information Storage Services' UNION ALL
	SELECT '6010', 'Libraries and Archives' UNION ALL
	SELECT '6020', 'Other Information Services' UNION ALL
	SELECT '6210', 'Central Banking' UNION ALL
	SELECT '6221', 'Banking' UNION ALL
	SELECT '6222', 'Building Society Operation' UNION ALL
	SELECT '6223', 'Credit Union Operation' UNION ALL
	SELECT '6229', 'Other Depository Financial Intermediation' UNION ALL
	SELECT '6230', 'Non-depository Financing' UNION ALL
	SELECT '6240', 'Financial Asset Investing' UNION ALL
	SELECT '6310', 'Life Insurance' UNION ALL
	SELECT '6321', 'Health Insurance' UNION ALL
	SELECT '6322', 'General Insurance' UNION ALL
	SELECT '6330', 'Superannuation Funds' UNION ALL
	SELECT '6411', 'Financial Asset Broking Services' UNION ALL
	SELECT '6419', 'Other Auxiliary Finance and Investment Services' UNION ALL
	SELECT '6420', 'Auxiliary Insurance Services' UNION ALL
	SELECT '6611', 'Passenger Car Rental and Hiring' UNION ALL
	SELECT '6619', 'Other Motor Vehicle and Transport Equipment Rental and Hiring' UNION ALL
	SELECT '6620', 'Farm Animals and Bloodstock Leasing' UNION ALL
	SELECT '6631', 'Heavy Machinery and Scaffolding Rental and Hiring' UNION ALL
	SELECT '6632', 'Video and Other Electronic Media Rental' UNION ALL
	SELECT '6639', 'Other Goods and Equipment Rental and Hiring n.e.c.' UNION ALL
	SELECT '6640', 'Non-Financial Intangible Assets (except Copyrights) Leasing' UNION ALL
	SELECT '6711', 'Residential Property Operators' UNION ALL
	SELECT '6712', 'Non-Residential Property Operators' UNION ALL
	SELECT '6720', 'Real Estate Services' UNION ALL
	SELECT '6910', 'Scientific Research Services' UNION ALL
	SELECT '6921', 'Architectural Services' UNION ALL
	SELECT '6922', 'Surveying and Mapping Services' UNION ALL
	SELECT '6923', 'Engineering Design and Engineering Consulting Services' UNION ALL
	SELECT '6924', 'Other Specialised Design Services' UNION ALL
	SELECT '6925', 'Scientific Testing and Analysis Services' UNION ALL
	SELECT '6931', 'Legal Services' UNION ALL
	SELECT '6932', 'Accounting Services' UNION ALL
	SELECT '6940', 'Advertising Services' UNION ALL
	SELECT '6950', 'Market Research and Statistical Services' UNION ALL
	SELECT '6961', 'Corporate Head Office Management Services' UNION ALL
	SELECT '6962', 'Management Advice and Other Consulting Services' UNION ALL
	SELECT '6970', 'Veterinary Services' UNION ALL
	SELECT '6991', 'Professional Photographic Services' UNION ALL
	SELECT '6999', 'Other Professional, Scientific and Technical Services n.e.c.' UNION ALL
	SELECT '7000', 'Computer Systems Design and Related Services' UNION ALL
	SELECT '7211', 'Employment Placement and Recruitment Services' UNION ALL
	SELECT '7212', 'Labour Supply Services' UNION ALL
	SELECT '7220', 'Travel Agency and Tour Arrangement Services' UNION ALL
	SELECT '7291', 'Office Administrative Services' UNION ALL
	SELECT '7292', 'Document Preparation Services' UNION ALL
	SELECT '7293', 'Credit Reporting and Debt Collection Services' UNION ALL
	SELECT '7294', 'Call Centre Operation' UNION ALL
	SELECT '7299', 'Other Administrative Services n.e.c.' UNION ALL
	SELECT '7311', 'Buildings Cleaning Services' UNION ALL
	SELECT '7312', 'Buildings Pest Control Services' UNION ALL
	SELECT '7313', 'Gardening Services' UNION ALL
	SELECT '7320', 'Packaging Services' UNION ALL
	SELECT '7510', 'Central Government Administration' UNION ALL
	SELECT '7520', 'State Government Administration' UNION ALL
	SELECT '7530', 'Local Government Administration' UNION ALL
	SELECT '7540', 'Justice' UNION ALL
	SELECT '7551', 'Domestic Government Representation' UNION ALL
	SELECT '7552', 'Foreign Government Representation' UNION ALL
	SELECT '7600', 'Defence' UNION ALL
	SELECT '7711', 'Police Services' UNION ALL
	SELECT '7712', 'Investigation and Security Services' UNION ALL
	SELECT '7713', 'Fire Protection and Other Emergency Services (except Ambulance Services)' UNION ALL
	SELECT '7714', 'Correctional and Detention Services' UNION ALL
	SELECT '7719', 'Other Public Order and Safety Services' UNION ALL
	SELECT '7720', 'Regulatory Services' UNION ALL
	SELECT '8010', 'Preschool Education' UNION ALL
	SELECT '8021', 'Primary Education' UNION ALL
	SELECT '8022', 'Secondary Education' UNION ALL
	SELECT '8023', 'Combined Primary and Secondary Education' UNION ALL
	SELECT '8024', 'Special School Education' UNION ALL
	SELECT '8101', 'Technical and Vocational Education and Training' UNION ALL
	SELECT '8102', 'Higher Education' UNION ALL
	SELECT '8211', 'Sports and Physical Recreation Instruction' UNION ALL
	SELECT '8212', 'Arts Education' UNION ALL
	SELECT '8219', 'Adult, Community and Other Education n.e.c.' UNION ALL
	SELECT '8220', 'Educational Support Services' UNION ALL
	SELECT '8401', 'Hospitals (except Psychiatric Hospitals)' UNION ALL
	SELECT '8402', 'Psychiatric Hospitals' UNION ALL
	SELECT '8511', 'General Practice Medical Services' UNION ALL
	SELECT '8512', 'Specialist Medical Services' UNION ALL
	SELECT '8520', 'Pathology and Diagnostic Imaging Services' UNION ALL
	SELECT '8531', 'Dental Services' UNION ALL
	SELECT '8532', 'Optometry and Optical Dispensing' UNION ALL
	SELECT '8533', 'Physiotherapy Services' UNION ALL
	SELECT '8534', 'Chiropractic and Osteopathic Services' UNION ALL
	SELECT '8539', 'Other Allied Health Services' UNION ALL
	SELECT '8591', 'Ambulance Services' UNION ALL
	SELECT '8599', 'Other Health Care Services n.e.c.' UNION ALL
	SELECT '8601', 'Aged Care Residential Services' UNION ALL
	SELECT '8609', 'Other Residential Care Services' UNION ALL
	SELECT '8710', 'Child Care Services' UNION ALL
	SELECT '8790', 'Other Social Assistance Services' UNION ALL
	SELECT '8910', 'Museum Operation' UNION ALL
	SELECT '8921', 'Zoological and Botanic Gardens Operation' UNION ALL
	SELECT '8922', 'Nature Reserves and Conservation Parks Operation' UNION ALL
	SELECT '9001', 'Performing Arts Operation' UNION ALL
	SELECT '9002', 'Creative Artists, Musicians, Writers and Performers' UNION ALL
	SELECT '9003', 'Performing Arts Venue Operation' UNION ALL
	SELECT '9111', 'Health and Fitness Centres and Gymnasia Operation' UNION ALL
	SELECT '9112', 'Sport and Physical Recreation Clubs and Sports Professionals' UNION ALL
	SELECT '9113', 'Sports and Physical Recreation Venues, Grounds and Facilities Operation' UNION ALL
	SELECT '9114', 'Sport and Physical Recreation Administrative Service' UNION ALL
	SELECT '9121', 'Horse and Dog Racing Administration and Track Operation' UNION ALL
	SELECT '9129', 'Other Horse and Dog Racing Activities' UNION ALL
	SELECT '9131', 'Amusement Parks and Centres Operation' UNION ALL
	SELECT '9139', 'Amusement and Other Recreation Activities n.e.c.' UNION ALL
	SELECT '9201', 'Casino Operation' UNION ALL
	SELECT '9202', 'Lottery Operation' UNION ALL
	SELECT '9209', 'Other Gambling Activities' UNION ALL
	SELECT '9411', 'Automotive Electrical Services' UNION ALL
	SELECT '9412', 'Automotive Body, Paint and Interior Repair' UNION ALL
	SELECT '9419', 'Other Automotive Repair and Maintenance' UNION ALL
	SELECT '9421', 'Domestic Appliance Repair and Maintenance' UNION ALL
	SELECT '9422', 'Electronic (except Domestic Appliance) and Precision Equipment Repair and Maintenance' UNION ALL
	SELECT '9429', 'Other Machinery and Equipment Repair and Maintenance' UNION ALL
	SELECT '9491', 'Clothing and Footwear Repair' UNION ALL
	SELECT '9499', 'Other Repair and Maintenance n.e.c.' UNION ALL
	SELECT '9511', 'Hairdressing and Beauty Services' UNION ALL
	SELECT '9512', 'Diet and Weight Reduction Centre Operation' UNION ALL
	SELECT '9520', 'Funeral, Crematorium and Cemetery Services' UNION ALL
	SELECT '9531', 'Laundry and Dry-Cleaning Services' UNION ALL
	SELECT '9532', 'Photographic Film Processing' UNION ALL
	SELECT '9533', 'Parking Services' UNION ALL
	SELECT '9534', 'Brothel Keeping and Prostitution Services' UNION ALL
	SELECT '9539', 'Other Personal Services n.e.c.' UNION ALL
	SELECT '9540', 'Religious Services' UNION ALL
	SELECT '9551', 'Business and Professional Association Services' UNION ALL
	SELECT '9552', 'Labour Association Services' UNION ALL
	SELECT '9559', 'Other Interest Group Services n.e.c.' UNION ALL
	SELECT '9601', 'Private Households Employing Staff' UNION ALL
	SELECT '9602', 'Undifferentiated Goods-Producing Activities of Private Households for Own Use' UNION ALL
	SELECT '9603', 'Undifferentiated Service-Producing Activities of Private Households for Own Use'

), cANZSIC AS (

	SELECT
		d.DivisionCode
		,d.DivisionDescription
		,sd.SubdivisionCode
		,sd.SubdivisionDescription
		,g.GroupCode
		,g.GroupDescription
		,c.ClassCode
		,c.ClassDescription
		,d.DivisionCode + c.ClassCode AS ANZSICCode
	FROM
		cDivision AS d
		INNER JOIN cSubdivision AS sd
			ON d.DivisionCode = sd.DivisionCode
		INNER JOIN cGroup AS g
			ON LEFT(g.GroupCode, 2) = sd.SubdivisionCode
		INNER JOIN cClass AS c
			ON LEFT(c.ClassCode, 3) = g.GroupCode

)
MERGE INTO dbo.dimANZSIC AS tgt
USING cANZSIC as src
	ON tgt.ANZSICCode = src.ANZSICCode

WHEN MATCHED THEN UPDATE
SET
	DivisionCode			= src.DivisionCode
	,DivisionDescription	= src.DivisionDescription
	,SubdivisionCode		= src.SubdivisionCode
	,SubdivisionDescription	= src.SubdivisionDescription
	,GroupCode				= src.GroupCode
	,GroupDescription		= src.GroupDescription
	,ClassCode				= src.ClassCode
	,ClassDescription		= src.ClassDescription
	,ANZSICCode				= src.ANZSICCode

WHEN NOT MATCHED THEN
INSERT(	DivisionCode
		,DivisionDescription
		,SubdivisionCode
		,SubdivisionDescription
		,GroupCode
		,GroupDescription
		,ClassCode
		,ClassDescription
		,ANZSICCode
	)
VALUES(	src.DivisionCode
		,src.DivisionDescription
		,src.SubdivisionCode
		,src.SubdivisionDescription
		,src.GroupCode
		,src.GroupDescription
		,src.ClassCode
		,src.ClassDescription
		,src.ANZSICCode
)
;
GO