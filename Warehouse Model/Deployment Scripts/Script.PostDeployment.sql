﻿/* Dimension population */
PRINT '';
PRINT CONVERT(VARCHAR(23), SYSDATETIME(), 121) + ' - Running data population scripts';
PRINT '';

:r ".\Data Population\Populate dimHolidayDate.sql"
:r ".\Data Population\Populate dimDate.sql"
:r ".\Data Population\Populate dimTime.sql"
:r ".\Data Population\Populate dimContinent.sql"
:r ".\Data Population\Populate dimCountry.sql"
:r ".\Data Population\Populate dimCountryContinent.sql"
:r ".\Data Population\Populate dimCurrency.sql"
:r ".\Data Population\Populate dimANZSIC.sql"
:r ".\Data Population\Populate dimNZRegion.sql"
:r ".\Data Population\Populate dimTopLevelDomain.sql"
:r ".\Data Population\Populate dimLanguages.sql"
:r ".\Data Population\Populate dimHTTPStatusCode.sql"
:r ".\Data Population\Populate dimFTPStatusCode.sql"
:r ".\Data Population\Populate dimChemicalElements.sql"
:r ".\Data Population\Populate dimUSState.sql"
:r ".\Data Population\Populate dimGender.sql"
:r ".\Data Population\Populate dimNZGSTRate.sql"

/* Post deploy stats */
:r ".\Post Deploy\Display SSDT deploy duration.sql"