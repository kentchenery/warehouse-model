﻿CREATE PROCEDURE [dbo].[dimDateAdd]
    @StartDate      DATE = '1900-01-01'
    ,@EndDate       DATE = '2199-12-31'
    ,@TaxStartDay   TINYINT = 1
    ,@TaxStartMonth TINYINT = 4
AS
/*
---
Description:   Procedure to add new dates to the dbo.dimDate dimension.  It generates the dates between the two dates supplied calculating the various attributes for the date dimension.

Parameters:
  - StartDate:      The earliest date to manage/update
  - EndDate:        The latest date to manage/update
  - TaxStartDay:    The day of the month the tax year starts
  - TaxStartMonth:  The month that the tax year starts

Returns:
  (none)

Examples:
  - EXEC dbo.dimDateAdd
  - EXEC dbo.dimDateAdd @StartDate = '2013-01-01', @EndDate = '2013-12-31'
...
 */
BEGIN;
    /* Turn of row counting */
    SET NOCOUNT ON;

    /* Verify @EndDate > @StartDate */
    IF (@StartDate > @EndDate)
    BEGIN;
        /* Swap the dates */
        DECLARE @StartEndDateFix    DATE;

        SET @StartEndDateFix = @StartDate;
        SET @StartDate = @EndDate;
        SET @EndDate = @StartEndDateFix;

    END;

    CREATE TABLE #dimDate(
        [DateID] [int] NOT NULL,
        [DateKey] [date] NULL,
        [DateDisplayShort] [nvarchar](10) NULL,
        [DateDisplayLong] [nvarchar](50) NULL,
        [Year] [smallint] NULL,
        [Quarter] [tinyint] NULL,
        [QuarterName] [nvarchar](20) NULL,
        [QuarterID] [int] NULL,
        [Month] [smallint] NULL,
        [MonthName] [nvarchar](20) NULL,
        [MonthID] [int] NULL,
        [Day] [tinyint] NULL,
        [WeekdayName] [nvarchar](20) NULL,
        [DayOfWeek] [tinyint] NULL,
        [NthWeekDayOfMonth] [smallint] NULL,
        [NthWeekDayOfYear] [smallint] NULL,
        [ISOWeek] [tinyint] NULL,
        [ISOYear] [smallint] NULL,
        [IsWeekDay] [char](1) NULL,
        [IsWeekend] [char](1) NULL,
        [IsBusinessDay] AS CASE WHEN IsWeekDay = 'Y' AND IsPublicHoliday = 'N' THEN 'Y' ELSE 'N' END,
        [IsPublicHoliday] [char](1) NULL,
        [PublicHolidayName] [nvarchar](50) NULL,
        [HolidayDateID] [smallint] NULL,
        [TaxYear] [smallint] NULL,
        [TaxQuarter] [tinyint] NULL,
        [TaxQuarterName] [nvarchar](20) NULL,
        [TaxQuarterID] [int] NULL,
        [TaxMonthID] [int] NULL
    );

    ALTER TABLE #dimDate ADD CONSTRAINT PK_tmp_dimDate PRIMARY KEY CLUSTERED(DateID);

    CREATE UNIQUE INDEX UIX_tmp_dimDate ON #dimDate(Year, Month, Day);

    DECLARE @EasterDates TABLE(
        [Year]              INT
        ,EasterDate         DATE
        ,GoodFridayDate     AS DATEADD(DAY, -2, EasterDate) PERSISTED
        ,EasterMondayDate   AS DATEADD(DAY, 1, EasterDate)  PERSISTED
    );

    /* Populate the easter dates */
    WITH cYears AS (
        SELECT
            DATEPART(YEAR, DATEADD(YEAR, Num, @StartDate))  AS Year
        FROM
            dbo.Numbers
        WHERE
            Num <= DATEDIFF(YEAR, @StartDate, @EndDate)
    )
    INSERT INTO @EasterDates(Year, EasterDate)
    SELECT
        Year
        ,dbo.CalcEaster(Year)   AS EasterDate
    FROM
        cYears;


    WITH cDates AS (

        /* Get the list of dates between @StartDate and @EndDate from a numbers table */
        SELECT
            DATEADD(DAY, Num, @StartDate)   AS DateKey
        FROM
            dbo.Numbers
        WHERE
            Num <= DATEDIFF(DAY, @StartDate, @EndDate)

    ), cFormattedDates AS (

        /* Do some basic date calculations */
        SELECT 
            DateKey
            ,CONVERT(INT, CONVERT(NVARCHAR(10), d.DateKey, 112))        AS DateID
            ,CONVERT(NVARCHAR(10), d.DateKey, 121)                      AS DateDisplayShort
            ,CONVERT(NVARCHAR(50), d.DateKey, 113)                      AS DateDisplayLong
            ,YEAR(d.DateKey)                                            AS Year
            ,DATEPART(QUARTER, d.DateKey)                               AS Quarter
            ,'Qtr ' + CONVERT(CHAR(1), DATEPART(QUARTER, d.DateKey))    AS QuarterName
            ,(YEAR(d.DateKey) * 10) + DATEPART(QUARTER, d.DateKey)      AS QuarterID
            ,MONTH(d.DateKey)                                           AS Month
            ,DATENAME(MONTH, d.DateKey)                                 AS MonthName
            ,(YEAR(d.DateKey) * 100) + MONTH(d.DateKey)                 AS MonthID
            ,DAY(d.DateKey)                                             AS Day
            ,DATENAME(WEEKDAY, d.DateKey)                               AS WeekdayName
            ,DATEPART(WEEKDAY, d.DateKey)                               AS DayOfWeek
            ,DATEPART(ISO_WEEK, DateKey)                                AS ISOWeek
            ,CASE
                WHEN DATEPART(ISO_WEEK, DateKey) >= 52 AND MONTH(d.DateKey) = 1 THEN YEAR(d.DateKey) - 1
                ELSE YEAR(d.DateKey)    
            END                                                         AS ISOYear
            ,CASE
                WHEN DATEPART(WEEKDAY, d.DateKey) IN (1, 7)    THEN 'N'
                ELSE 'Y'
            END                                                         AS IsWeekDay
            ,CASE
                WHEN DATEPART(WEEKDAY, d.DateKey) IN (1, 7)    THEN 'Y'
                ELSE 'N'
            END                                                         AS IsWeekend
            ,CAST('N' AS CHAR(1))                                       AS IsPublicHoliday
            ,CAST(NULL AS NVARCHAR(50))                                 AS PublicHolidayName
            ,CAST(NULL AS SMALLINT)                                     AS HolidayDateID
            ,YEAR(d.DateKey) - CASE
                                    WHEN MONTH(d.DateKey) < @TaxStartMonth THEN 1
                                    WHEN MONTH(d.DateKey) = @TaxStartMonth AND DAY(d.DateKey) < @TaxStartDay THEN 1
                                    ELSE 0
                                END                                     AS TaxYear
            ,(CASE
		        WHEN MONTH(d.DateKey) < @TaxStartMonth      THEN 12 + MONTH(d.DateKey)  - @TaxStartMonth
		        ELSE (MONTH(d.DateKey) - @TaxStartMonth)
	        END / 3) + 1                                                AS TaxQuarter
            ,'Qtr ' + CAST((CASE
		        WHEN MONTH(d.DateKey) < @TaxStartMonth      THEN 12 + MONTH(d.DateKey)  - @TaxStartMonth
		        ELSE (MONTH(d.DateKey) - @TaxStartMonth)
	        END / 3) + 1 AS VARCHAR(1))                                 AS TaxQuarterName

            ,(10 * (YEAR(d.DateKey) - CASE
                                    WHEN MONTH(d.DateKey) < @TaxStartMonth THEN 1
                                    WHEN MONTH(d.DateKey) = @TaxStartMonth AND DAY(d.DateKey) < @TaxStartDay THEN 1
                                    ELSE 0
                                END)) + (CASE
		                                    WHEN MONTH(d.DateKey) < @TaxStartMonth      THEN 12 + MONTH(d.DateKey)  - @TaxStartMonth
		                                    ELSE (MONTH(d.DateKey) - @TaxStartMonth)
	                                    END / 3) + 1                    AS TaxQuarterID

            ,(100 * (YEAR(d.DateKey) - CASE
                                    WHEN MONTH(d.DateKey) < @TaxStartMonth THEN 1
                                    WHEN MONTH(d.DateKey) = @TaxStartMonth AND DAY(d.DateKey) < @TaxStartDay THEN 1
                                    ELSE 0
                                END)) + (CASE
		                                    WHEN MONTH(d.DateKey) < @TaxStartMonth      THEN 12 + MONTH(d.DateKey)  - @TaxStartMonth
		                                    ELSE (MONTH(d.DateKey) - @TaxStartMonth)
	                                    END)                            AS TaxMonthID
        FROM
            cDates AS d
    )
    INSERT INTO #dimDate
    SELECT
        DateID, DateKey, DateDisplayShort, DateDisplayLong, Year, Quarter, QuarterName, QuarterID, Month, MonthName, MonthID, Day, WeekdayName, 
        DayOfWeek, 0 /* NthWeekDayOfMonth */, 0 /* NthWeekDayOfYear */, ISOWeek, ISOYear, IsWeekDay, IsWeekend, IsPublicHoliday, PublicHolidayName, HolidayDateID,
		TaxYear, TaxQuarter, TaxQuarterName, TaxQuarterID, TaxMonthID
    FROM
        cFormattedDates;

    /* Set the Nth occurances fields */
    WITH cNthDays AS (
        SELECT
            DateID
            ,NthWeekDayOfMonth = ROW_NUMBER() OVER(PARTITION BY Year, Month, WeekdayName ORDER BY DateID)
            ,NthWeekDayOfYear = ROW_NUMBER() OVER(PARTITION BY Year, WeekdayName ORDER BY DateID)
        FROM
            #dimDate
    )
    UPDATE tmp
    SET
        NthWeekDayOfMonth = c.NthWeekDayOfMonth
        ,NthWeekDayOfYear = c.NthWeekDayOfYear
    FROM
        #dimDate AS tmp
        INNER JOIN cNthDays AS c
            ON tmp.DateID = c.DateID
    ;


    /* Set the Easter holidays */
    UPDATE tmp
    SET
        IsPublicHoliday = 'Y'
        ,PublicHolidayName = 'Easter Sunday'
    FROM
        #dimDate AS tmp
        INNER JOIN @EasterDates AS e
            ON tmp.DateKey = e.EasterDate
    ;

    UPDATE tmp
    SET
        IsPublicHoliday = 'Y'
        ,PublicHolidayName = 'Easter Monday'
    FROM
        #dimDate AS tmp
        INNER JOIN @EasterDates AS e
            ON tmp.DateKey = e.EasterMondayDate
    ;

    UPDATE tmp
    SET
        IsPublicHoliday = 'Y'
        ,PublicHolidayName = 'Good Friday'
    FROM
        #dimDate AS tmp
        INNER JOIN @EasterDates AS e
            ON tmp.DateKey = e.GoodFridayDate
    ;

    /* Set the fixed holidays */
    UPDATE tmp
    SET
        --IsPublicHoliday = CASE    /* If its mondayised we'll set the IsPublicHoliday to "N" */
        --                        WHEN h.IsMondayised = 1 THEN 'N' 
        --                        WHEN IsWeekDay = 'Y' THEN 'N' ELSE 'Y' 
        --                END
        IsPublicHoliday    = 'Y'
        ,PublicHolidayName = h.HolidayName
        ,HolidayDateID = h.HolidayDateID
    FROM
        #dimDate AS tmp
        INNER JOIN dbo.dimHolidayDate AS h
            ON tmp.Month = h.Month
            AND tmp.Day = h.Day
    WHERE
        h.Month IS NOT NULL
    AND    h.Day    IS NOT NULL


    /* Set the observed holidays */
    UPDATE d
    SET
        IsPublicHoliday = 'Y'
        ,PublicHolidayName = h.HolidayName + ' (observed)'
        ,HolidayDateID = h.HolidayDateID
    FROM
        #dimDate as tmp
        INNER JOIN dbo.dimHolidayDate AS h
            ON tmp.Month = h.Month
            AND tmp.Day = h.Day
        CROSS APPLY(
            SELECT MAX(d.DateKey) AS LastNonWeekendDate
            FROM
                #dimDate AS d
            WHERE
                d.DateID < tmp.DateID
            AND d.IsWeekend = 'N'
            ) AS LastWeekDay
        --CROSS APPLY (
        --    SELECT MIN(d.DateKey) AS NextWeekdayDate
        --    FROM #dimDate AS d
        --    WHERE
        --        d.DateID >= tmp.DateID
        --    AND d.IsWeekDay = 'Y'
        --    ) AS NextWeekDay
        INNER JOIN #dimDate AS d
            ON d.DateKey = DATEADD(DAY, CASE WHEN tmp.WeekdayName = 'Saturday' THEN 1 ELSE 0 END + DATEDIFF(DAY, LastWeekDay.LastNonWeekendDate, tmp.DateKey), tmp.DateKey)
    WHERE
        tmp.IsWeekDay = 'N'
    AND h.IsMondayised = 1
    AND h.Month IS NOT NULL
    AND    h.Day    IS NOT NULL


    /* Set the Nth day of the month holidays */
    UPDATE tmp
    SET
        IsPublicHoliday = 'Y'
        ,PublicHolidayName = n.HolidayName
        ,HolidayDateID = n.HolidayDateID
    FROM
        #dimDate AS tmp
        INNER JOIN dbo.dimHolidayDate AS n
            ON tmp.Month = n.Month
            AND tmp.WeekdayName = n.WeekdayName
            AND tmp.NthWeekDayOfMonth = n.NthOccurrence
    WHERE
        n.NthOccurrence IS NOT NULL;


    MERGE INTO dbo.dimDate AS d
    USING #dimDate AS tmp
        ON d.DateID = tmp.DateID

    WHEN MATCHED THEN UPDATE
    SET
        DateKey                 = tmp.DateKey
        ,DateDisplayShort       = tmp.DateDisplayShort
        ,DateDisplayLong        = tmp.DateDisplayLong
        ,Year                   = tmp.Year
        ,Quarter                = tmp.Quarter
        ,QuarterName            = tmp.QuarterName
        ,QuarterID              = tmp.QuarterID
        ,Month                  = tmp.Month
        ,MonthName              = tmp.MonthName
        ,MonthID                = tmp.MonthID
        ,Day                    = tmp.Day
        ,[WeekdayName]          = tmp.WeekdayName
        ,DayOfWeek              = tmp.DayOfWeek
        ,NthWeekDayOfMonth      = tmp.NthWeekDayOfMonth
        ,NthWeekDayOfYear       = tmp.NthWeekDayOfYear
        ,ISOWeek                = tmp.ISOWeek
        ,ISOYear                = tmp.ISOYear
        ,[IsWeekday]            = tmp.IsWeekDay
        ,IsWeekend              = tmp.IsWeekend
        ,IsBusinessDay          = tmp.IsBusinessDay
        ,IsPublicHoliday        = tmp.IsPublicHoliday
        ,PublicHolidayName      = tmp.PublicHolidayName
        ,HolidayDateID          = tmp.HolidayDateID
        ,TaxYear                = tmp.TaxYear
        ,TaxQuarter             = tmp.TaxQuarter
        ,TaxQuarterName         = tmp.TaxQuarterName
        ,TaxQuarterID           = tmp.TaxQuarterID
        ,TaxMonthID             = tmp.TaxMonthID

    WHEN NOT MATCHED THEN 
    INSERT(
        DateID
        ,DateKey                        
        ,DateDisplayShort            
        ,DateDisplayLong            
        ,Year
        ,Quarter                    
        ,QuarterName                
        ,QuarterID                    
        ,Month                        
        ,MonthName                    
        ,MonthID                    
        ,Day                        
        ,[WeekdayName]                
        ,DayOfWeek                    
        ,NthWeekDayOfMonth            
        ,NthWeekDayOfYear            
        ,ISOWeek
        ,ISOYear
        ,[IsWeekday]                    
        ,IsWeekend                    
        ,IsBusinessDay                
        ,IsPublicHoliday            
        ,PublicHolidayName
        ,HolidayDateID
        ,TaxYear          
        ,TaxQuarter       
        ,TaxQuarterName   
        ,TaxQuarterID     
        ,TaxMonthID       
        )
    VALUES(
        tmp.DateID
        ,tmp.DateKey
        ,tmp.DateDisplayShort
        ,tmp.DateDisplayLong
        ,tmp.Year
        ,tmp.Quarter
        ,tmp.QuarterName
        ,tmp.QuarterID
        ,tmp.Month
        ,tmp.MonthName
        ,tmp.MonthID
        ,tmp.Day
        ,tmp.WeekdayName
        ,tmp.DayOfWeek
        ,tmp.NthWeekDayOfMonth
        ,tmp.NthWeekDayOfYear
        ,tmp.ISOWeek
        ,tmp.ISOYear
        ,tmp.IsWeekDay
        ,tmp.IsWeekend
        ,tmp.IsBusinessDay
        ,tmp.IsPublicHoliday
        ,tmp.PublicHolidayName
        ,tmp.HolidayDateID
        ,tmp.TaxYear          
        ,tmp.TaxQuarter       
        ,tmp.TaxQuarterName   
        ,tmp.TaxQuarterID     
        ,tmp.TaxMonthID       
        )
        ;

    /*
     * Tidy up
     */
    DROP TABLE #dimDate;
END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Procedure to add new dates to the dbo.dimDate dimension.  It generates the dates between the two dates supplied calculating the various attributes for the date dimension.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'dimDateAdd';

