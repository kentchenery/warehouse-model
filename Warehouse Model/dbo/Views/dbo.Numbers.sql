﻿CREATE VIEW [dbo].[Numbers]
AS
/*
 ---
 Description:   Generates a list of numbers so that it can be used as a numbers or tally table in queries.  This numbers table
                is zero based (it starts as zero as opposed to 1)
 ...
 */
WITH N1(N) AS (

    /* 10 */
    SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 

), N2(N) AS (

    /* 100 */
    SELECT a.N
    FROM N1 AS a, N1 AS b

), N4(N) AS (

    /* 10000 */
    SELECT a.N
    FROM N2 AS a, N2 AS b

), N8(N) AS (

    /* 100000000 */
    SELECT a.N
    FROM N4 AS a, N4 AS b

), ZeroBase(Num) AS (

    /* Add 0 */
    SELECT 0 AS NUM

    UNION ALL

    SELECT
        ROW_NUMBER() OVER(ORDER BY (SELECT NULL))    AS Num
    FROM
        N8
)
/* Return the numbers */
SELECT
    Num
FROM
    ZeroBase
;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A numbers (or tally) table to assist with queries that use numbers tables.  This view derives its values through a CTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Numbers';

