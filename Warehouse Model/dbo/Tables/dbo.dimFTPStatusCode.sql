﻿CREATE TABLE [dbo].[dimFTPStatusCode] (
    [FTPStatusCodeID]      INT             NOT NULL,
    [StatusCode]           INT             NULL,
    [StatusDescription]    NVARCHAR (260)  NOT NULL,
    [ReplyType]            INT             NOT NULL,
    [ReplyTypeName]        NVARCHAR (30)   NOT NULL,
    [ReplyTypeDescription] NVARCHAR (1000) NOT NULL,
    [GroupType]            INT             NOT NULL,
    [GroupTypeName]        NVARCHAR (30)   NOT NULL,
    [GroupTypeDescription] NVARCHAR (150)  NOT NULL,
    CONSTRAINT [PK_dimFTPStatusCode] PRIMARY KEY CLUSTERED ([FTPStatusCodeID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group type description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'GroupTypeDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group type name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'GroupTypeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group type.  Derived from the second digit of the status code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'GroupType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reply type description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'ReplyTypeDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reply type name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'ReplyTypeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FTP reply type.  This is derived from the first digit of the code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'ReplyType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description of the status code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FTP status code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'COLUMN', @level2name = N'FTPStatusCodeID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FTP Status Code dimension table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode';


GO
CREATE NONCLUSTERED INDEX [AK_dimFTPStatusCode_StatusCode]
    ON [dbo].[dimFTPStatusCode]([StatusCode] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'CONSTRAINT', @level2name = N'PK_dimFTPStatusCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on StatusCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimFTPStatusCode', @level2type = N'INDEX', @level2name = N'AK_dimFTPStatusCode_StatusCode';

