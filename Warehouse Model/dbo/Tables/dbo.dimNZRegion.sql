﻿CREATE TABLE [dbo].[dimNZRegion] (
    [NZRegionID]             SMALLINT      NOT NULL,
    [RegionCode]             NVARCHAR (6)  NOT NULL,
    [RegionName]             NVARCHAR (50) NULL,
    [SubdivisionCategory]    NVARCHAR (50) NULL,
    [IslandID]               SMALLINT      NULL,
    [IslandCode]             CHAR (4)      NULL,
    [IslandName]             NVARCHAR (12) NULL,
    [AnniversaryMonth]       TINYINT       NULL,
    [AnniversaryDay]         TINYINT       NULL,
    [AnniversaryDescription] NVARCHAR (20) NULL,
    [AnniversaryName]        NVARCHAR (30) NULL,
    CONSTRAINT [PK_dimNZRegion] PRIMARY KEY CLUSTERED ([NZRegionID] ASC),
    CONSTRAINT [AK_dimNZRegion_RegionCode] UNIQUE NONCLUSTERED ([RegionCode] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'New Zealand region dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The subdivision category (e.g. regional council).  This seems to represent how the region is governed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'SubdivisionCategory';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The region name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'RegionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'6 character code to represent the region (e.g. NZ-AUK = Auckland)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'RegionCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'NZRegionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the island', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'IslandName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The island ID (1 = North Island, 2 = South Island)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'IslandID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A 4 character code to represent the island (e.g. NZ-N = North, NZ-S = South)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'IslandCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the anniversary', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'AnniversaryName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The month the region anniversary occurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'AnniversaryMonth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The textual date the anniversary occurrs on (e.g. 29th January)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'AnniversaryDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The day in the month the region anniversary occurrs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'COLUMN', @level2name = N'AnniversaryDay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on RegionCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'CONSTRAINT', @level2name = N'AK_dimNZRegion_RegionCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZRegion', @level2type = N'CONSTRAINT', @level2name = N'PK_dimNZRegion';

