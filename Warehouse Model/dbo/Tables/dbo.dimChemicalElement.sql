﻿CREATE TABLE [dbo].[dimChemicalElement] (
    [ChemicalElementID] SMALLINT        NOT NULL,
    [AtomicNumber]      SMALLINT        NOT NULL,
    [ElementSymbol]     VARCHAR (3)     NOT NULL,
    [ElementName]       VARCHAR (20)    NOT NULL,
    [ElementGroup]      SMALLINT        NOT NULL,
    [Period]            SMALLINT        NOT NULL,
    [Block]             CHAR (1)        NOT NULL,
    [STPState]          VARCHAR (10)    NOT NULL,
    [Occurrence]        VARCHAR (10)    NOT NULL,
    [Category]          VARCHAR (10)    NOT NULL,
    [SubCategory]       VARCHAR (25)    NOT NULL,
    [AtomicWeight]      DECIMAL (11, 8) NULL,
    [Density]           DECIMAL (10, 8) NULL,
    [MeltingPointK]     DECIMAL (8, 4)  NULL,
    [BoilingPointK]     DECIMAL (8, 4)  NULL,
    [Heat]              DECIMAL (5, 3)  NULL,
    [ElectroNegativity] DECIMAL (3, 2)  NULL,
    [Abundance]         VARCHAR (10)    NULL,
    CONSTRAINT [PK_dimChemicalElement] PRIMARY KEY CLUSTERED ([ChemicalElementID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chemical elements of the periodic table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'State at "Standard Temperature and Pressure"', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'STPState';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Symbol of the element as seen on the periodic table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'ElementSymbol';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'English name of the element', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'ElementName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'ChemicalElementID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Atomic number of the element', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'AtomicNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The subcategory that the element belongs to (e.g. Alkali metal, Alkali-earth metal, halogen, nobel gas etc)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'SubCategory';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Period the element belongs to in the periodic table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Period';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Occurrence distinguishes naturally occurring elements, categorized as either primordial or transient (from decay), and additional synthetic elements that have been produced technologically, but are not known to occur naturally.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Occurrence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Metling point temperature in Kelvin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'MeltingPointK';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heat capacity in J/gK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Heat';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group the element belongs to in the periodic table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'ElementGroup';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Electronegativity of the element', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'ElectroNegativity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Density in g/cm3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Density';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The category that the element belongs to (e.g. metal, metalloid, non-metal)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Boiling point temperature in Kelvin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'BoilingPointK';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Block the element belongs to in the periodic tabl', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Block';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Atomic weight of the element (in atomic mass units u)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'AtomicWeight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abundance of the element in the Earths crust', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'COLUMN', @level2name = N'Abundance';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimChemicalElement', @level2type = N'CONSTRAINT', @level2name = N'PK_dimChemicalElement';

