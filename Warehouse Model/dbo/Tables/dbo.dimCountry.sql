﻿CREATE TABLE [dbo].[dimCountry] (
    [CountryID]   SMALLINT      NOT NULL,
    [CountryName] NVARCHAR (50) NULL,
    [Alpha2Code]  CHAR (2)      NULL,
    [Alpha3Code]  CHAR (3)      NULL,
    [NumericCode] CHAR (3)      NULL,
    [NumericNum]  SMALLINT      NULL,
    CONSTRAINT [PK_dimCountry] PRIMARY KEY CLUSTERED ([CountryID] ASC),
    CONSTRAINT [AK_dimCountry_Alpha2Code] UNIQUE NONCLUSTERED ([Alpha2Code] ASC),
    CONSTRAINT [AK_dimCountry_Alpha3Code] UNIQUE NONCLUSTERED ([Alpha3Code] ASC),
    CONSTRAINT [AK_dimCountry_NumericCode] UNIQUE NONCLUSTERED ([NumericCode] ASC),
    CONSTRAINT [AK_dimCountry_NumericNum] UNIQUE NONCLUSTERED ([NumericNum] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Country dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Integer representation of the 3 digit ISO number code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'NumericNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 digit ISO country numeric code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'NumericCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Country name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'CountryName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key. Country ID column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'CountryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 letter ISO country code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'Alpha3Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2 letter ISO country code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'COLUMN', @level2name = N'Alpha2Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on NumericNum column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCountry_NumericNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on NumericCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCountry_NumericCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on Alpha3Code column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCountry_Alpha3Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on Alpha2Code column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCountry_Alpha2Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountry', @level2type = N'CONSTRAINT', @level2name = N'PK_dimCountry';

