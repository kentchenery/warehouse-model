﻿CREATE TABLE [dbo].[dimHTTPStatusCode] (
    [HTTPStatusID]      SMALLINT       NOT NULL,
    [StatusCode]        SMALLINT       NULL,
    [StatusClass]       NVARCHAR (13)  NOT NULL,
    [StatusName]        NVARCHAR (54)  NOT NULL,
    [StatusDescription] NVARCHAR (700) NOT NULL,
    CONSTRAINT [PK_dimHTTPStatusCode] PRIMARY KEY CLUSTERED ([HTTPStatusID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A description about the HTTP status code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The HTTP Status name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The HTTP status class (e.g. Informational, Success, Redirection, Client Error, Server Error)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusClass';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The HTTP status code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'COLUMN', @level2name = N'StatusCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'COLUMN', @level2name = N'HTTPStatusID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dimension for HTTP Status codes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode';


GO
CREATE NONCLUSTERED INDEX [AK_dimHTTPStatusCode_StatusCode]
    ON [dbo].[dimHTTPStatusCode]([StatusCode] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'CONSTRAINT', @level2name = N'PK_dimHTTPStatusCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on StatusCode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHTTPStatusCode', @level2type = N'INDEX', @level2name = N'AK_dimHTTPStatusCode_StatusCode';

