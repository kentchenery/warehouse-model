﻿CREATE TABLE [dbo].[dimContinent] (
    [ContinentID]   SMALLINT     NOT NULL,
    [ContinentName] VARCHAR (50) NOT NULL,
    [ContinentCode] CHAR (2)     NULL,
    CONSTRAINT [PK_dimContinent] PRIMARY KEY CLUSTERED ([ContinentID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2 letter code for each continent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimContinent', @level2type = N'COLUMN', @level2name = N'ContinentCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the continent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimContinent', @level2type = N'COLUMN', @level2name = N'ContinentName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimContinent', @level2type = N'COLUMN', @level2name = N'ContinentID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Continent dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimContinent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimContinent', @level2type = N'CONSTRAINT', @level2name = N'PK_dimContinent';

