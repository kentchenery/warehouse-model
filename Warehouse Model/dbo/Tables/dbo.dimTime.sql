﻿CREATE TABLE [dbo].[dimTime] (
    [TimeID]           INT           NOT NULL,
    [TimeKey]          TIME (0)      NULL,
    [Hour24]           TINYINT       NULL,
    [Hour12]           TINYINT       NULL,
    [HalfHourID]       SMALLINT      NULL,
    [HalfHour]         CHAR (5)      NULL,
    [QuarterHourID]    SMALLINT      NULL,
    [QuarterHour]      CHAR (5)      NULL,
    [MinuteID]         SMALLINT      NULL,
    [Minute]           TINYINT       NULL,
    [MinuteDisplay]    CHAR (5)      NULL,
    [Seconds]          TINYINT       NULL,
    [TimeDisplayShort] NVARCHAR (10) NULL,
    [TimeDisplayLong]  NVARCHAR (30) NULL,
    [IsBusinessHours]  CHAR (1)      NULL,
    [IsMorning]        CHAR (1)      NULL,
    [IsAfternoon]      CHAR (1)      NULL,
    [AMPM]             CHAR (4)      NULL,
    CONSTRAINT [PK_dimTime] PRIMARY KEY CLUSTERED ([TimeID] ASC),
    CONSTRAINT [AK_dimTime_TimeKey] UNIQUE NONCLUSTERED ([TimeKey] ASC),
    CONSTRAINT [AK_dimTime_TimeParts] UNIQUE NONCLUSTERED ([Hour24] ASC, [Minute] ASC, [Seconds] ASC) WITH (FILLFACTOR = 100)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Time dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time datatype represented by the record', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'TimeKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key.  The integer value represents the 24hr time stored for the record.  (e.g. 0 = 00:00:00, 193608 = 19:36:08)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'TimeID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short display version of the time (e.g. 17:45:23 = 17:45:23)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'TimeDisplayShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Long display version of the time (e.g. 17:45:23 = 05:45:23 am)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'TimeDisplayLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The seconds value of the time (e.g. 08:23:00 = 0, 19:54:32 = 32)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'Seconds';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID value for the half hour part in a 24hour day (e.g. 07:03 = 700, 08:48 = 803)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'QuarterHourID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The quarter hour the time falls in (e.g. 01:27:00 = 01:15, 07:45:00 = 07:45)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'QuarterHour';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID value for the minute part in a 24hour day (e.g. 07:03 = 703, 08:48 = 848)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'MinuteID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Time display down to minute level (e.g. 04:34:12 = 04:34)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'MinuteDisplay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The minute value of the time (e.g. 08:23:00 = 23, 19:54:32 = 54)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'Minute';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is the time before midday (in the morning)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'IsMorning';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show whether the time falls within business hours (8:30am to 4:59pm. 5:00pm is not considered business hours)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'IsBusinessHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is the time after midday (in the afternoon)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'IsAfternoon';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The hour value as seen on a 24hour clock', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'Hour24';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The hour value as seen on a 12hour clock', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'Hour12';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID value for the half hour part in a 24hour day. (e.g. 300 = 03:00:00 to 03:29:59, 1501 = 15:30:00 to 15:59:59)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'HalfHourID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The half hour the time falls in (e.g. 01:27:00 = 01:00, 07:45:00 = 07:30)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'HalfHour';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AM or PM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'COLUMN', @level2name = N'AMPM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on Hour24, Minute and Seconds column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'CONSTRAINT', @level2name = N'AK_dimTime_TimeParts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on TimeKey column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'CONSTRAINT', @level2name = N'AK_dimTime_TimeKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTime', @level2type = N'CONSTRAINT', @level2name = N'PK_dimTime';

