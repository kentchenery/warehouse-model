﻿CREATE TABLE [dbo].[dimHolidayDate] (
    [HolidayDateID] SMALLINT      NOT NULL,
    [HolidayName]   NVARCHAR (50) NOT NULL,
    [Month]         SMALLINT      NULL,
    [Day]           SMALLINT      NULL,
    [WeekdayName]   NVARCHAR (20) NULL,
    [NthOccurrence] SMALLINT      NULL,
    [IsMondayised]  BIT           NULL,
    CONSTRAINT [PK_dimHolidayDates] PRIMARY KEY CLUSTERED ([HolidayDateID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contains an entry per holiday to describe when it occurs (e.g. 25/12 = Christmas, 4th Monday in October = Labour Day)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The day of the week the holiday occurs on if the holiday moves from year to year.  Typically this will be Monday as holidays are typically "mondayised"', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'WeekdayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Which occurrence of the day in the month that the holiday occurs on', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'NthOccurrence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The month the holiday occurs in.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'Month';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show whether the holiday is mondayised or not.  If the holiday occurs on a weekend then the following monday is observed as the holiday (e.g. Christmas day is mondayised)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'IsMondayised';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the holiday', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'HolidayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'HolidayDateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The day of the month the holiday occurs on.  Only populated if the holiday is a fixed date.  (E.g. Christmas = 25 Dec)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'COLUMN', @level2name = N'Day';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimHolidayDate', @level2type = N'CONSTRAINT', @level2name = N'PK_dimHolidayDates';

