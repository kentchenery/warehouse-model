﻿CREATE TABLE [dbo].[dimExchangeRate] (
    [ExchangeRateID]   INT             NOT NULL,
    [FromCurrencyCode] CHAR (3)        NOT NULL,
    [ToCurrencyCode]   CHAR (3)        NOT NULL,
    [ValidFromUTC]     DATETIME2 (3)   NOT NULL,
    [ValidToUTC]       DATETIME2 (3)   NOT NULL,
    [ExchangeRate]     DECIMAL (19, 9) NOT NULL,
    [FromCurrencyID]   INT             NOT NULL,
    [ToCurrencyID]     INT             NOT NULL,
    CONSTRAINT [PK_dimExchangeRate] PRIMARY KEY NONCLUSTERED ([ExchangeRateID] ASC),
    CONSTRAINT [FK_dimExchangeRate_dimCurrency_FromCurrencyCode] FOREIGN KEY ([FromCurrencyCode]) REFERENCES [dbo].[dimCurrency] ([CurrencyCode]),
    CONSTRAINT [FK_dimExchangeRate_dimCurrency_FromCurrencyID] FOREIGN KEY ([FromCurrencyID]) REFERENCES [dbo].[dimCurrency] ([CurrencyID]),
    CONSTRAINT [FK_dimExchangeRate_dimCurrency_ToCurrencyCode] FOREIGN KEY ([ToCurrencyCode]) REFERENCES [dbo].[dimCurrency] ([CurrencyCode]),
    CONSTRAINT [FK_dimExchangeRate_dimCurrency_ToCurrencyID] FOREIGN KEY ([ToCurrencyID]) REFERENCES [dbo].[dimCurrency] ([CurrencyID]),
    CONSTRAINT [AK_dimExchangeRate_Column] UNIQUE NONCLUSTERED ([FromCurrencyCode] ASC, [ToCurrencyCode] ASC, [ValidFromUTC] ASC)
);




GO

CREATE CLUSTERED INDEX [CIX_dimExchangeRate_ValidFromUTC]
    ON [dbo].[dimExchangeRate]([ValidFromUTC] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exchange history table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date and time the rate is valid to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ValidToUTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date and time the rate is valid from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ValidFromUTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The currency ID from dbo.dimCurrency that we are converting to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ToCurrencyID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 letter currency code of the currency we are converting to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ToCurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The currency ID from dbo.dimCurrency that we are converting from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'FromCurrencyID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 letter currency code of the currency we are converting from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'FromCurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The primary key', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ExchangeRateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The exchange rate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'COLUMN', @level2name = N'ExchangeRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on FromCurrencyCode, ToCurrencyCode and ValidFromUTC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'CONSTRAINT', @level2name = N'AK_dimExchangeRate_Column';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'CONSTRAINT', @level2name = N'PK_dimExchangeRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clustered index on the ValidFromUTC column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimExchangeRate', @level2type = N'INDEX', @level2name = N'CIX_dimExchangeRate_ValidFromUTC';

