﻿CREATE TABLE [dbo].[dimDate] (
    [DateID]            INT           NOT NULL,
    [DateKey]           DATE          NULL,
    [DateDisplayShort]  NVARCHAR (10) NULL,
    [DateDisplayLong]   NVARCHAR (30) NULL,
    [Year]              SMALLINT      NULL,
    [Quarter]           TINYINT       NULL,
    [QuarterName]       NVARCHAR (20) NULL,
    [QuarterID]         INT           NULL,
    [Month]             SMALLINT      NULL,
    [MonthName]         NVARCHAR (20) NULL,
    [MonthID]           INT           NULL,
    [Day]               TINYINT       NULL,
    [WeekdayName]       NVARCHAR (20) NULL,
    [DayOfWeek]         TINYINT       NULL,
    [NthWeekDayOfMonth] SMALLINT      NULL,
    [NthWeekDayOfYear]  SMALLINT      NULL,
    [ISOWeek]           TINYINT       NULL,
    [ISOYear]           SMALLINT      NULL,
    [IsWeekday]         CHAR (1)      NULL,
    [IsWeekend]         CHAR (1)      NULL,
    [IsBusinessDay]     CHAR (1)      NULL,
    [IsPublicHoliday]   CHAR (1)      NULL,
    [PublicHolidayName] NVARCHAR (50) NULL,
    [HolidayDateID]     SMALLINT      NULL,
    [TaxYear]           SMALLINT      NULL,
    [TaxQuarter]        TINYINT       NULL,
    [TaxQuarterName]    NVARCHAR (20) NULL,
    [TaxQuarterID]      INT           NULL,
    [TaxMonthID]        INT           NULL,
    CONSTRAINT [PK_dimDate] PRIMARY KEY CLUSTERED ([DateID] ASC),
    CONSTRAINT [FK_dimDate_dimHolidayDate] FOREIGN KEY ([HolidayDateID]) REFERENCES [dbo].[dimHolidayDate] ([HolidayDateID]),
    CONSTRAINT [AK_dimDate_DateKey] UNIQUE NONCLUSTERED ([DateKey] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [AK_dimDate_DateParts] UNIQUE NONCLUSTERED ([Year] ASC, [Month] ASC, [Day] ASC) WITH (FILLFACTOR = 100)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign key for dimHolidayDate from dimDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'CONSTRAINT', @level2name = N'FK_dimDate_dimHolidayDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The year value of the date (e.g. 2013)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'Year';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the day in the week (e.g. Sunday)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'WeekdayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The display value of the quarter (e.g. Qtr 1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'QuarterName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'An ID value for the quarter that is unique across all quarters in the dates. It is not a unique value for each date as many dates span a particular quarter.  (e.g. Q1 in 2013 is stored as 20131)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'QuarterID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The quarter value of the date (e.g. 1 represents Q1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'Quarter';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the public holiday', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'PublicHolidayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The nth occurrence of this weekday this year', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'NthWeekDayOfYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The nth occurrence of this weekday this month', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'NthWeekDayOfMonth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the month (E.g. February)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'MonthName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A unique value for the month represented as YYYYMM (e.g. Feb 2013 = 201302)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'MonthID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The month number in the year (e.g. Feb = 2)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'Month';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show whether the day falls on a weekend or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'IsWeekend';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show whether the day is a weekday or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'IsWeekday';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show if the day is a public holiday or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'IsPublicHoliday';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The ISO year that ISO_Week is part of.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'ISOYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The ISO week number as returned from DATEPART(ISO_WEEK, DateValue)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'ISOWeek';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to show whether the day is a business day or not.  Effectively if the day is a weekday and it is not a public holiday then this is "Y"', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'IsBusinessDay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign Key to dbo.dimHolidayDate.  The HolidayDateID from dbo.dimHolidayDate that the holiday was derived from.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'HolidayDateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The number of the day of the week (e.g. Sunday = 1, Monday = 2 etc)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'DayOfWeek';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The day number in the month (e.g. 23rd Feb would be represented as 23)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'Day';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date value stored as the date datatype', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'DateKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key.  The value is an integer representation of the date that is stored for this record.  (e.g.  3rd June 2013 is represented as 20130603)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'DateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short display version of the date (YYYY-MM-DD)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'DateDisplayShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The long display version of the date (DD MMM YYYY)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'COLUMN', @level2name = N'DateDisplayLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index for Year, Month and Day', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'CONSTRAINT', @level2name = N'AK_dimDate_DateParts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on the DateKey column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'CONSTRAINT', @level2name = N'AK_dimDate_DateKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimDate', @level2type = N'CONSTRAINT', @level2name = N'PK_dimDate';

