﻿CREATE TABLE [dbo].[dimNZGSTRate] (
    [NZGSTRateID]    SMALLINT       NOT NULL,
    [GSTRatePct]     DECIMAL (4, 2) NOT NULL,
    [GSTRateFactor]  DECIMAL (4, 3) NOT NULL,
    [GSTRateDisplay] VARCHAR (20)   NOT NULL,
    [ValidFrom]      DATE           NOT NULL,
    [ValidTo]        DATE           NOT NULL,
    CONSTRAINT [PK_dimNZGSTRate] PRIMARY KEY CLUSTERED ([NZGSTRateID] ASC)
);




GO

CREATE NONCLUSTERED INDEX [IX_dimNZGSTRate_ValidFrom_ValidTo]
    ON [dbo].[dimNZGSTRate]([ValidFrom] ASC, [ValidTo] ASC) WITH (FILLFACTOR = 100);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NZ GST rate dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date the rate was effective to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'ValidTo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date the rate was effective from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'ValidFrom';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'NZGSTRateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GST rate as a percentage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'GSTRatePct';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A multiplication factor for the GST rate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'GSTRateFactor';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Display value', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'COLUMN', @level2name = N'GSTRateDisplay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'CONSTRAINT', @level2name = N'PK_dimNZGSTRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Index for ValidFrom and ValidTo dates', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimNZGSTRate', @level2type = N'INDEX', @level2name = N'IX_dimNZGSTRate_ValidFrom_ValidTo';

