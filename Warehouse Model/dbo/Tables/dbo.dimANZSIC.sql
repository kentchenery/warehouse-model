﻿CREATE TABLE [dbo].[dimANZSIC] (
    [ANZSICID]               SMALLINT       IDENTITY (1, 1) NOT NULL,
    [DivisionCode]           CHAR (1)       NULL,
    [DivisionDescription]    NVARCHAR (150) NULL,
    [SubdivisionCode]        CHAR (2)       NULL,
    [SubdivisionDescription] NVARCHAR (150) NULL,
    [GroupCode]              CHAR (3)       NULL,
    [GroupDescription]       NVARCHAR (150) NULL,
    [ClassCode]              CHAR (4)       NULL,
    [ClassDescription]       NVARCHAR (150) NULL,
    [ANZSICCode]             CHAR (5)       NULL,
    CONSTRAINT [PK_dimANZSIC] PRIMARY KEY CLUSTERED ([ANZSICID] ASC),
    CONSTRAINT [AK_dimANZSIC_ANZSICCode] UNIQUE NONCLUSTERED ([ANZSICCode] ASC)
);





GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Australia & New Zealand Standard Industry Codes (ANZSIC) dimension.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subdivision description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'SubdivisionDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2 character subdivision code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'SubdivisionCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'GroupDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 character group code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'GroupCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Division description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'DivisionDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Single character division code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'DivisionCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Class description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'ClassDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'4 character class code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'ClassCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'ANZSICID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'5 character ANZSIC code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'COLUMN', @level2name = N'ANZSICCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on ANZSICCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'CONSTRAINT', @level2name = N'AK_dimANZSIC_ANZSICCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimANZSIC', @level2type = N'CONSTRAINT', @level2name = N'PK_dimANZSIC';

