﻿CREATE TABLE [dbo].[dimLanguages] (
    [LanguageID]     INT           NOT NULL,
    [EnglishName]    VARCHAR (80)  NOT NULL,
    [NativeName]     NVARCHAR (80) NOT NULL,
    [LanguageFamily] VARCHAR (20)  NOT NULL,
    [ISO639_1]       CHAR (2)      NOT NULL,
    [ISO639_2T]      CHAR (3)      NOT NULL,
    [ISO639_2B]      CHAR (3)      NOT NULL,
    CONSTRAINT [PK_dimLanguages] PRIMARY KEY CLUSTERED ([LanguageID] ASC),
    CONSTRAINT [AK_dimLanguages_ISO639_1] UNIQUE NONCLUSTERED ([ISO639_1] ASC),
    CONSTRAINT [AK_dimLanguages_ISO639_2B] UNIQUE NONCLUSTERED ([ISO639_2B] ASC),
    CONSTRAINT [AK_dimLanguages_ISO639_2T] UNIQUE NONCLUSTERED ([ISO639_2T] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Languages dimension derived from ISO 639.

More information about ISO 693 can be found at http://www.iso.org/iso/language_codes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The native name for the language (e.g. French = Français)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'NativeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'LanguageID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The family the language belongs to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'LanguageFamily';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The 3 character ISO639-2/T code for the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'ISO639_2T';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The 3 character ISO639-2/B code for the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'ISO639_2B';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The 2 character ISO639-1 code for the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'ISO639_1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The English name for the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'COLUMN', @level2name = N'EnglishName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on ISO639_2T column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'CONSTRAINT', @level2name = N'AK_dimLanguages_ISO639_2T';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on ISO639_2B column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'CONSTRAINT', @level2name = N'AK_dimLanguages_ISO639_2B';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on ISO639_1 column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'CONSTRAINT', @level2name = N'AK_dimLanguages_ISO639_1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimLanguages', @level2type = N'CONSTRAINT', @level2name = N'PK_dimLanguages';

