﻿CREATE TABLE [dbo].[dimCountryContinent] (
    [CountryID]   SMALLINT NOT NULL,
    [ContinentID] SMALLINT NOT NULL,
    CONSTRAINT [PK_dimCountryContinent] PRIMARY KEY CLUSTERED ([CountryID] ASC, [ContinentID] ASC),
    CONSTRAINT [FK_dimCountryContinent_dimContinent] FOREIGN KEY ([ContinentID]) REFERENCES [dbo].[dimContinent] ([ContinentID]),
    CONSTRAINT [FK_dimCountryContinent_dimCountry] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[dimCountry] ([CountryID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign key between dimContinent and dimCountryContinent.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent', @level2type = N'CONSTRAINT', @level2name = N'FK_dimCountryContinent_dimContinent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign key constraint between dimCountry and dimCountryContinent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent', @level2type = N'CONSTRAINT', @level2name = N'FK_dimCountryContinent_dimCountry';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign key to dimContinent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent', @level2type = N'COLUMN', @level2name = N'ContinentID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Foreign key to dimCountry', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent', @level2type = N'COLUMN', @level2name = N'CountryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Country to contentinents dimension.  A country can be in more than one continent (e.g. Russia, Cyprus, Kazakhstan)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCountryContinent', @level2type = N'CONSTRAINT', @level2name = N'PK_dimCountryContinent';

