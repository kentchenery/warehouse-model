﻿CREATE TABLE [dbo].[dimUSState] (
    [USStateID]              SMALLINT     NOT NULL,
    [StateCode]              CHAR (2)     NOT NULL,
    [CommonName]             VARCHAR (14) NOT NULL,
    [OfficialName]           VARCHAR (48) NOT NULL,
    [CapitalCity]            VARCHAR (14) NOT NULL,
    [MostPopulatedCity]      VARCHAR (14) NOT NULL,
    [StatehoodDate]          DATE         NULL,
    [AreaMi]                 INT          NULL,
    [AreaKM]                 INT          NULL,
    [Population]             INT          NULL,
    [CensusBureauRegion]     VARCHAR (9)  NULL,
    [CensusBureauDivision]   VARCHAR (18) NULL,
    [EconomicAnalysisRegion] VARCHAR (14) NULL,
    CONSTRAINT [PK_dimUSState] PRIMARY KEY CLUSTERED ([USStateID] ASC),
    CONSTRAINT [AK_dimUSState_StateCode] UNIQUE NONCLUSTERED ([StateCode] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'Population';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total area in kilometers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'AreaKM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total area in miles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'AreaMi';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date the state came into existence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'StatehoodDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The city with the largest population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'MostPopulatedCity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The capital city', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'CapitalCity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Official title of the state', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'OfficialName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Common name of the state in general use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'CommonName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2 letter code for the state', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'StateCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'USStateID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of US states', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Bureau of Economic Analysis regions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'EconomicAnalysisRegion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Regions used by the United States Census Bureau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'CensusBureauRegion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Divisions used by the United States Census Bureau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'COLUMN', @level2name = N'CensusBureauDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'CONSTRAINT', @level2name = N'PK_dimUSState';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on StateCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimUSState', @level2type = N'CONSTRAINT', @level2name = N'AK_dimUSState_StateCode';

