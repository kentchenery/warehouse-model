﻿CREATE TABLE [dbo].[dimCurrency] (
    [CurrencyID]     INT           NOT NULL,
    [CurrencyCode]   CHAR (3)      NOT NULL,
    [CurrencyName]   NVARCHAR (65) NOT NULL,
    [CurrencyNumber] CHAR (3)      NOT NULL,
    [DecimalMinor]   SMALLINT      NULL,
    [CurrencySymbol] NVARCHAR (6)  NULL,
    CONSTRAINT [PK_dimCurrency] PRIMARY KEY CLUSTERED ([CurrencyID] ASC),
    CONSTRAINT [AK_dimCurrency_CurrencyCode] UNIQUE NONCLUSTERED ([CurrencyCode] ASC),
    CONSTRAINT [AK_dimCurrency_CurrencyNumber] UNIQUE NONCLUSTERED ([CurrencyNumber] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currency dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The number of decimal places for the minor portion of the currency.  (e.g. USD, AUD, and NZD all have cents to 2 decimal places.  Therefore the value of 2 would be stored here).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'DecimalMinor';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The currency symbol', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'CurrencySymbol';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 digit ISO currency number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'CurrencyNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currency name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'CurrencyName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key.  Currency ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'CurrencyID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 letter ISO currency code (e.g. NZD = NZ Dollar, USD = US Dollar, GBP = Great British Pound)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'COLUMN', @level2name = N'CurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on CurrencyNumber column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCurrency_CurrencyNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on CurrencyCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'CONSTRAINT', @level2name = N'AK_dimCurrency_CurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimCurrency', @level2type = N'CONSTRAINT', @level2name = N'PK_dimCurrency';

