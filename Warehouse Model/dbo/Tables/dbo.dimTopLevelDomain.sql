﻿CREATE TABLE [dbo].[dimTopLevelDomain] (
    [TopLevelDomainID]       INT            IDENTITY (1, 1) NOT NULL,
    [TopLevelDomainCode]     NVARCHAR (64)  NOT NULL,
    [DomainType]             NVARCHAR (20)  NOT NULL,
    [SponsoringOrganisation] NVARCHAR (120) NOT NULL,
    [IANAUrl]                NVARCHAR (200) NULL,
    CONSTRAINT [PK_dimTopLevelDomain] PRIMARY KEY CLUSTERED ([TopLevelDomainID] ASC),
    CONSTRAINT [AK_dimTopLevelDomain_TopLevelDomainCode] UNIQUE NONCLUSTERED ([TopLevelDomainCode] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Top level domain (gTLD) dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'COLUMN', @level2name = N'TopLevelDomainID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The top level domain code (e.g. .com, .net, .nz, .uk)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'COLUMN', @level2name = N'TopLevelDomainCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The organisation responsible for the management of the domain', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'COLUMN', @level2name = N'SponsoringOrganisation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL to the IANA website about the domain', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'COLUMN', @level2name = N'IANAUrl';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A grouping for domain types (e.g. Country code, Infrastructure)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'COLUMN', @level2name = N'DomainType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alternate key index on TopLevelDomainCode column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'CONSTRAINT', @level2name = N'AK_dimTopLevelDomain_TopLevelDomainCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key index', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimTopLevelDomain', @level2type = N'CONSTRAINT', @level2name = N'PK_dimTopLevelDomain';

