﻿CREATE TABLE [dbo].[dimGender] (
    [GenderID]   TINYINT      NOT NULL,
    [Gender]     VARCHAR (30) NOT NULL,
    [GenderCode] CHAR (1)     NOT NULL,
    CONSTRAINT [PK_dimGender] PRIMARY KEY CLUSTERED ([GenderID] ASC)
);





GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gender dimension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimGender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column.  Value follows the ISO gender values as seen here: http://en.wikipedia.org/wiki/ISO/IEC_5218', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimGender', @level2type = N'COLUMN', @level2name = N'GenderID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Single letter code for the gender (e.g M = Male, F = Female)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimGender', @level2type = N'COLUMN', @level2name = N'GenderCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The gender name (e.g. Male, Female)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimGender', @level2type = N'COLUMN', @level2name = N'Gender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dimGender', @level2type = N'CONSTRAINT', @level2name = N'PK_dimGender';

