﻿CREATE FUNCTION [dbo].[CalcEaster](@Year INT)
RETURNS DATE
WITH SCHEMABINDING
AS
BEGIN;
    /*
     * This function originally came from http://www.bbc.co.uk/dna/h2g2/A653267 which read:
     *
     * Given the Year as a four digit number in the range 1700 - 2299
     * Calculate Day and Month of Easter Sunday
     *  Note 1: the algorithm has not been tested outside this range.
     *  Note 2: the \ operator performs integer division without remainder.
     *  Note 3: the date returned is the Gregorian Calendar date
     *          (the one we use now), even for dates in the 18th Century.
     * 
     * a = Year mod 19;
     * b = Year \ 100;
     * c = Year mod 100;
     * d = b \ 4;
     * e = b mod 4;
     * f = c \ 4;
     * g = c mod 4;
     * 
     * h = (b + 8)\25;
     * i = (b - h + 1)\3;
     * j = (19*a + b - d - i + 15) mod 30;
     * k = (32 + 2*e + 2*f - j - g) mod 7;
     * m = (a + 11*j + 22*k) \ 451;
     * n = j + k - 7*m + 114;
     * 
     * Month = n\31;
     * Day = (n mod 31) + 1;
     */
    DECLARE @a  INT;
    DECLARE @b  INT;
    DECLARE @c  INT;
    DECLARE @d  INT;
    DECLARE @e  INT;
    DECLARE @f  INT;
    DECLARE @g  INT;
    DECLARE @h  INT;
    DECLARE @i  INT;
    DECLARE @j  INT;
    DECLARE @k  INT;
    DECLARE @l  INT;
    DECLARE @m  INT;
    DECLARE @n  INT;

    SELECT @a = (@Year % 19);
    SELECT @b = (@Year / 100);
    SELECT @c = (@Year % 100);
    SELECT @d = ((@Year / 100) / 4);
    SELECT @e = ((@Year / 100) % 4);
    SELECT @f = ((@Year % 100) / 4);
    SELECT @g = ((@Year % 100) % 4);
    
    SELECT @h = (@b + 8) / 25;
    SELECT @i = (@b - @h +1 ) / 3;
    SELECT @j = ((19 * @a) + @b - @d - @i + 15) % 30;
    SELECT @k = (32 + (2 * @e) + (2 * @f) - @j - @g) % 7;
    SELECT @m = (@a + (11 * @j) + (22 * @k)) / 451;
    SELECT @n = (@j + @k) - (7 * @m) + 114;

    /*
     * Return the values as a date
     */
    RETURN CONVERT(DATE, CONVERT(CHAR(4), @Year) + '-' + RIGHT('0' + CONVERT(VARCHAR(2), (@n / 31)), 2) + '-' + RIGHT('0' + CONVERT(VARCHAR(2), (@n % 31) + 1), 2));
END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Function that given a year returns the date that Easter falls on', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'CalcEaster';

